-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 21, 2020 at 02:39 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pendaftaran`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `Id_Admin` int(4) NOT NULL,
  `Kode_Admin` int(12) NOT NULL,
  `Nama` varchar(50) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `Jabatan` varchar(50) NOT NULL,
  `Password` varchar(255) NOT NULL,
  `Status` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`Id_Admin`, `Kode_Admin`, `Nama`, `Email`, `Jabatan`, `Password`, `Status`) VALUES
(1, 12345678, 'Budi Permata', 'budi@gmail.com', 'Manager', '$2y$10$fSc.yyKb/fcOyYzQJe1/euLpE65ZvN.igPFyJPHZYKXTxUj/5hIl.', ''),
(2, 2018103791, 'Ferry', 'chaiferry1502@gmail.com', 'SDA', '$2y$10$c6hIrhVKbh6C4jQJfBS89OQXZVixeOY9UvwLSe/x6AU26A/jb4Tfe', ''),
(3, 2018103847, 'Malvin Hadipurwanto', 'hadimalvin@gmail.com', 'Finance', '$2y$10$tQT1uZlnJTac61.vsHHSIuSYF7cUEwAgwjFlqj8eRWpEdSrPLtcia', ''),
(4, 2018104003, 'Yosia Enrique', 'yosiaenrique0@gmail.com', 'SDA', '$2y$10$r6UJM57h9wTPC7iXwmHvBe4ZdmThWHz72VxLqgkBr19K9zvCSdJGy', ''),
(5, 2018103848, 'Hizkia Natanael', 'hesel.natanael07@gmail.com', 'Manager', '$2y$10$s.gBGHxmLpi2rRixoM0sA.aMFkXJnGwbqUSSO0.yFhHJ2eDZaCP0G', ''),
(6, 2018103829, 'Andre Natanael', 'andrenathaniel@gmail.com', 'Finance', '$2y$10$YpdiXsv6yel8vl/NSfa/o.AefOhFZ9O9KdtMdkJV5ibX6Odwwm6tC', '');

-- --------------------------------------------------------

--
-- Table structure for table `daftarproposal`
--

CREATE TABLE `daftarproposal` (
  `Id_Proposal` int(4) NOT NULL,
  `Kode_Skripsi` varchar(6) NOT NULL,
  `NIM` int(15) NOT NULL,
  `Judul_BI` text NOT NULL,
  `Judul_EN` text NOT NULL,
  `Tanggal_Diajukan` date NOT NULL,
  `Tanggal_Disetujui` date DEFAULT NULL,
  `Tahun_Di` year(4) NOT NULL,
  `Tahun_Ke` year(4) NOT NULL,
  `Status` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `daftarubahjudul`
--

CREATE TABLE `daftarubahjudul` (
  `Id_Judul` int(4) NOT NULL,
  `Kode_Skripsi` varchar(6) NOT NULL,
  `NIM` int(15) NOT NULL,
  `Judul_BIlama` text NOT NULL,
  `Judul_ENlama` text NOT NULL,
  `Judul_BIbaru` text NOT NULL,
  `Judul_ENbaru` text NOT NULL,
  `Alasan` varchar(20) NOT NULL,
  `Tanggal_Diajukan` date NOT NULL,
  `Tanggal_Disetujui` date DEFAULT NULL,
  `Tahun_Di` year(4) NOT NULL,
  `Tahun_Ke` year(4) NOT NULL,
  `Status` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jadwalsusulan`
--

CREATE TABLE `jadwalsusulan` (
  `Id_Susulan` int(4) NOT NULL,
  `Kode_Ujian` varchar(10) NOT NULL,
  `NIM` int(15) NOT NULL,
  `Nama_Matkul` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jadwalujian`
--

CREATE TABLE `jadwalujian` (
  `Id_Ujian` int(4) NOT NULL,
  `Kode_Ujian` varchar(8) NOT NULL,
  `Nama_Matkul` varchar(50) NOT NULL,
  `Kode_Jurusan` varchar(15) NOT NULL,
  `Semester` int(1) NOT NULL,
  `Tanggal` date NOT NULL,
  `Jam` time NOT NULL,
  `Ruang` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jurusan`
--

CREATE TABLE `jurusan` (
  `Id_Jurusan` int(4) NOT NULL,
  `Fakultas` varchar(30) NOT NULL,
  `Kode_Jurusan` varchar(15) NOT NULL,
  `Nama_Jurusan` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jurusan`
--

INSERT INTO `jurusan` (`Id_Jurusan`, `Fakultas`, `Kode_Jurusan`, `Nama_Jurusan`) VALUES
(1, 'Industri Kreatif', 'TI', 'Informatika'),
(2, 'Industri Kreatif', 'SI', 'Sistem Informasi'),
(3, 'Bisnis', 'MN', 'Manajemen'),
(4, 'Bisnis', 'AK', 'Akuntansi'),
(5, 'Bisnis', 'MA', 'Matematika'),
(6, 'Industri Kreatif', 'IK', 'Ilmu Komunikasi'),
(7, 'Industri Kreatif', 'DV', 'Desain Komunikasi Visual'),
(8, 'Industri Kreatif', 'AT', 'Arsitektur');

-- --------------------------------------------------------

--
-- Table structure for table `keys`
--

CREATE TABLE `keys` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `key` varchar(40) NOT NULL,
  `level` int(2) NOT NULL,
  `ignore_limits` tinyint(1) NOT NULL DEFAULT '0',
  `is_private_key` tinyint(1) NOT NULL DEFAULT '0',
  `ip_addresses` text,
  `date_created` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `keys`
--

INSERT INTO `keys` (`id`, `user_id`, `key`, `level`, `ignore_limits`, `is_private_key`, `ip_addresses`, `date_created`) VALUES
(1, 12345678, '1234', 1, 0, 0, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `limits`
--

CREATE TABLE `limits` (
  `id` int(11) NOT NULL,
  `uri` varchar(255) NOT NULL,
  `count` int(10) NOT NULL,
  `hour_started` int(11) NOT NULL,
  `api_key` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `limits`
--

INSERT INTO `limits` (`id`, `uri`, `count`, `hour_started`, `api_key`) VALUES
(1, 'uri:api/mahasiswa:get', 2, 1591332203, '1234'),
(2, 'uri:api/skripsi:delete', 2, 1591435868, '1234'),
(3, 'uri:api/mahasiswa:delete', 1, 1591721951, '1234'),
(4, 'uri:api/jadwalujian:delete', 1, 1591455131, '1234');

-- --------------------------------------------------------

--
-- Table structure for table `mahasiswa`
--

CREATE TABLE `mahasiswa` (
  `Id_Mahasiswa` int(10) NOT NULL,
  `Password` varchar(255) NOT NULL,
  `NIM` int(15) NOT NULL,
  `Nama` varchar(80) NOT NULL,
  `Kode_Jurusan` varchar(15) NOT NULL,
  `Semester` int(2) NOT NULL,
  `Email` varchar(40) NOT NULL,
  `Telepon` varchar(12) NOT NULL,
  `Kode_Program` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mahasiswa`
--

INSERT INTO `mahasiswa` (`Id_Mahasiswa`, `Password`, `NIM`, `Nama`, `Kode_Jurusan`, `Semester`, `Email`, `Telepon`, `Kode_Program`) VALUES
(1, '$2y$10$BjSDbIsyeYoeOZDiKZtOZ.WsBP7puQab8MpPgVcx/414s3ew1sQde', 2018103847, 'Malvin', 'AT', 8, 'hadimalvin@gmail.com', '081589271654', 'SP'),
(3, '$2y$10$klwPbaa2OuTUcYCcYg3nVuJ5Iq404NICH8E.ICyViWZb22M//M1wa', 2018103791, 'Ferry Chai', 'IK', 5, 'ferryyuno@gmail.com', '082213844350', 'SM'),
(5, '$2y$10$WX0/28OlQp99yJthOFFQO.It3JUe8Zuu3IkQGGu2U3pNWq799GzWW', 2018103848, 'Hizkia Natanael', 'SI', 1, 'hesel.natanael07@gmail.com', '085654378765', 'DP'),
(6, '$2y$10$4aW03TWB8ZdQ3fYoILHbnOIDJjH8DUD1Rgo/6za7vOKP0Z3PlK6le', 2018103829, 'Andre Natanael', 'SI', 2, 'alenathaniel14@gmail.com', '082213544322', 'DM'),
(10, '$2y$10$ep8TAJD3Zd93Ps0HMp/qden6Y2v9pQjT1zY8ZqibYgH/o9pVpwQ6S', 2018104004, 'Yosia Enrique', 'AT', 5, 'yosiaenrique0@gmail.com', '086654379876', 'MM');

-- --------------------------------------------------------

--
-- Table structure for table `matkul`
--

CREATE TABLE `matkul` (
  `Id_Matkul` int(11) NOT NULL,
  `Semester` int(1) NOT NULL,
  `Nama_Matkul` varchar(50) NOT NULL,
  `Kode_Jurusan` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `matkul`
--

INSERT INTO `matkul` (`Id_Matkul`, `Semester`, `Nama_Matkul`, `Kode_Jurusan`) VALUES
(13, 1, 'Programming and Algorithm', 'TI'),
(14, 1, 'Basic Web Programming', 'TI'),
(15, 1, 'Science and Critical Thinking', 'TI'),
(16, 2, 'Advance Web Programming ', 'TI'),
(17, 2, 'Computer Network and System Security', 'TI'),
(18, 2, 'Structured Data', 'TI'),
(19, 3, 'Object Oriented Programming', 'TI'),
(20, 3, 'Basic Database', 'TI'),
(21, 3, 'Operating System', 'TI'),
(22, 4, 'Advanced Database', 'TI'),
(23, 4, 'Visual Programming Technique', 'TI'),
(24, 4, 'Software Engineering ', 'TI'),
(25, 5, 'Computer Graphics', 'TI'),
(26, 5, 'Parallel Computation', 'TI'),
(27, 5, 'Mobile Computing', 'TI'),
(28, 6, 'Cloud Computation Technology', 'TI'),
(29, 6, 'Computer Vision', 'TI'),
(30, 6, 'Data Science', 'TI'),
(31, 7, 'Computer System and Architecture', 'TI'),
(32, 7, 'Natural Language Programming ', 'TI'),
(33, 7, 'Big Data ', 'TI'),
(34, 8, 'Thesis', 'TI'),
(35, 8, 'Capita Selecta for Computer', 'TI'),
(36, 8, 'Computer Security', 'TI'),
(37, 1, 'Pengantar Bisnis', 'AK'),
(38, 1, 'Akuntansi Keuangan Dasar I', 'AK'),
(39, 1, 'Pengantar Teknologi Informasi I', 'AK'),
(40, 2, 'Pengantar Manajemen', 'AK'),
(41, 2, 'Statistika Bisnis', 'AK'),
(42, 2, 'Akuntansi Keuangan Dasar II', 'AK'),
(43, 3, 'Akt. Keuangan Menengah I', 'AK'),
(44, 3, 'Akuntansi Biaya', 'AK'),
(45, 3, 'Manajemen Keuangan I', 'AK'),
(46, 4, 'Akt. Keuangan Menengah II', 'AK'),
(47, 4, 'Akt. Perusahaan Manufaktur', 'AK'),
(48, 4, 'Manajemen Keuangan II', 'AK'),
(49, 5, 'Akuntansi Manajemen', 'AK'),
(50, 5, 'Sistem Informasi Manajemen I', 'AK'),
(51, 5, 'Sistem Informasi Akuntansi', 'AK'),
(52, 6, 'Pemeriksaan Manajemen', 'AK'),
(53, 6, 'Metodologi Penelitian', 'AK'),
(54, 6, 'Pemeriksaan Akuntan II', 'AK'),
(55, 7, 'EDP Auditing', 'AK'),
(56, 7, 'Akuntansi Internasional', 'AK'),
(57, 7, 'Etika Bisnis', 'AK'),
(58, 8, 'Sistem Pengendalian Manajemen', 'AK'),
(59, 8, 'Manajemen Operasional', 'AK'),
(60, 8, 'Manajemen Biaya', 'AK'),
(61, 1, 'Pengantar Teknologi Informasi II', 'SI'),
(62, 1, 'Praktikum Algoritma dan Pemrograman 1', 'SI'),
(63, 1, 'Algoritma dan Pemrograman 1', 'SI'),
(64, 2, 'Algoritma dan Pemrograman 2', 'SI'),
(65, 2, 'Praktikum Algoritma dan Pemrograman 2', 'SI'),
(66, 2, 'Struktur Data', 'SI'),
(67, 3, 'Sistem Informasi Manajemen II', 'SI'),
(68, 3, 'Sistem Informasi Akuntansi dan Keuangan', 'SI'),
(69, 3, 'Sistem Informasi Data dan Berkas', 'SI'),
(70, 4, 'Sistem Jaringan Komputer', 'SI'),
(71, 4, 'Sistem Operasi', 'SI'),
(72, 4, 'Pemrograman Berorientasi Objek', 'SI'),
(73, 5, 'Grafik Komputer dan Pengolahan Citra', 'SI'),
(74, 5, 'Interaksi Manusia dan Komputer', 'SI'),
(75, 5, 'Sistem Basis Data 1', 'SI'),
(76, 6, 'Analisis dan Perancangan Sistem Informasi', 'SI'),
(77, 6, 'Sistem Basis Data 2', 'SI'),
(78, 6, 'Pengantar Teknik Kompilasi', 'SI'),
(79, 7, 'Pengelolaan Proyek Sistem Informasi', 'SI'),
(80, 7, 'Testing dan Implementasi Sistem', 'SI'),
(81, 7, 'Enterprise Architect', 'SI'),
(82, 8, 'Pengantar Bisnis Teknologi Informasi', 'SI'),
(83, 8, 'Etika dan Profesi Teknologi Sistem Informasi', 'SI'),
(84, 8, 'Audit Sistem Informasi', 'SI'),
(86, 1, 'Prinsip Akuntansi I', 'MN'),
(87, 1, 'Pengantar Ekonomi', 'MN'),
(88, 1, 'Pengantar Manajemen dan Bisnis', 'MN'),
(89, 2, 'Pengantar Industri Kreatif', 'MN'),
(90, 2, 'Manajemen Pemasaran', 'MN'),
(91, 2, 'Prinsip Akuntansi II', 'MN'),
(92, 3, 'Aspek Legal Bisnis', 'MN'),
(93, 3, 'Ekonomi Makro', 'MN'),
(94, 3, 'Pemasaran Media Baru', 'MN'),
(95, 4, 'Marketing Komunikasi', 'MN'),
(96, 4, 'Manajemen Operasi', 'MN'),
(97, 4, 'Ekonomi Mikro', 'MN'),
(99, 5, 'Dasar Perpajakan', 'MN'),
(100, 5, 'Bisnis Internasional', 'MN'),
(101, 5, 'Kepemimpinan', 'MN'),
(102, 6, 'Pengembangan Produk dan Merk', 'MN'),
(103, 6, 'Memahami Dinamika Sosial', 'MN'),
(104, 6, 'Manajerial Ekonomi ', 'MN'),
(105, 7, 'Strategi Manajemen', 'MN'),
(106, 7, 'Properti Intelektual', 'MN'),
(108, 7, 'Fundamental Analisa Data', 'MN'),
(109, 8, 'Bisnis Internasional II', 'MN'),
(110, 8, 'Kepemimpinan II', 'MN'),
(111, 8, 'Strategi Manajemen II', 'MN'),
(112, 1, 'Logika Matematika dan Teori Himpunan', 'MA'),
(113, 1, 'Kalkulus I', 'MA'),
(114, 1, 'Analisis Data Dasar', 'MA'),
(115, 2, 'Aljabar Linear Elementer', 'MA'),
(116, 2, 'Pemrograman Komputer', 'MA'),
(117, 2, 'Kalkulus II', 'MA'),
(118, 3, 'Kalkulus Perubahan Banyak', 'MA'),
(119, 3, 'Geometri', 'MA'),
(120, 3, 'Teori Peluang ', 'MA'),
(121, 4, 'Matematika Diskrit', 'MA'),
(122, 4, 'Metode Numerik', 'MA'),
(123, 4, 'Statistika Matematika', 'MA'),
(124, 5, 'Analisis Riil', 'MA'),
(125, 5, 'Optimasi', 'MA'),
(126, 5, 'Pemodelan Stokastik', 'MA'),
(127, 6, 'Akuntansi Aktuaria', 'MA'),
(128, 6, 'Matematika Aktuaria', 'MA'),
(129, 6, 'Pemodelan Aktuaria', 'MA'),
(130, 7, 'Seminar Matematika', 'MA'),
(131, 7, 'Kerja Praktek', 'MA'),
(132, 7, 'Struktur Aljabar', 'MA'),
(133, 8, 'Citra Profesional ', 'MA'),
(134, 8, 'Statistika Non Parametik', 'MA'),
(135, 8, 'Matemitika Risiko dan Valuasi Aset ', 'MA'),
(136, 1, 'Pengenalan Dasar Ilmu Komunikasi', 'IK'),
(137, 1, 'Pengenalan Dasar Sosiologi', 'IK'),
(138, 1, 'Pengenalan Dasar Psikologi', 'IK'),
(139, 2, 'Etika dan Filsafat Komunikasi', 'IK'),
(140, 2, 'Komunikasi Digital', 'IK'),
(141, 2, 'Pengenalan Dasar Periklanan', 'IK'),
(142, 3, 'Teori Komunikasi', 'IK'),
(143, 3, 'Komunikasi Visual', 'IK'),
(144, 3, 'Manajemen Periklanan', 'IK'),
(145, 4, 'Metode Penelitian Komunikasi I', 'IK'),
(146, 4, 'Perencanaan Kreatif dan Strategi Periklanan', 'IK'),
(147, 4, 'Komunikasi Organisasi', 'IK'),
(148, 5, 'Metode Penelitian Komunikasi II', 'IK'),
(149, 5, 'Komunikasi Kesehatan', 'IK'),
(150, 5, 'Design Grafis Periklanan', 'IK'),
(151, 6, 'Produksi Iklan Digital', 'IK'),
(152, 6, 'Komunikasi Antar Biaya', 'IK'),
(153, 6, 'Riset Periklanan', 'IK'),
(154, 7, 'Produksi Iklan Audio Visual', 'IK'),
(155, 7, 'Kerja Praktek Ilkom', 'IK'),
(156, 7, 'Bahasa Inggris Khusus', 'IK'),
(157, 8, 'Regulasi dan Etika Humas', 'IK'),
(158, 8, 'Regulasi dan Etika Penyiaran', 'IK'),
(159, 8, 'Manajemen Merk', 'IK'),
(160, 1, 'Gambar 1 - Gambar Dasar', 'DV'),
(161, 1, 'Nirmana 2D', 'DV'),
(163, 1, 'Sejarah Seni Rupa dan Pra Design', 'DV'),
(164, 2, 'Gambar 2 - Gambar Analisa', 'DV'),
(165, 2, 'Nirmana 3D', 'DV'),
(166, 2, 'Sejarah Design', 'DV'),
(167, 3, 'Gambar 3 - Manusia dan Konsep', 'DV'),
(168, 3, 'DKV 1 - Metode Desain Dasar dan Gestalt', 'DV'),
(169, 3, 'Fotografi Dasar', 'DV'),
(170, 4, 'Ilustrasi', 'DV'),
(171, 4, 'DKV - 2 Sistem Design ', 'DV'),
(172, 4, 'Bahasa Rupa dan Psikologi Persepsi', 'DV'),
(173, 4, 'Grafika Cetak 1', 'DV'),
(174, 5, 'DKV - 3 Media dan Eksperimen Material', 'DV'),
(175, 5, 'Kemasan', 'DV'),
(176, 5, 'Grafika Cetak 2', 'DV'),
(177, 6, 'DKV - 4  Pre Projek', 'DV'),
(178, 6, 'Kerja Praktek DKV', 'DV'),
(179, 6, 'Kalbis Peduli', 'DV'),
(180, 7, 'DKV - 5 Pameran dan Perancangan Portofolio', 'DV'),
(181, 7, 'Eksperimen Kreatif', 'DV'),
(182, 7, 'Metodologi Design', 'DV'),
(183, 8, 'Tugas Akhir ', 'DV'),
(184, 8, 'Kalbis Olahraga', 'DV'),
(185, 8, 'Kalbis Seni', 'DV'),
(187, 1, 'Estetika Rupa', 'AT'),
(188, 1, 'Pengantar Arsitektur', 'AT'),
(189, 1, 'Komunikasi Arsitektur', 'AT'),
(190, 2, 'Eksplorasi Bentuk', 'AT'),
(191, 2, 'Struktur dan Konstruksi Bangunan I', 'AT'),
(192, 2, 'Matematika Arsitektur', 'AT'),
(193, 3, 'Metode Perancangan', 'AT'),
(194, 3, 'Struktur dan Konstruksi Bangunan II', 'AT'),
(195, 3, 'Perancangan Arsitektur I', 'AT'),
(196, 4, 'Sistem Bangunan', 'AT'),
(197, 4, 'Design Arsitektur Berbasis Komputer', 'AT'),
(198, 4, 'Perancangan Arsitektur II', 'AT'),
(199, 5, 'Struktur dan Bentuk', 'AT'),
(200, 5, 'Teori Arsitektur', 'AT'),
(201, 5, 'Perancangan Arsitektur III', 'AT'),
(202, 6, 'Perancangan Arsitektur IV', 'AT'),
(203, 6, 'Arsitektur Hijau', 'AT'),
(204, 6, 'Material dan Teknologi Bangunan', 'AT'),
(205, 7, 'Riset Tugas Akhir', 'AT'),
(206, 7, 'Perancangan Arsitektur V', 'AT'),
(207, 7, 'Real Estate dan Pemukiman', 'AT'),
(208, 8, 'Tugas Akhir Arsitektur', 'AT'),
(209, 8, 'Bangunan Industri', 'AT'),
(210, 8, 'Perancangan Kota', 'AT');

-- --------------------------------------------------------

--
-- Table structure for table `program`
--

CREATE TABLE `program` (
  `Id_Program` int(2) NOT NULL,
  `Kode` varchar(2) NOT NULL,
  `Program` varchar(15) NOT NULL,
  `Kelas` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `program`
--

INSERT INTO `program` (`Id_Program`, `Kode`, `Program`, `Kelas`) VALUES
(1, 'SP', 'Sarjana', 'Pagi'),
(2, 'SM', 'Sarjana', 'Malam'),
(3, 'MP', 'Magister', 'Pagi'),
(4, 'MM', 'Magister', 'Malam'),
(5, 'DP', 'Diploma', 'Pagi'),
(6, 'DM', 'Diploma', 'Malam');

-- --------------------------------------------------------

--
-- Table structure for table `semester`
--

CREATE TABLE `semester` (
  `Id_Semester` int(10) NOT NULL,
  `semester` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `semester`
--

INSERT INTO `semester` (`Id_Semester`, `semester`) VALUES
(1, 'Semester 1'),
(2, 'Semester 2'),
(3, 'Semester 3'),
(4, 'Semester 4'),
(5, 'Semester 5'),
(6, 'Semester 6'),
(7, 'Semester 7'),
(8, 'Semester 8');

-- --------------------------------------------------------

--
-- Table structure for table `skripsi`
--

CREATE TABLE `skripsi` (
  `Id_Skripsi` int(2) NOT NULL,
  `Kode_Skripsi` varchar(6) NOT NULL,
  `NIM` int(15) NOT NULL,
  `Judul_BI` text NOT NULL,
  `Judul_EN` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ujiansusulan`
--

CREATE TABLE `ujiansusulan` (
  `Id_formSusulan` int(4) NOT NULL,
  `Kode_Form` varchar(10) NOT NULL,
  `NIM` int(15) NOT NULL,
  `Nama_Matkul` varchar(50) NOT NULL,
  `Tanggal_Diajukan` date NOT NULL,
  `Tanggal_Disetujui` date DEFAULT NULL,
  `Tahun_Di` year(4) NOT NULL,
  `Tahun_Ke` year(4) NOT NULL,
  `Alasan` text NOT NULL,
  `Status` varchar(10) NOT NULL,
  `Bayar` char(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`Id_Admin`);

--
-- Indexes for table `daftarproposal`
--
ALTER TABLE `daftarproposal`
  ADD PRIMARY KEY (`Id_Proposal`),
  ADD UNIQUE KEY `Kode_Skripsi` (`Kode_Skripsi`),
  ADD KEY `NIM` (`NIM`);

--
-- Indexes for table `daftarubahjudul`
--
ALTER TABLE `daftarubahjudul`
  ADD PRIMARY KEY (`Id_Judul`),
  ADD UNIQUE KEY `Kode_Skripsi` (`Kode_Skripsi`);

--
-- Indexes for table `jadwalsusulan`
--
ALTER TABLE `jadwalsusulan`
  ADD PRIMARY KEY (`Id_Susulan`),
  ADD KEY `Kode_Ujian` (`Kode_Ujian`);

--
-- Indexes for table `jadwalujian`
--
ALTER TABLE `jadwalujian`
  ADD PRIMARY KEY (`Id_Ujian`);

--
-- Indexes for table `jurusan`
--
ALTER TABLE `jurusan`
  ADD PRIMARY KEY (`Id_Jurusan`),
  ADD UNIQUE KEY `Kode_Jurusan` (`Kode_Jurusan`);

--
-- Indexes for table `keys`
--
ALTER TABLE `keys`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `limits`
--
ALTER TABLE `limits`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  ADD PRIMARY KEY (`Id_Mahasiswa`),
  ADD UNIQUE KEY `NIM` (`NIM`),
  ADD KEY `kode_jurusan` (`Kode_Jurusan`),
  ADD KEY `Kode_Program` (`Kode_Program`);

--
-- Indexes for table `matkul`
--
ALTER TABLE `matkul`
  ADD PRIMARY KEY (`Id_Matkul`),
  ADD UNIQUE KEY `Nama_Matkul` (`Nama_Matkul`),
  ADD KEY `Kode_Jurusan` (`Kode_Jurusan`);

--
-- Indexes for table `program`
--
ALTER TABLE `program`
  ADD PRIMARY KEY (`Id_Program`),
  ADD UNIQUE KEY `Kode` (`Kode`);

--
-- Indexes for table `semester`
--
ALTER TABLE `semester`
  ADD PRIMARY KEY (`Id_Semester`);

--
-- Indexes for table `skripsi`
--
ALTER TABLE `skripsi`
  ADD PRIMARY KEY (`Id_Skripsi`),
  ADD UNIQUE KEY `Kode_Skripsi` (`Kode_Skripsi`),
  ADD UNIQUE KEY `NIM` (`NIM`);

--
-- Indexes for table `ujiansusulan`
--
ALTER TABLE `ujiansusulan`
  ADD PRIMARY KEY (`Id_formSusulan`),
  ADD KEY `NIM` (`NIM`),
  ADD KEY `Nama_Matkul` (`Nama_Matkul`),
  ADD KEY `Kode_Ujian` (`Kode_Form`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `Id_Admin` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `daftarproposal`
--
ALTER TABLE `daftarproposal`
  MODIFY `Id_Proposal` int(4) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `daftarubahjudul`
--
ALTER TABLE `daftarubahjudul`
  MODIFY `Id_Judul` int(4) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jadwalsusulan`
--
ALTER TABLE `jadwalsusulan`
  MODIFY `Id_Susulan` int(4) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jadwalujian`
--
ALTER TABLE `jadwalujian`
  MODIFY `Id_Ujian` int(4) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jurusan`
--
ALTER TABLE `jurusan`
  MODIFY `Id_Jurusan` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `keys`
--
ALTER TABLE `keys`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `limits`
--
ALTER TABLE `limits`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  MODIFY `Id_Mahasiswa` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `matkul`
--
ALTER TABLE `matkul`
  MODIFY `Id_Matkul` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=211;

--
-- AUTO_INCREMENT for table `program`
--
ALTER TABLE `program`
  MODIFY `Id_Program` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `semester`
--
ALTER TABLE `semester`
  MODIFY `Id_Semester` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `skripsi`
--
ALTER TABLE `skripsi`
  MODIFY `Id_Skripsi` int(2) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ujiansusulan`
--
ALTER TABLE `ujiansusulan`
  MODIFY `Id_formSusulan` int(4) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `daftarproposal`
--
ALTER TABLE `daftarproposal`
  ADD CONSTRAINT `daftarproposal_ibfk_1` FOREIGN KEY (`NIM`) REFERENCES `mahasiswa` (`NIM`);

--
-- Constraints for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  ADD CONSTRAINT `mahasiswa_ibfk_1` FOREIGN KEY (`Kode_Program`) REFERENCES `program` (`Kode`),
  ADD CONSTRAINT `mahasiswa_ibfk_2` FOREIGN KEY (`Kode_Jurusan`) REFERENCES `jurusan` (`Kode_Jurusan`);

--
-- Constraints for table `matkul`
--
ALTER TABLE `matkul`
  ADD CONSTRAINT `matkul_ibfk_1` FOREIGN KEY (`Kode_Jurusan`) REFERENCES `jurusan` (`Kode_Jurusan`);

--
-- Constraints for table `skripsi`
--
ALTER TABLE `skripsi`
  ADD CONSTRAINT `skripsi_ibfk_1` FOREIGN KEY (`NIM`) REFERENCES `mahasiswa` (`NIM`);

--
-- Constraints for table `ujiansusulan`
--
ALTER TABLE `ujiansusulan`
  ADD CONSTRAINT `ujiansusulan_ibfk_1` FOREIGN KEY (`NIM`) REFERENCES `mahasiswa` (`NIM`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
