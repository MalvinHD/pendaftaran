    var scroll;
    $(window).scroll(function(){
        scroll = $(window).scrollTop();
        if (scroll > 100) {
            $(".custom").addClass("py-0");
            $(".custom").removeClass("py-3");
            $(".custom").css({"border-radius":"0 0 50px 50px"});
        }else{
            $(".custom").addClass("py-3");
            $(".custom").removeClass("py-0");
            $(".custom").css({"border-radius":"0 0 0 0"});
        }
    });

// Material Design example
$(document).ready(function () {
    
    $('.mdb-select.select-wrapper .select-dropdown').val("").removeAttr('readonly').attr("placeholder", "Klik untuk memilih").prop('required', true).addClass('form-control').css('background-color', '#fff');
    
    function prosesBuat(){
        $.ajax({
            type: "POST",
            url: "cariDataMatkul", 
            data: {
                Kode_Jurusan : $("#selectJurusan").val(), 
                Semester : $("#selectSemester").val()
            },
            dataType: "json",
            success: function(response){ // Ketika proses pengiriman berhasila
              $("#selectMatkul").html(response.list_matkul);
            },
            error: function (xhr, ajaxOptions, thrownError) { // Ketika ada error
              alert(xhr.status + "\n" + xhr.responseText + "\n" + thrownError); // Munculkan alert error
            }
        });
    }

    $("#selectJurusan").change(function(){
        prosesBuat();
    });

    $("#selectSemester").change(function(){
        prosesBuat();
    });

    AOS.init();
});