// Material Design example
$(document).ready(function () {
  $('#dtMaterialDesignExample').DataTable();
  $('#dtMaterialDesignExample_wrapper').find('label').each(function () {
    $(this).parent().append($(this).children());
  });
  $('#dtMaterialDesignExample_wrapper .dataTables_length').parent().removeClass('col-sm-12');
  $('#dtMaterialDesignExample_wrapper .dataTables_filter').parent().removeClass('col-sm-12');
  $('#dtMaterialDesignExample_wrapper .dataTables_filter').find('input').each(function () {
    const $this = $(this); 
    $this.attr("placeholder", "Search");
    $this.addClass("font-light");
    $this.removeClass('form-control-sm');
  });
  $('#dtMaterialDesignExample_wrapper .dataTables_length').addClass('col-md-3 md-form font-light');
  $('#dtMaterialDesignExample_wrapper .dataTables_length label').addClass('d-none');
  $('#dtMaterialDesignExample_wrapper .dataTables_filter').addClass('md-form');
  $('#dtMaterialDesignExample_wrapper select').removeClass('custom-select custom-select-sm form-control form-control-sm');
  $('#dtMaterialDesignExample_wrapper select').addClass('mdb-select');
  $('#dtMaterialDesignExample_wrapper .dataTables_filter').find('label').remove();

//MANAGEMEN
  $('#datatableMatkulMN').DataTable();
  $('#datatableMatkulMN_wrapper').find('label').each(function () {
    $(this).parent().append($(this).children());
  });
  $('#datatableMatkulMN_wrapper .dataTables_length').parent().removeClass('col-sm-12');
  $('#datatableMatkulMN_wrapper .dataTables_filter').parent().removeClass('col-sm-12');
  $('#datatableMatkulMN_wrapper .dataTables_filter').find('input').each(function () {
    const $this = $(this); 
    $this.attr("placeholder", "Search");
    $this.addClass("font-light");
    $this.removeClass('form-control-sm');
  });
  $('#datatableMatkulMN_wrapper .dataTables_length').addClass('col-md-5 md-form font-light');
  $('#datatableMatkulMN_wrapper .dataTables_length label').addClass('d-none');
  $('#datatableMatkulMN_wrapper .dataTables_filter').addClass('md-form');
  $('#datatableMatkulMN_wrapper select').removeClass('custom-select custom-select-sm form-control form-control-sm');
  $('#datatableMatkulMN_wrapper select').addClass('mdb-select');
  $('#datatableMatkulMN_wrapper .dataTables_filter').find('label').remove();



//AKUNTANSI
  $('#datatableMatkulAK').DataTable();
  $('#datatableMatkulAK_wrapper').find('label').each(function () {
    $(this).parent().append($(this).children());
  });
  $('#datatableMatkulAK_wrapper .dataTables_length').parent().removeClass('col-sm-12');
  $('#datatableMatkulAK_wrapper .dataTables_filter').parent().removeClass('col-sm-12');
  $('#datatableMatkulAK_wrapper .dataTables_filter').find('input').each(function () {
    const $this = $(this); 
    $this.attr("placeholder", "Search");
    $this.addClass("font-light");
    $this.removeClass('form-control-sm');
  });
  $('#datatableMatkulAK_wrapper .dataTables_length').addClass('col-md-5 md-form font-light');
  $('#datatableMatkulAK_wrapper .dataTables_length label').addClass('d-none');
  $('#datatableMatkulAK_wrapper .dataTables_filter').addClass('md-form');
  $('#datatableMatkulAK_wrapper select').removeClass('custom-select custom-select-sm form-control form-control-sm');
  $('#datatableMatkulAK_wrapper select').addClass('mdb-select');
  $('#datatableMatkulAK_wrapper .dataTables_filter').find('label').remove();

//MATEMATIKA
  $('#datatableMatkulMA').DataTable();
  $('#datatableMatkulMA_wrapper').find('label').each(function () {
    $(this).parent().append($(this).children());
  });
  $('#datatableMatkulMA_wrapper .dataTables_length').parent().removeClass('col-sm-12');
  $('#datatableMatkulMA_wrapper .dataTables_filter').parent().removeClass('col-sm-12');
  $('#datatableMatkulMA_wrapper .dataTables_filter').find('input').each(function () {
    const $this = $(this); 
    $this.attr("placeholder", "Search");
    $this.addClass("font-light");
    $this.removeClass('form-control-sm');
  });
  $('#datatableMatkulMA_wrapper .dataTables_length').addClass('col-md-5 md-form font-light');
  $('#datatableMatkulMA_wrapper .dataTables_length label').addClass('d-none');
  $('#datatableMatkulMA_wrapper .dataTables_filter').addClass('md-form');
  $('#datatableMatkulMA_wrapper select').removeClass('custom-select custom-select-sm form-control form-control-sm');
  $('#datatableMatkulMA_wrapper select').addClass('mdb-select');
  $('#datatableMatkulMA_wrapper .dataTables_filter').find('label').remove();


//INFORMATIKA

  $('#datatableMatkulTI').DataTable();
  $('#datatableMatkulTI_wrapper').find('label').each(function () {
    $(this).parent().append($(this).children());
  });
  $('#datatableMatkulTI_wrapper .dataTables_length').parent().removeClass('col-sm-12');
  $('#datatableMatkulTI_wrapper .dataTables_filter').parent().removeClass('col-sm-12');
  $('#datatableMatkulTI_wrapper .dataTables_filter').find('input').each(function () {
    const $this = $(this); 
    $this.attr("placeholder", "Search");
    $this.addClass("font-light");
    $this.removeClass('form-control-sm');
  });
  $('#datatableMatkulTI_wrapper .dataTables_length').addClass('col-md-5 md-form font-light');
  $('#datatableMatkulTI_wrapper .dataTables_length label').addClass('d-none');
  $('#datatableMatkulTI_wrapper .dataTables_filter').addClass('md-form');
  $('#datatableMatkulTI_wrapper select').removeClass('custom-select custom-select-sm form-control form-control-sm');
  $('#datatableMatkulTI_wrapper select').addClass('mdb-select');
  $('#datatableMatkulTI_wrapper .dataTables_filter').find('label').remove();


//SISTEM INFORMASI

  $('#datatableMatkulSI').DataTable();
  $('#datatableMatkulSI_wrapper').find('label').each(function () {
    $(this).parent().append($(this).children());
  });
  $('#datatableMatkulSI_wrapper .dataTables_length').parent().removeClass('col-sm-12');
  $('#datatableMatkulSI_wrapper .dataTables_filter').parent().removeClass('col-sm-12');
  $('#datatableMatkulSI_wrapper .dataTables_filter').find('input').each(function () {
    const $this = $(this); 
    $this.attr("placeholder", "Search");
    $this.addClass("font-light");
    $this.removeClass('form-control-sm');
  });
  $('#datatableMatkulSI_wrapper .dataTables_length').addClass('col-md-5 md-form font-light');
  $('#datatableMatkulSI_wrapper .dataTables_length label').addClass('d-none');
  $('#datatableMatkulSI_wrapper .dataTables_filter').addClass('md-form');
  $('#datatableMatkulSI_wrapper select').removeClass('custom-select custom-select-sm form-control form-control-sm');
  $('#datatableMatkulSI_wrapper select').addClass('mdb-select');
  $('#datatableMatkulSI_wrapper .dataTables_filter').find('label').remove();


//ILMU KOMUNIKASI

  $('#datatableMatkulIK').DataTable();
  $('#datatableMatkulIK_wrapper').find('label').each(function () {
    $(this).parent().append($(this).children());
  });
  $('#datatableMatkulIK_wrapper .dataTables_length').parent().removeClass('col-sm-12');
  $('#datatableMatkulIK_wrapper .dataTables_filter').parent().removeClass('col-sm-12');
  $('#datatableMatkulIK_wrapper .dataTables_filter').find('input').each(function () {
    const $this = $(this); 
    $this.attr("placeholder", "Search");
    $this.addClass("font-light");
    $this.removeClass('form-control-sm');
  });
  $('#datatableMatkulIK_wrapper .dataTables_length').addClass('col-md-5 md-form font-light');
  $('#datatableMatkulIK_wrapper .dataTables_length label').addClass('d-none');
  $('#datatableMatkulIK_wrapper .dataTables_filter').addClass('md-form');
  $('#datatableMatkulIK_wrapper select').removeClass('custom-select custom-select-sm form-control form-control-sm');
  $('#datatableMatkulIK_wrapper select').addClass('mdb-select');
  $('#datatableMatkulIK_wrapper .dataTables_filter').find('label').remove();


//DESAIN KOMUNIKASI VISUAL

  $('#datatableMatkulDV').DataTable();
  $('#datatableMatkulDV_wrapper').find('label').each(function () {
    $(this).parent().append($(this).children());
  });
  $('#datatableMatkulDV_wrapper .dataTables_length').parent().removeClass('col-sm-12');
  $('#datatableMatkulDV_wrapper .dataTables_filter').parent().removeClass('col-sm-12');
  $('#datatableMatkulDV_wrapper .dataTables_filter').find('input').each(function () {
    const $this = $(this); 
    $this.attr("placeholder", "Search");
    $this.addClass("font-light");
    $this.removeClass('form-control-sm');
  });
  $('#datatableMatkulDV_wrapper .dataTables_length').addClass('col-md-5 md-form font-light');
  $('#datatableMatkulDV_wrapper .dataTables_length label').addClass('d-none');
  $('#datatableMatkulDV_wrapper .dataTables_filter').addClass('md-form');
  $('#datatableMatkulDV_wrapper select').removeClass('custom-select custom-select-sm form-control form-control-sm');
  $('#datatableMatkulDV_wrapper select').addClass('mdb-select');
  $('#datatableMatkulDV_wrapper .dataTables_filter').find('label').remove();


//ARSITEKTUR

  $('#datatableMatkulAT').DataTable();
  $('#datatableMatkulAT_wrapper').find('label').each(function () {
    $(this).parent().append($(this).children());
  });
  $('#datatableMatkulAT_wrapper .dataTables_length').parent().removeClass('col-sm-12');
  $('#datatableMatkulAT_wrapper .dataTables_filter').parent().removeClass('col-sm-12');
  $('#datatableMatkulAT_wrapper .dataTables_filter').find('input').each(function () {
    const $this = $(this); 
    $this.attr("placeholder", "Search");
    $this.addClass("font-light");
    $this.removeClass('form-control-sm');
  });
  $('#datatableMatkulAT_wrapper .dataTables_length').addClass('col-md-5 md-form font-light');
  $('#datatableMatkulAT_wrapper .dataTables_length label').addClass('d-none');
  $('#datatableMatkulAT_wrapper .dataTables_filter').addClass('md-form');
  $('#datatableMatkulAT_wrapper select').removeClass('custom-select custom-select-sm form-control form-control-sm');
  $('#datatableMatkulAT_wrapper select').addClass('mdb-select');
  $('#datatableMatkulAT_wrapper .dataTables_filter').find('label').remove();

  $('.mdb-select').materialSelect();   
});

