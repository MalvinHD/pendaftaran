<?php
class Api_Model extends CI_model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->helper('date');
    }

    public function getAllData($x, $y = '', $z = '')
    {
        switch ($x) {
            case 1:
                if($y!=''){
                    $this->db->select("Id_Mahasiswa, mahasiswa.NIM, mahasiswa.Nama, mahasiswa.Kode_Jurusan, Semester, Email, Telepon, Kode_Program, Fakultas, Nama_Jurusan, Program, Kelas");
                    $this->db->from('mahasiswa');
                    $this->db->join("jurusan", "mahasiswa.Kode_Jurusan = jurusan.Kode_Jurusan");
                    $this->db->join("program", "mahasiswa.Kode_Program = program.Kode");
                    $this->db->where('Id_Mahasiswa', $y);
                    return $this->db->get()->row_array();
                    break;
                }else{
                    $this->db->select("Id_Mahasiswa, mahasiswa.NIM, mahasiswa.Nama, mahasiswa.Kode_Jurusan, Semester, Email, Telepon, Kode_Program, Fakultas, Nama_Jurusan, Program, Kelas");
                    $this->db->from('mahasiswa');
                    $this->db->join("jurusan", "mahasiswa.Kode_Jurusan = jurusan.Kode_Jurusan");
                    $this->db->join("program", "mahasiswa.Kode_Program = program.Kode");
                    return $this->db->get()->result_array();
                    break;
                }

            case 2:
                if($y!=''){
                    $this->db->select("*");
                    $this->db->from('jadwalujian');
                    $this->db->join("jurusan", "jurusan.Kode_Jurusan = jadwalujian.Kode_Jurusan");
                    $this->db->where('Id_Ujian', $y);
                    return $this->db->get()->row_array();
                    break;
                }else{
                    $this->db->select("*");
                    $this->db->from('jadwalujian');
                    $this->db->join("jurusan", "jurusan.Kode_Jurusan = jadwalujian.Kode_Jurusan");
                    return $this->db->get()->result_array();
                    break; 
                }

            case 3:
                if($y!=''){
                    $this->db->select("*");
                    $this->db->from('skripsi');
                    $this->db->join("mahasiswa", "mahasiswa.NIM = skripsi.NIM");
                    $this->db->where('Id_Skripsi', $y);
                    return $this->db->get()->row_array();
                    break;
                }else{
                    if($z!='')
                    {
                        $this->db->select("*");
                        $this->db->from('skripsi');
                        $this->db->join("mahasiswa", "mahasiswa.NIM = skripsi.NIM");
                        $this->db->where('skripsi.NIM', $z);
                        return $this->db->get()->row_array();
                        break;

                    }
                    else
                    {
                        $this->db->select("*");
                        $this->db->from('skripsi');
                        $this->db->join("mahasiswa", "mahasiswa.NIM = skripsi.NIM");
                        return $this->db->get()->result_array();
                        break;
                    }
                        
                }
                
            case 4:
                if($y!=''){
                    $this->db->select("*");
                    $this->db->from('daftarproposal');
                    $this->db->join("mahasiswa", "daftarproposal.NIM = mahasiswa.NIM");
                    $this->db->join("jurusan", "mahasiswa.Kode_Jurusan = jurusan.Kode_Jurusan");
                    $this->db->join("program", "mahasiswa.Kode_Program = program.Kode");
                    $this->db->where('Id_Proposal', $y);
                    return $this->db->get()->row_array();
                    break;
                }else{
                    $this->db->select("*");
                    $this->db->from('daftarproposal');
                    $this->db->join("mahasiswa", "daftarproposal.NIM = mahasiswa.NIM");
                    $this->db->join("jurusan", "mahasiswa.Kode_Jurusan = jurusan.Kode_Jurusan");
                    $this->db->join("program", "mahasiswa.Kode_Program = program.Kode");
                    return $this->db->get()->result_array();
                    break;
                }


            case 5:
                if($y!=''){
                    return $this->db->get_where('admin',['Email' => $y])->row_array();
                    break;
                }
                else{
                    $this->db->select('Id_Admin, Kode_Admin, Nama, Email, Jabatan');
                    $this->db->from('admin');
                    return $this->db->get()->result_array();
                    break;
                }

            case 6:
                return $this->db->get('matkul')->result_array();
                break;

            case 7:
                if($y!=''){
                    return $this->db->get_where('jurusan', ['Fakultas' => $y])->result_array();
                    break;
                }else{
                    return $this->db->get('jurusan')->result_array();
                    break;                
                }
                

            case 8:
                if($y!='' && $z!=''){
                    return $this->db->get_where('program',['Program' => $y,'Kelas' => $z])->row_array();
                    break;
                }else{
                    return $this->db->get('program')->result_array();
                    break;
                }

            case 9:
                if($y!=''){
                    return $this->db->get_where('daftarubahjudul', ["Kode_Skripsi" => $y])->row_array();
                    break;
                }else{
                    $this->db->select("*");
                    $this->db->from('daftarubahjudul');
                    $this->db->join("skripsi", "daftarubahjudul.Kode_Skripsi = skripsi.Kode_Skripsi");
                    $this->db->join("mahasiswa", "daftarubahjudul.NIM = mahasiswa.NIM");
                    $this->db->join("jurusan", "mahasiswa.Kode_Jurusan = jurusan.Kode_Jurusan");
                    $this->db->join("program", "mahasiswa.Kode_Program = program.Kode");
                    return $this->db->get()->result_array();
                    break;
                }

            case 10:
                if($y!=''){
                    $this->db->select("*");
                    $this->db->from('ujiansusulan');
                    $this->db->join("mahasiswa", "ujiansusulan.NIM = mahasiswa.NIM");
                    $this->db->join("jurusan", "mahasiswa.Kode_Jurusan = jurusan.Kode_Jurusan");
                    $this->db->join("program", "mahasiswa.Kode_Program = program.Kode");
                    $this->db->where("ujiansusulan.NIM", $y);
                    return $this->db->get()->result_array();
                    break;

                }
                else{
                    $this->db->select("*");
                    $this->db->from('ujiansusulan');
                    $this->db->join("mahasiswa", "ujiansusulan.NIM = mahasiswa.NIM");
                    $this->db->join("jurusan", "mahasiswa.Kode_Jurusan = jurusan.Kode_Jurusan");
                    $this->db->join("program", "mahasiswa.Kode_Program = program.Kode");
                    return $this->db->get()->result_array();
                    break;
                }
                

            case 11:
                $this->db->select("*");
                $this->db->from('jadwalsusulan');
                $this->db->join("mahasiswa", "mahasiswa.NIM = jadwalsusulan.NIM");
                $this->db->join("jurusan", "mahasiswa.Kode_Jurusan = jurusan.Kode_Jurusan");
                $this->db->join("jadwalujian", "jadwalujian.Kode_Ujian = jadwalsusulan.Kode_Ujian");
                return $this->db->get()->result_array();
                break;

            case 12:
                if($z!='' && $y!=''){
                    return $this->db->get_where('matkul',['Kode_Jurusan' => $y, 'Semester' => $z])->result();    
                    break;            
                }
                else{
                    if ($z!='') {
                        return $this->db->get_where('matkul',['Semester' => $z])->result();    
                        break;                   
                    }
                    else{
                        return $this->db->get_where('matkul',['Kode_Jurusan' => $y])->result();    
                        break;                   
                    }
                }

            case 13:
                if ($z!='') {
                    return $this->db->get_where('daftarubahjudul', ["NIM" => $y, "Status" => $z])->result_array();    
                    break;
                } else {
                    return $this->db->get_where('daftarubahjudul', ["NIM" => $y])->result_array();    
                    break;
                }      
                

            case 14:
                if($z!=''){
                    return $this->db->get_where('daftarproposal', ["NIM" => $y, "Status" => $z])->result_array();
                    break;
                }
                else{
                    return $this->db->get_where('daftarproposal', ["NIM" => $y])->result_array();
                    break;
                }

            case 15:
                return $this->db->get_where('admin', ["Kode_Admin" => $y])->row_array();
                break;

            case 16:
                return $this->db->get_where('mahasiswa', ["NIM" => $y])->row_array();
                break;

            case 17:
                return $this->db->get_where('jurusan', ['Kode_Jurusan' => $y])->row_array();
                break;
                
            case 18:
                return $this->db->get_where('program', ['Kode' => $y])->row_array();
                break;

            case 19:
                return $this->db->get_where('admin', ["Status" => $y])->row_array();
                break;      

            case 20:
                return $this->db->get_where('jadwalujian', ["Kode_Ujian" => $y])->row_array();
                break;   

            case 21:               
                return $this->db->get_where('admin', ["Id_Admin" => $y])->row_array();
                break;   

            case 22:
                return $this->db->get_where('mahasiswa', ["Id_Mahasiswa" => $y])->row_array();
                break;

        }
    }



    public function create($tipe,$data)
    {
        switch ($tipe)
        {
            case 1:
                $this->db->insert('mahasiswa', $data);
                return $this->db->affected_rows();        
                break;

            case 2:
                $this->db->insert('jadwalujian', $data);
                return $this->db->affected_rows();        
                break;

            case 3:
                $this->db->insert('skripsi', $data);
                return $this->db->affected_rows();        
                break;

            case 4:
                $this->db->insert('daftarproposal', $data);
                return $this->db->affected_rows();        
                break;

            case 5:
                $this->db->insert('admin', $data);
                return $this->db->affected_rows();        
                break;

            case 6:
                $this->db->insert('matkul', $data);
                return $this->db->affected_rows();        
                break;

            case 7:
                $this->db->insert('jadwalsusulan', $data);
                return $this->db->affected_rows();        
                break;

            case 8:
                $this->db->insert('daftarubahjudul', $data);
                return $this->db->affected_rows();        
                break;

            case 9:
                $this->db->insert('ujiansusulan', $data);
                return $this->db->affected_rows();        
                break;
        }
    }

    public function update($tipe,$data,$id)
    {
        switch ($tipe)
        {
            case 1:
                $this->db->update('mahasiswa', $data, ['Id_Mahasiswa' => $id]);
                return $this->db->affected_rows();        
                break;

            case 2:
                $this->db->update('jadwalujian', $data, ['Id_Ujian' => $id]);
                return $this->db->affected_rows();        
                break;

            case 3:
                $this->db->update('skripsi', $data, ['Kode_Skripsi' => $id]);
                return $this->db->affected_rows();        
                break;

            case 4:
                $this->db->update('daftarproposal', $data, ['Kode_Skripsi' => $id]);
                return $this->db->affected_rows();        
                break;

            case 5:
                $this->db->update('admin', $data, ['Id_Admin' => $id]);
                return $this->db->affected_rows();        
                break;            

            case 6:
                $this->db->update('matkul', $data, ['Id_Matkul' => $id]);
                return $this->db->affected_rows();        
                break;     

            case 7:
                $this->db->update('daftarubahjudul', $data, ['Kode_Skripsi' => $id]);
                return $this->db->affected_rows();        
                break;     

            case 8:
                $this->db->update('ujiansusulan', $data, ['Kode_Form' => $id]);
                return $this->db->affected_rows();        
                break;     
        }
    }
    
    public function delete($tipe, $id)
    {
        switch ($tipe)
        {
            case 1:
                $this->db->delete('mahasiswa',['Id_Mahasiswa' => $id]);
                return $this->db->affected_rows();        
                break;

            case 2:
                $this->db->delete('jadwalujian',['Id_Ujian' => $id]);
                return $this->db->affected_rows();        
                break;

            case 3:
                $this->db->delete('skripsi',['Id_Skripsi' => $id]);
                return $this->db->affected_rows();        
                break;

            case 4:
                $this->db->delete('daftarproposal',['Id_Proposal' => $id]);
                return $this->db->affected_rows();        
                break;

            case 5:
                $this->db->delete('admin',['Id_Admin' => $id]);
                return $this->db->affected_rows();        
                break;

            case 6:
                $this->db->delete('matkul',['Id_Matkul' => $id]);
                return $this->db->affected_rows();        
                break;

        }
    }
}
