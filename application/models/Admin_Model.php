<?php

use GuzzleHttp\Client;

class Admin_Model extends CI_model
{

    private $_client;

    public function __construct()
    {
        parent::__construct();
        $headers = [
            'Authorization' => $this->session->userdata('token'),        
            'Accept'        => 'application/json',

        ];
        $this->load->database();
        $this->load->helper('date');
        $this->_client = new Client([
            'base_uri' => 'https://devprayoga.com/layanan_mahasiswa/api/',
            'headers' => $headers
        ]);
        
    }


    public function loginadmin($x,$y)
    {

        $data = [
            'LM-TOKEN' => '1234',
            'kode' => $x,
            'password' => $y,
        ];

        $check = $this->_client->request('POST', 'loginadmin',[
            'form_params' => $data
        ]);
        $result = json_decode($check->getBody()->getContents(),true);
        return $result;
    }


    public function getAllData($x,$y='',$z='')
    {
        switch ($x) 
        {
            case 1:
                $response = $this->_client->request('GET', 'matkul', [
                    'query' => [
                        'LM-TOKEN' => '1234'                        
                    ]
                ]);

                $result = json_decode($response->getBody()->getContents(), true);
                return $result['data'];
                break;

            case 2:
                $response = $this->_client->request('GET', 'admin', [
                    'query' => [
                        'LM-TOKEN' => '1234'
                    ]
                ]);

                $result = json_decode($response->getBody()->getContents(), true);
                return $result['data'];
                break;

            case 3:
                $response = $this->_client->request('GET', 'mahasiswa', [
                    'query' => [
                        'LM-TOKEN' => '1234'
                    ]
                ]);

                $result = json_decode($response->getBody()->getContents(), true);
                return $result['data'];
                break;

            case 4:
                $response = $this->_client->request('GET', 'program', [
                    'query' => [
                        'LM-TOKEN' => '1234',
                        'program' => $y,
                        'kelas' => $z
                    ]
                ]);

                $result = json_decode($response->getBody()->getContents(), true);
                return $result['data'];
                break;

            case 5:
                if($y!=''){
                    $response = $this->_client->request('GET', 'jurusan', [
                        'query' => [
                            'LM-TOKEN' => '1234',
                            'fakultas' => $y
                        ]
                    ]);

                    $result = json_decode($response->getBody()->getContents(), true);
                    return $result['data'];
                    break;
                }

                else{
                    $response = $this->_client->request('GET', 'jurusan', [
                        'query' => [
                            'LM-TOKEN' => '1234'
                        ]
                    ]);

                    $result = json_decode($response->getBody()->getContents(), true);
                    return $result['data'];
                    break;
                }

            case 6:
                $response = $this->_client->request('GET', 'listskripsi', [
                    'query' => [
                        'LM-TOKEN' => '1234'
                    ]
                ]);

                $result = json_decode($response->getBody()->getContents(), true);
                return $result;
                break;

            case 7:
                $response = $this->_client->request('GET', 'daftarubahjudul', [
                    'query' => [
                        'LM-TOKEN' => '1234'
                    ]
                ]);

                $result = json_decode($response->getBody()->getContents(), true);
                return $result;
                break;
                

            case 8:
                $response = $this->_client->request('GET', 'skripsi', [
                    'query' => [
                        'LM-TOKEN' => '1234'
                    ]
                ]);
                $result = json_decode($response->getBody()->getContents(), true);
                return $result;
                break;

            case 11:
                $response = $this->_client->request('GET', 'ujiansusulan', [
                    'query' => [
                        'LM-TOKEN' => '1234'
                    ]
                ]);
                $result = json_decode($response->getBody()->getContents(), true);
                return $result;
                break;
                
            
            case 12:
                $response = $this->_client->request('GET', 'jadwalsusulan', [
                    'query' => [
                        'LM-TOKEN' => '1234'
                    ]
                ]);
                $result = json_decode($response->getBody()->getContents(), true);
                return $result;
                break;
                

            case 13:
                $response = $this->_client->request('GET', 'jadwalujian', [
                    'query' => [
                        'LM-TOKEN' => '1234'
                    ]
                ]);
                $result = json_decode($response->getBody()->getContents(), true);
                return $result;
                break;    

            case 14:
                if($z!='' && $y!=''){
                    $response = $this->_client->request('GET', 'matkul', [
                        'query' => [
                            'LM-TOKEN' => '1234',
                            'jurusan' => $y,
                            'semester' => $z
                        ]
                    ]);
                    
                    $result = json_decode($response->getBody()->getContents(), true);
                    return $result['data'];
                    break;
                }
                else{
                    if ($z!='') {
                        $response = $this->_client->request('GET', 'matkul', [
                            'query' => [
                                'LM-TOKEN' => '1234',
                                'semester' => $z
                            ]
                        ]);

                        $result = json_decode($response->getBody()->getContents(), true);
                        return $result['data'];
                        break;

                    }
                    else{
                        $response = $this->_client->request('GET', 'matkul', [
                            'query' => [
                                'LM-TOKEN' => '1234',
                                'jurusan' => $y                           
                            ]
                        ]);

                        $result = json_decode($response->getBody()->getContents(), true);
                        return $result['data'];
                        break;
                    }
                }

            case 15:
                $response = $this->_client->request('GET', 'adminforget', [
                    'query' => [
                        'LM-TOKEN' => '1234',                
                        'email' => $y
                    ]
                ]);

                $result = json_decode($response->getBody()->getContents(), true);
                return $result;
                break;

            case 16:
                $response = $this->_client->request('GET', 'status', [
                    'query' => [
                        'LM-TOKEN' => '1234',                
                        'status' => $y
                    ]
                ]);

                $result = json_decode($response->getBody()->getContents(), true);
                return $result;
                break;

            case 17:
                $response = $this->_client->request('GET', 'jadwalujiankode', [
                    'query' => [
                        'LM-TOKEN' => '1234',                
                        'kode' => $y
                    ]
                ]);

                $result = json_decode($response->getBody()->getContents(), true);
                return $result;
                break;

            case 18:
                $response = $this->_client->request('GET', 'mahasiswanim', [
                    'query' => [
                        'LM-TOKEN' => '1234',                
                        'nim' => $y
                    ]
                ]);

                $result = json_decode($response->getBody()->getContents(), true);
                return $result;
                break;

            case 19:
                $response = $this->_client->request('GET', 'adminid', [
                    'query' => [
                        'LM-TOKEN' => '1234',                
                        'id' => $y
                    ]
                ]);

                $result = json_decode($response->getBody()->getContents(), true);
                return $result;
                break;

        }        
    }


    public function tambahMahasiswa($x)
    {
        $data =
        [
            "password" => password_hash($this->input->post('nim'), PASSWORD_DEFAULT),
            "nim" => $this->input->post('nim'),
            "nama" => $this->input->post('nama'),                 
            "jurusan" => $this->input->post('jurusan'),
            "semester" => $this->input->post('semester'),
            "email" => $this->input->post('email'),
            "telepon" => $this->input->post('telepon'),
            "kode" => $x,
            "LM-TOKEN" => '1234'
        ];

        $response = $this->_client->request('POST','Mahasiswa',[
            'form_params' => $data
        ]);

        $result = json_decode($response->getBody()->getContents());
        return $result;
    }


    public function editMahasiswa($x)
    {
        $data = 
        [
            "nim" => $this->input->post('nim'),
            "nama" => $this->input->post('nama'),                 
            "jurusan" => $this->input->post('jurusan'),
            "semester" => $this->input->post('semester'),
            "email" => $this->input->post('email'),
            "telepon" => $this->input->post('telepon'),
            "kode" => $x,
            "id" => $this->input->post('Id'),
            "LM-TOKEN" => '1234'
        ];

        $response = $this->_client->request('PUT','Mahasiswa',[
            'form_params' => $data
        ]);

        $result = json_decode($response->getBody()->getContents());
        return $result;
    }


    public function tambahAdmin()
    {
        $data =
        [   
            "kode" => $this->input->post('kode'),
            "nama" => $this->input->post('nama'),
            "email" => $this->input->post('email'),
            "jabatan" => $this->input->post('jabatan'),                 
            "password" => $this->input->post('password'),
            'LM-TOKEN' => '1234'
        ];

        $response = $this->_client->request('POST','admin',[
            'form_params' => $data
        ]);

        $result = json_decode($response->getBody()->getContents());
        return $result;
    }

    public function editAdmin()
    {
        $data = 
        [
            "kode" => $this->input->post('kode'),
            "nama" => $this->input->post('nama'),
            "email" => $this->input->post('email'),              
            "jabatan" => $this->input->post('jabatan'),                 
            "id" => $this->input->post('Id'),
            "LM-TOKEN" => '1234'
        ];

        $response = $this->_client->request('PUT','admin',[
            'form_params' => $data
        ]);

        $result = json_decode($response->getBody()->getContents());
        return $result;
        
    }




    public function tambahMatkul()
    {
        $data =
        [   
            "nama" => $this->input->post('nama'),
            "semester" => $this->input->post('semester'),
            "jurusan" => $this->input->post('jurusan'),
            "LM-TOKEN" => '1234'
        ];

        $response = $this->_client->request('POST','matkul',[
            'form_params' => $data
        ]);

        $result = json_decode($response->getBody()->getContents());
        return $result;
    }

    public function editMatkul($id)
    {
        $data = 
        [
            "nama" => $this->input->post('nama'),
            "semester" => $this->input->post('semester'),
            "id" => $id,
            "LM-TOKEN" => '1234'
        ];

        $response = $this->_client->request('PUT','matkul',[
            'form_params' => $data
        ]);

        $result = json_decode($response->getBody()->getContents());
        return $result;
    }

    public function tambahJadwal($x)
    {
        $data =
        [   
            "kode" => $x,
            "matkul" => $this->input->post('matkul'),
            "jurusan" => $this->input->post('jurusan'),
            "semester" => $this->input->post('semester'),
            "tanggal" => $this->input->post('tanggal'),
            "jam" => $this->input->post('jam'),
            "ruang" => $this->input->post('ruang'),
            "LM-TOKEN" => '1234'
        ];

        $response = $this->_client->request('POST','jadwalujian',[
            'form_params' => $data
        ]);

        $result = json_decode($response->getBody()->getContents());
        return $result;
        
    }



    public function editUjian($x,$y)
    {
        if($x == 1)
        {
            date_default_timezone_set('Asia/Jakarta');
            $tanggal = date("Y-m-d");

            $data = [
                "status" => "TERIMA", 
                "tanggal" => $tanggal,
                "kode" => $y,
                "LM-TOKEN" => '1234'
            ];

            $response1 = $this->_client->request('PUT','ujiansusulan',[
                'form_params' => $data
            ]);

            $doto = 
            [   
                "kodeujian" => $this->input->post('kodeujiansusulan'),
                "nim" => $this->input->post('nim'),
                "namamatkul" => $this->input->post('namamatkul'),
                "LM-TOKEN" => '1234'

            ];

            $response2 = $this->_client->request('POST','jadwalsusulan',[
                'form_params' => $doto
            ]);
        }

        elseif($x == 2)
        {
            $data = [
                "status" => "TOLAK",
                "kode" => $y,
                "LM-TOKEN" => '1234'
            ];

            $response = $this->_client->request('PUT','ujiansusulan',[
                'form_params' => $data
            ]);
        }
    }

    public function editJudul($x,$y)
    {
        if($x == 1)
        {
            date_default_timezone_set('Asia/Jakarta');
            $tanggal = date("Y-m-d");

            $data = [
                "status" => "TERIMA", 
                "tanggal" => $tanggal,
                "kode" => $y,
                "LM-TOKEN" => '1234'
            ];
            
            $sukses = $this->_client->request('PUT','daftarubahjudul',[
                            'form_params' => $data
                        ]);            

            $doto = [
                "judulbi" => $this->input->post('judulbi'), 
                "judulen" => $this->input->post('judulen'),
                "kode" => $y,
                "LM-TOKEN" => '1234'
            ];

           $skripsi = $this->_client->request('PUT','skripsi',[
                        'form_params' => $doto
                    ]);
            
        }

        elseif($x == 2)
        {
            $data = [
                "status" => "TOLAK",
                "kode" => $y,
                "LM-TOKEN" => '1234'
            ];
            $gagal = $this->_client->request('PUT','daftarubahjudul',[
                        'form_params' => $data
                    ]);
        }
    }


    public function editProposal($x,$y)
    {
        if($x == 1)
        {
            date_default_timezone_set('Asia/Jakarta');
            $tanggal = date("Y-m-d");

            $data = [
                "status" => "TERIMA", 
                "tanggal" => $tanggal,
                "kode" => $y,
                "LM-TOKEN" => '1234'

            ];

            $sukses = $this->_client->request('PUT','listskripsi',[
                        'form_params' => $data
                    ]);            
            

            $doto = [
                "kode" => $y, 
                "nim" => $this->input->post('nim'), 
                "judulbi" => $this->input->post('judulbi'), 
                "judulen" => $this->input->post('judulen'),
                "LM-TOKEN" => '1234'
            ];

            $kirim = $this->_client->request('POST','skripsi',[
                        'form_params' => $doto
                    ]);            
        }

        elseif($x == 2)
        {
            $data = [
                "status" => "TOLAK",
                "kode" => $y,
                "LM-TOKEN" => '1234'                
            ];

            $gagal = $this->_client->request('PUT','listskripsi',[
                        'form_params' => $data
                    ]);            
            
        }
    }

    public function setStatus($x,$y)
    {
        $data =
        [   
            "LM-TOKEN" => '1234',
            "id" => $x,
            "status" => $y
        ];

        $response = $this->_client->request('PUT','status',[
            'form_params' => $data
        ]);

        $result = json_decode($response->getBody()->getContents());
        return $result;

    }

    public function chgpw()
    {
        $password = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
        $data =
        [   
            "LM-TOKEN" => '1234',
            "id" => $this->input->post('id'),
            "password" => $password,
            "status" => NULL,
        ];

        $response = $this->_client->request('PUT','chgpw',[
            'form_params' => $data
        ]);

        $result = json_decode($response->getBody()->getContents());
        return $result;

    }




    public function hapus($tipe, $id)
    {
        switch ($tipe)
        {
            case 1:
                $response = $this->_client->request('DELETE', 'mahasiswa',[                    
                    'form_params' => [
                        'LM-TOKEN' => '1234',
                        'id' => $id
                    ]
                ]);

                $result = json_decode($response->getBody()->getContents(), true);
                return $result;
                break;

            case 2:
                $response = $this->_client->request('DELETE', 'admin',[
                    
                    'form_params' => [
                        'LM-TOKEN' => '1234',
                        'id' => $id
                    ]
                ]);

                $result = json_decode($response->getBody()->getContents(), true);
                return $result;
                break;

            case 3:
                $response = $this->_client->request('DELETE', 'matkul',[
                    
                    'form_params' => [
                        'LM-TOKEN' => '1234',
                        'id' => $id
                    ]
                ]);

                $result = json_decode($response->getBody()->getContents(), true);
                return $result;
                break;

            case 4:
                $response = $this->_client->request('DELETE', 'jadwalujian',[
                    
                    'form_params' => [
                        'LM-TOKEN' => '1234',
                        'id' => $id
                    ]
                ]);

                $result = json_decode($response->getBody()->getContents(), true);
                return $result;
                break;
        }
    }

}
?>