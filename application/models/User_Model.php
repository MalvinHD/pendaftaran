<?php

use GuzzleHttp\Client;

class User_Model extends CI_model
{

private $_client;

    public function __construct()
    {
        parent::__construct();
        $headers = [
            'Authorization' => $this->session->userdata('token'),        
            'Accept'        => 'application/json',

        ];
        $this->load->database();
        $this->load->helper('date');
        $this->_client = new Client([
            'base_uri' => 'https://devprayoga.com/layanan_mahasiswa/api/',
            'headers' => $headers
        ]);
    }


    public function loginmahasiswa($x,$y)
    {

        $data = [
            'LM-TOKEN' => '1234',
            'nim' => $x,
            'password' => $y,
        ];

        $check = $this->_client->request('POST', 'loginmahasiswa',[
            'form_params' => $data
        ]);
        $result = json_decode($check->getBody()->getContents(),true);
        return $result;
    }

    public function getAllData($x, $y = '', $z = '')
    {
        switch ($x) {
            case 1:
                $response = $this->_client->request('GET', 'mahasiswa', [
                    'query' => [
                        'LM-TOKEN' => '1234',
                        'id' => $y
                    ]
                ]);

                $result = json_decode($response->getBody()->getContents(), true);
                return $result['data'];
                break;

            case 2:
                $response = $this->_client->request('GET', 'jurusan', [
                    'query' => [
                        'LM-TOKEN' => '1234'
                    ]
                ]);

                $result = json_decode($response->getBody()->getContents(), true);
                return $result['data'];
                break;

            case 3:
                $response = $this->_client->request('GET', 'skripsi', [
                    'query' => [
                        'LM-TOKEN' => '1234',
                        'nim' => $y,
                    ]
                ]);
                $result = json_decode($response->getBody()->getContents(), true);
                return $result;
                break;

            case 4:
                $response = $this->_client->request('GET', 'daftarubahjudul', [
                    'query' => [
                        'LM-TOKEN' => '1234',
                        'nim' => $y,
                        'status'=> $z
                    ]
                ]);
                $result = json_decode($response->getBody()->getContents(), true);
                return $result;
                break;
                

            case 5:
                $response = $this->_client->request('GET', 'listskripsi', [
                    'query' => [
                        'LM-TOKEN' => '1234',
                        'nim' => $y,
                        'status'=> $z
                    ]
                ]);
                $result = json_decode($response->getBody()->getContents(), true);
                return $result;
                break;


            case 6:
                $response = $this->_client->request('GET', 'matkul', [
                    'query' => [
                        'LM-TOKEN' => '1234',
                        'semester' => $y,
                        'jurusan' => $z
                    ]
                ]);
                $result = json_decode($response->getBody()->getContents(), true);
                return $result['data'];
                break;
                
            case 7:
                $response = $this->_client->request('GET', 'ujiansusulan', [
                    'query' => [
                        'LM-TOKEN' => '1234',
                        'nim' => $y
                    ]
                ]);
                $result = json_decode($response->getBody()->getContents(), true);
                return $result;
                break;

            case 8:
                $response = $this->_client->request('GET', 'mahasiswapass', [
                    'query' => [
                        'LM-TOKEN' => '1234',
                        'id' => $y
                    ]
                ]);
                $result = json_decode($response->getBody()->getContents(), true);
                return $result['data'];
                break;
        }
    }



    public function change_pass($x)
    {
        $password_hash = password_hash($this->input->post('new_password1'), PASSWORD_DEFAULT);
        $data = 
        [
            "password" => $password_hash,            
            "id" => $x,
            "LM-TOKEN" => '1234'
        ];

        $response = $this->_client->request('PUT','chgpass',[
            'form_params' => $data
        ]);

        $result = json_decode($response->getBody()->getContents());
        return $result;
    }



    public function listGantiJudul()
    {        

        date_default_timezone_set('Asia/Jakarta');
        $tanggal = date("Y-m-d");

        $response = $this->_client->request('GET', 'daftarubahjudul', [
                    'query' => [
                        'LM-TOKEN' => '1234',
                        'kode' => $this->input->post('kode_skripsi')
                    ]
                ]);

        $result = json_decode($response->getBody()->getContents(), true);

        $cek = $result['status'];

        if($cek)
        {
            $data =[
                'LM-TOKEN' => '1234',
                'kode' => $this->input->post('kode_skripsi'),
                "nim" => $this->input->post('nim'),
                "jl1" => $this->input->post('jl1'),
                "jl2" => $this->input->post('jl2'),
                "jb1" => $this->input->post('jb1'),
                "jb2" => $this->input->post('jb2'),
                "alasan" => $this->input->post('alasan'),
                "tanggal" => $tanggal,
                "tahun1" => $this->input->post('tahun1'),
                "tahun2" => $this->input->post('tahun2'),
                "status" => "WAIT"
            ];
            
            $response1 = $this->_client->request('PUT', 'daftarubahjudul', [
                'form_params' => $data
                    
            ]);
        }
        else
        {
            $tanggal = date("Y-m-d");
            $data = [
                'LM-TOKEN' => '1234',
                'kode' => $this->input->post('kode_skripsi'),
                "nim" => $this->input->post('nim'),
                "jl1" => $this->input->post('jl1'),
                "jl2" => $this->input->post('jl2'),
                "jb1" => $this->input->post('jb1'),
                "jb2" => $this->input->post('jb2'),
                "alasan" => $this->input->post('alasan'),
                "tanggal" => $tanggal,
                "tahun1" => $this->input->post('tahun1'),
                "tahun2" => $this->input->post('tahun2'),
                "status" => "WAIT"
            ];

            $response1 = $this->_client->request('POST', 'daftarubahjudul', [
                'form_params' => $data
            ]);
            
        }
    }



    public function listAjuinProposal()
    {
        date_default_timezone_set('Asia/Jakarta');
        $tanggal = date("Y-m-d");

        $response = $this->_client->request('GET', 'listskripsi', [
                    'query' => [
                        'LM-TOKEN' => '1234',
                        'nim' => $this->input->post('nim')
                    ]
                ]);

        $result = json_decode($response->getBody()->getContents(), true);

        $cek = $result['status'];

        if($cek)
        {
            $kode = $result['data'][0];

            $data = [

                "LM-TOKEN" => '1234',
                "kode" => $kode['Kode_Skripsi'],
                'nim' => $this->input->post('nim'),
                "ja" => $this->input->post('ja'),
                "jb" => $this->input->post('jb'),
                "tanggal" => $tanggal,
                "tahunA" => $this->input->post('tahunA'),
                "tahunB" => $this->input->post('tahunB'),
                "status" => "WAIT"
            ];

            $response1 = $this->_client->request('PUT', 'listskripsi', [
                'form_params' => $data
            ]);

            $result = json_decode($response1->getBody()->getContents());
            return $result;

        }
        else
        {   
            $data = [
                "LM-TOKEN" => '1234',                    
                "nim" => $this->input->post('nim'),
                "jurusan" => $this->input->post('jurusan'),
                "ja" => $this->input->post('ja'),
                "jb" => $this->input->post('jb'),
                "tahunA" => $this->input->post('tahunA'),
                "tahunB" => $this->input->post('tahunB'),
                "status" => "WAIT"

            ];     
            $response2 = $this->_client->request('POST', 'listskripsi', [
                'form_params' => $data
            ]);            
            
            $result = json_decode($response2->getBody()->getContents());
            return $result;
        }
    }

    public function tambahUjian($kode)
    {
        date_default_timezone_set('Asia/Jakarta');
        $tanggal = date("Y-m-d");
        
        $data = [
            "LM-TOKEN" => '1234',
            "kode" => $kode,
            "nim" => $this->input->post('nim'),
            "matakuliah" => $this->input->post('matakuliah'),
            "tanggal" => $tanggal,
            "tahunC" => $this->input->post('tahunC'),
            "tahunD" => $this->input->post('tahunD'),
            "alasan" => $this->input->post('alasan'),
            "status" => 'WAIT',
            "bayar" => 'N'
        ];

        $response = $this->_client->request('POST','ujiansusulan', [
                'form_params' => $data
            ]);

        $result = json_decode($response->getBody()->getContents());
        return $result;
    }

    public function bayar($id)
    {
        $data = [
            "LM-TOKEN" => '1234',
            "kode" => $id,
            "bayar" => 'Y'
        ];

        $response = $this->_client->request('PUT','bayarujian', [
                'form_params' => $data
            ]);

        $result = json_decode($response->getBody()->getContents());
        return $result;

    }
}
