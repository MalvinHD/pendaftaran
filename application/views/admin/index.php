<div class="container">
  <!--Section: Content-->
  <section class="my-5 text-center">

      <div class="row col-md-12 py-5">

        <div class="col-md-7 mx-auto d-none d-xl-block">
          <img src="images/backgroundadmin.svg" class="img-fluid">
        </div>
        

        <div class="col-md-5 my-5">
          <!-- Material form login -->          
          <div class="col-md-9 d-inline-block" data-aos="fade-left" data-aos-delay="500">
                <div class="card" style="border-radius: 40px;">
                	<h3 class="card-header white-text text-center py-4 font-bold"  style="border-radius:40px 40px 0 0; background-color: #5e35b1;">
              	     	LOGIN
          	     	</h3>

                  <!--Card content-->
                  <div class="card-body">
                    	<form  method="POST" class="text-center" style="color: #757575;" action="<?= base_url(); ?>Admin">

            		            <div class="md-form my-4">
            		                <input type="text" id="username" class="form-control" name="kodeadmin">
            		                <label for="username">Kode Admin</label>
            		            </div>

            		            <!-- Password -->
            		            <div class="md-form my-4">
            		                <input type="password" id="password" class="form-control" name="password">
            		                <label for="password">Password</label>
            		            </div>

                            <div class="my-4">
                                <div class="col-md-8" style="margin: auto;">
                                  <?php echo $this->session->flashdata('message'); ?>
                                    <button class="btn btnlogin btn-rounded btn-block waves-effect z-depth-0 font-bold" style="color: grey;" type="submit">Sign in</button>  
                                </div>
                                <div class="col-md-12 mt-3" style="margin: auto;">
                                    <h6><a class="font-light black-text forgotpass" style="font-size: 10pt;" data-toggle="modal" data-target="#forgot"><i class="fas fa-lock mx-2"></i>Forgot Password ?</a></h6>  
                                </div>                                
                            </div>      		                        		               
      		            </form>      		        
                  </div>
                </div>
          </div>
          <!-- Material form login -->
        </div>

      </div>
  </section>
  <!--Section: Content-->
</div>


<div id="forgot" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
          <div class="modal-content" style="border-radius: 40px;">

                <div class="modal-header" style="border-radius: 0 30px; background-color: #5e35b1;">
                      <h3 class="modal-title font-bold white-text" id="exampleModalLabel">Forgot Password</h3>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                      </button>
                </div>

                <div class="modal-body">
                      <form action="<?= base_url(); ?>admin/verifForgotPass" method="POST" class="col-md-12 row" enctype="multipart/form-data" novalidate>
                            
                            <div class="col-md-12">                       
                                  <div class="md-form">
                                        <input  id="email" type="text" class="validate form-control font-light" name="email" required>
                                        <label class="font-light" for="email">Email</label>
                                  </div>
                            </div>
                            
                            <div class="modal-footer col-md-12 ">
                                  <button type="submit" class="btn btn-rounded waves-effect waves-light white-text font-bold" style="background-color: #5e35b1;">Send</button>
                            </div>
                      </form>
                </div>

        </div>
    </div>
</div>