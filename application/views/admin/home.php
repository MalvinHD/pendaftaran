<main>
	<div class="container-fluid" style="padding-top: 10%;">
		
		<!-- Nav tabs -->
		<ul class="nav nav-tabs md-tabs nav-justified light-green lighten-1" role="tablist" style="border-radius: 20px;">
			<li class="nav-item">
		    	<a class="nav-link active font-weight-bold" data-toggle="tab" href="#ujianSusulan" role="tab">
		      	<i class="fas fa-book pr-2"></i>Ujian Susulan</a>
		  	</li>
		 	
		 	<li class="nav-item">
		    	<a class="nav-link font-weight-bold" data-toggle="tab" href="#proposalSkripsi" role="tab">
		      	<i class="fas fa-book pr-2"></i>Proposal Skripsi</a>
		  	</li>
		  	
		  	<li class="nav-item">
		    	<a class="nav-link font-weight-bold" data-toggle="tab" href="#perubahanJudul" role="tab">
		      	<i class="fas fa-book	 pr-2"></i>Perubahan Judul Skripsi</a>
		  	</li>
		</ul>
		<!-- Nav tabs -->

		<!-- Tab panels -->
		<div class="tab-content">

			<!-- Panel 1 -->
			<div class="tab-pane fade in show active" id="ujianSusulan" role="tabpanel">
				<div class="card py-3 px-3" style="border-radius: 30px;">
					<table class="table table-borderless table-hover text-center table-responsive">
				        <thead>
				          	<tr>
				          		<th class="font-bold">No</th>
				              	<th class="font-bold">Kode Form</th>
				              	<th class="font-bold">NIM</th>
				              	<th class="font-bold">Nama</th>
				              	<th class="font-bold">Tanggal Diajukan</th>
				              	<th class="font-bold">Status</th>
				              	<th class="font-bold">Option</th>
				          	</tr>
				        </thead>

				        <tbody>
				        	<?php if($ujianSusulan['status']) {?>
				          	<?php $i = 1; foreach ($ujianSusulan['data'] as $uj) : ?>
					          	<tr>	
						          	<td class="align-middle"><?= $i; ?></td>
						            <td class="align-middle"><?= $uj['Kode_Form']?></td>
						            <td class="align-middle"><a href="<?= base_url() ?>admin/dataMahasiswa#<?= $uj['NIM']?>"><?= $uj['NIM']?></a></td>
						           	<td class="align-middle"><?= $uj['Nama']?></td>
						            <td class="align-middle"><?= date("d F Y", strtotime($uj['Tanggal_Diajukan'])); ?></td>					
						            <td class="align-middle"><?= $uj['Status']?></td>
						            <td class="align-middle">
						            	<button class="btn btn-floating info-color waves-effect waves-light" data-toggle="modal" data-target="#detailSusulan<?= $i; ?>"><i class="fas fa-book"></i></button>
						            </td>


						            <div id="detailSusulan<?= $i; ?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
										<div class="modal-dialog modal-lg" role="document">

						    				<div class="modal-content" style="border-radius: 40px;">
						    					<div class="modal-header info-color" style="border-radius: 0 30px;">
											        <h2 class="modal-title font-bold white-text" id="exampleModalLabel"><?= $uj['Nama']; ?></h2>
											        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
											          	<span aria-hidden="true">&times;</span>
											        </button>
											    </div>
											    <div class="modal-body col-md-12">

											    	<form class="needs-validation" action="<?= base_url(); ?>admin/updateUjian/1/<?= $uj['Kode_Form'] ?>" method="POST" enctype="multipart/form-data" novalidate>

											    	<div class="col-md-12 row my-4">
											    		<div class="col-md-4">
												  			<h6 class="font-bold">Tahun Akademik	:</h6>
												  			<h5 class="font-light"><?= $uj['Tahun_Di'] ?>/<?= $uj['Tahun_Ke'] ?></h5>
												  		</div>
												    	<div class="col-md-4">
												  			<h6 class="font-bold">NIM	:</h6>
												  			<h5 class="font-light"><?= $uj['NIM'] ?></h5>
												  		</div>												  		
												  		<div class="col-md-4">
												  			<h6 class="font-bold">Tanggal diajukan	:</h6>
												  			<h5 class="font-light"><?= date("d F Y", strtotime($uj['Tanggal_Diajukan'])); ?></h5>
												  		</div>												  		
												  	</div>
												  	<div class="col-md-12 row my-4">
													  	<div class="col-md-4">
													  		<h6 class="font-bold">Fakultas	:</h6>
													  		<h5 class="font-light"><?= $uj['Fakultas'] ?></h5>
													  	</div> 
												    	<div class="col-md-4">
												  			<h6 class="font-bold">Jurusan	:</h6>
												  			<h5 class="font-light"><?= $uj['Nama_Jurusan'] ?></h5>
												  		</div>
												  		<div class="col-md-4">
												  			<h6 class="font-bold">Nama Mata Kuliah	:</h6>
												  			<h5 class="font-light"><?= $uj['Nama_Matkul'] ?></h5>
												  		</div>  	
											  		</div>

											  		<div class="col-md-12 row my-4">
												  		<div class="col-md-4">
													  		<h6 class="font-bold">Semester	:</h6>
													  		<h5 class="font-light">Semester <?= $uj['Semester'] ?></h5>
													  	</div>
													  	<div class="col-md-4">
													  		<h6 class="font-bold">Program	:</h6>
													  		<h5 class="font-light"><?= $uj['Program'] ?></h5>
													  	</div>
													  	<div class="col-md-4">
													  		<h6 class="font-bold">Kelas	:</h6>
													  		<h5 class="font-light"><?= $uj['Kelas'] ?></h5>
													  	</div>
											  		</div>
											  		<div class="col-md-12 row my-4">
												    	<div class="col-md-4">
												  			<h6 class="font-bold">Email	:</h6>
												  			<h5 class="font-light"><?= $uj['Email'] ?></h5>
												  		</div>  	
												    	<div class="col-md-8">
												  			<h6 class="font-bold">Telepon	:</h6>
												  			<h5 class="font-light"><?= $uj['Telepon'] ?></h5>
												  		</div>  	
											  		</div>
													<div class="col-md-12 my-3">
														<h6 class="font-bold">Alasan	:</h6>
												  		<h5 class="font-light font-italic">"<?= $uj['Alasan'] ?>"</h5>					
											  		</div> 
											  		<div class="col-md-12 my-3">
														<h6 class="font-bold">Status Pembayaran		:</h6>
														<?php if($uj['Bayar']=='N') {?>
												  			<h5 class="font-light">Menunggu proses pembayaran</h5>				
												  		<?php }if($uj['Bayar']=='Y') {?>
												  			<h5 class="font-light">Sudah melakukan transaksi</h5>				
												  		<?php } ?>
													</div>
											  		<?php if($uj['Status']=="WAIT"){ ?>
											  		<div class="col-md-12 my-3">
											  			<h6 class="font-bold">Kode Jadwal Ujian</h6>
									                    <select class="browser-default custom-select font-light" name="kodeujiansusulan" searchable="Cari..." required>
										                	<option class="font-light" disabled selected>Pilih Kode Ujian</option>
										                    <?php foreach($jadwalujian['data'] as $ju): ?>
										                       	<?php if($ju['Kode_Jurusan'] == $uj['Kode_Jurusan'] && $ju['Semester'] == $uj['Semester'] &&  $ju['Nama_Matkul'] ==  $uj['Nama_Matkul']) { 
										                       		$statusbayar.$i = true;
										                       	?>
										                       	<option class="font-light" value="<?= $ju['Kode_Ujian']; ?>"><?= $ju['Kode_Ujian'] ?> - <?= $ju['Nama_Matkul']?> - <?= date("d F Y", strtotime($ju['Tanggal'])); ?> - <?= date("H:i", strtotime($ju['Jam'])); ?> WIB - <?= $ju['Ruang']; ?></option>
										                    <?php } endforeach; ?>
										                </select>
											  		</div>
											    </div>
												   	<div class="modal-footer">
													   		<?php if($uj['Bayar'] == 'N') { ?>
													   			<button type="submit" class="btn green white-text btn-rounded font-bold" disabled>Terima  <span class="badge badge-pill black"><i class="fas fa-check white-text"></i></span></button>
													   		<?php } if($uj['Bayar'] == 'Y') { ?>
													   			<button type="submit" class="btn green white-text btn-rounded font-bold">Terima  <span class="badge badge-pill black"><i class="fas fa-check white-text"></i></span></button>
													   		<?php } ?>
												   		<input type="hidden" name="nim" value="<?= $uj['NIM']; ?>">
												   		<input type="hidden" name="namamatkul" value="<?= $uj['Nama_Matkul']; ?>">
												   	
												   		<a href="<?= base_url(); ?>admin/updateUjian/2/<?= $uj['Kode_Form'] ?>" class="btn red white-text btn-rounded font-bold">Tolak  <span class="badge badge-pill black"><i class="fas fa-remove white-text"></i></span></a>
												    </div>
												<?php }; ?>
												</form>
											</div>
										</div>
									</div>
					          	</tr>
				          	<?php $i++; endforeach; ?>	         
				          	<?php } ?>
				        </tbody>
				    </table>
		   		</div>


			</div>
			<!-- Panel 1 -->

		
			<!-- Panel 2 -->
			<div class="tab-pane fade" id="proposalSkripsi" role="tabpanel">
			    <div class="card py-3 px-3" style="border-radius: 30px;">
					<table class="table table-borderless table-hover text-center table-responsive">
				        <thead>
				          	<tr>
				          		<th class="font-bold">No</th>
				          		<th class="font-bold">Kode Skripsi</th>
				              	<th class="font-bold">NIM</th>
				              	<th class="font-bold">Nama</th>
				              	<th class="font-bold">Jurusan</th>
				              	<th class="font-bold">Status</th>
				              	<th class="font-bold">Formulir Proposal</th>
				          	</tr>
				        </thead>

				        <tbody>
				        	<?php if($daftarProposal['status']) {?>
				          	<?php $i = 1; foreach ($daftarProposal['data'] as $dp) : ?>
					          	<tr>	
						          	<td class="align-middle"><?= $i; ?></td>
						          	<td class="align-middle"><?= $dp['Kode_Skripsi']?></td>
						            <td class="align-middle"><?= $dp['NIM']?></td>
						            <td class="align-middle"><?= $dp['Nama']?></td>	
						            <td class="align-middle"><?= $dp['Nama_Jurusan']?></td>						            
						            <td class="align-middle"><?= $dp['Status']?></td>
						            <td class="align-middle">
						            	<button class="btn btn-floating info-color waves-effect waves-light" data-toggle="modal" data-target="#detailSkripsi<?= $i; ?>"><i class="fas fa-book"></i></button>
						            </td>


						            <div id="detailSkripsi<?= $i; ?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
										<div class="modal-dialog modal-lg" role="document">

						    				<div class="modal-content" style="border-radius: 40px;">
						    					<div class="modal-header info-color" style="border-radius: 0 30px;">
											        <h2 class="modal-title font-bold white-text" id="exampleModalLabel"><?= $dp['Nama']; ?></h2>
											        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
											          	<span aria-hidden="true">&times;</span>
											        </button>
											    </div>
											    <div class="modal-body col-md-12">
											    	<div class="col-md-12 row my-3">
											    		<div class="col-md-6">
												  			<h6 class="font-bold">Tahun Akademik	:</h6>
												  			<h5 class="font-light"><?= $dp['Tahun_Di'] ?>/<?= $dp['Tahun_Ke'] ?></h5>
												  		</div>
												    	<div class="col-md-6">
												  			<h6 class="font-bold">NIM	:</h6>
												  			<h5 class="font-light"><?= $dp['NIM'] ?></h5>
												  		</div>												  		
												  	</div>
												  	<div class="col-md-12 row my-3">
													  	<div class="col-md-6">
													  		<h6 class="font-bold">Fakultas	:</h6>
													  		<h5 class="font-light"><?= $dp['Fakultas'] ?></h5>
													  	</div> 
												    	<div class="col-md-6">
												  			<h6 class="font-bold">Jurusan	:</h6>
												  			<h5 class="font-light"><?= $dp['Nama_Jurusan'] ?></h5>
												  		</div>
											  		</div>

											  		<div class="col-md-12 row my-3">
												  		<div class="col-md-6">
													  		<h6 class="font-bold">Semester	:</h6>
													  		<h5 class="font-light">Semester <?= $dp['Semester'] ?></h5>
													  	</div>
													  	<div class="col-md-6">
													  		<h6 class="font-bold">Program	:</h6>
													  		<h5 class="font-light"><?= $dp['Program']."  ".$dp['Kelas'] ?></h5>
													  	</div>
											  		</div>
											  		<div class="col-md-12 row my-3">
												    	<div class="col-md-6">
												  			<h6 class="font-bold">Email	:</h6>
												  			<h5 class="font-light"><?= $dp['Email'] ?></h5>
												  		</div>  	
												    	<div class="col-md-6">
												  			<h6 class="font-bold">Telepon	:</h6>
												  			<h5 class="font-light"><?= $dp['Telepon'] ?></h5>
												  		</div>  	
											  		</div>
											  		<div class="col-md-12 my-3">
											  			<h6 class="font-bold">Judul Bahasa Indonesia	:</h6>
											  			<h5 class="font-light font-italic">"<?= $dp['Judul_BI'] ?>"</h5>
											  		</div>  	
													<div class="col-md-12 my-3">
														<h6 class="font-bold">Judul Bahasa Inggris	:</h6>
											  			<h5 class="font-light font-italic">"<?= $dp['Judul_EN'] ?>"</h5>
											  		</div>  	
											    </div>
											    <?php if($dp['Status']=="WAIT"){ ?>
												   	<div class="modal-footer" action="">
													   	<form method="POST" action="<?= base_url(); ?>admin/updateProposal/1/<?= $dp['Kode_Skripsi'] ?>">
													   		<input type="hidden" name="nim" value="<?= $dp['NIM'] ;?>">
													   		<input type="hidden" name="judulbi" value="<?= $dp['Judul_BI'] ;?>">
													   		<input type="hidden" name="judulen" value="<?= $dp['Judul_EN'] ;?>">
													   		<button type="submit" class="btn green white-text btn-rounded font-bold">Terima  <span class="badge badge-pill black"><i class="fas fa-check white-text"></i></span>
													   		</button>
												   		</form>

												   		<a href="<?= base_url(); ?>admin/updateProposal/2/<?= $dp['Kode_Skripsi'] ?>" class="btn red white-text btn-rounded font-bold">Tolak  <span class="badge badge-pill black"><i class="fas fa-remove white-text"></i></span></a>
												    </div>
												<?php }; ?>
											</div>

										</div>
									</div>
					          	</tr>
				          	<?php $i++; endforeach; ?>	         
				          <?php } ?>
				        </tbody>
				    </table>
		   		</div>			   
			</div>
			<!-- Panel 2 -->

		
			<!-- Panel 3 -->
			<div class="tab-pane fade" id="perubahanJudul" role="tabpanel">
				<div class="card py-3 px-3" style="border-radius: 30px;">
					<table class="table table-borderless table-hover text-center table-responsive">
				        <thead>
				          	<tr>
				          		<th class="font-bold">No</th>
				          		<th class="font-bold">Kode Skripsi</th>
				              	<th class="font-bold">NIM</th>
				              	<th class="font-bold">Nama</th>
				              	<th class="font-bold">Jurusan</th>
				              	<th class="font-bold">Status</th>
				              	<th class="font-bold">Formulir Proposal</th>
				          	</tr>
				        </thead>

				        <tbody>
				        	<?php if($perubahanJudul['status']) { ?>
				          	<?php $i = 1; foreach ($perubahanJudul['data'] as $pj) : ?>
					          	<tr>	
						          	<td class="align-middle"><?= $i; ?></td>
						          	<td class="align-middle"><?= $pj['Kode_Skripsi']?></td>
						            <td class="align-middle"><?= $pj['NIM']?></td>
						            <td class="align-middle"><?= $pj['Nama']?></td>	
						            <td class="align-middle"><?= $pj['Nama_Jurusan']?></td>						            
						            <td class="align-middle"><?= $pj['Status']?></td>
						            <td class="align-middle">
						            	<button class="btn btn-floating info-color waves-effect waves-light" data-toggle="modal" data-target="#detailSkripsijudul<?= $i; ?>"><i class="fas fa-book"></i></button>
						            </td>


						            <div id="detailSkripsijudul<?= $i; ?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
										<div class="modal-dialog modal-lg" role="document">

						    				<div class="modal-content" style="border-radius: 40px;">
						    					<div class="modal-header info-color" style="border-radius: 0 30px;">
											        <h2 class="modal-title font-bold white-text" id="exampleModalLabel"><?= $pj['Nama']; ?></h2>
											        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
											          	<span aria-hidden="true">&times;</span>
											        </button>
											    </div>
											    <div class="modal-body col-md-12">
											    	<div class="col-md-12 row my-3">
											    		<div class="col-md-6">
												  			<h6 class="font-bold">Tahun Akademik	:</h6>
												  			<h5 class="font-light"><?= $pj['Tahun_Di'] ?>/<?= $pj['Tahun_Ke'] ?></h5>
												  		</div>
												    	<div class="col-md-6">
												  			<h6 class="font-bold">NIM	:</h6>
												  			<h5 class="font-light"><?= $pj['NIM'] ?></h5>
												  		</div>												  		
												  	</div>
												  	<div class="col-md-12 row my-3">
													  	<div class="col-md-6">
													  		<h6 class="font-bold">Fakultas	:</h6>
													  		<h5 class="font-light"><?= $pj['Fakultas'] ?></h5>
													  	</div> 
												    	<div class="col-md-6">
												  			<h6 class="font-bold">Jurusan	:</h6>
												  			<h5 class="font-light"><?= $pj['Nama_Jurusan'] ?></h5>
												  		</div>
											  		</div>

											  		<div class="col-md-12 row my-3">
												  		<div class="col-md-6">
													  		<h6 class="font-bold">Semester	:</h6>
													  		<h5 class="font-light">Semester <?= $pj['Semester'] ?></h5>
													  	</div>
													  	<div class="col-md-6">
													  		<h6 class="font-bold">Program	:</h6>
													  		<h5 class="font-light"><?= $pj['Program']."  ".$pj['Kelas'] ?></h5>
													  	</div>
											  		</div>
											  		<div class="col-md-12 row my-3">
												    	<div class="col-md-6">
												  			<h6 class="font-bold">Email	:</h6>
												  			<h5 class="font-light"><?= $pj['Email'] ?></h5>
												  		</div>  	
												    	<div class="col-md-6">
												  			<h6 class="font-bold">Telepon	:</h6>
												  			<h5 class="font-light"><?= $pj['Telepon'] ?></h5>
												  		</div>  	
											  		</div>
											  		<h6 class="font-bold">Judul Lama</h6>
											  		<div class="col-md-12 my-3">
											  			<h6 class="font-bold">Judul Bahasa Indonesia	:</h6>
											  			<h5 class="font-light font-italic">"<?= $pj['Judul_BIlama'] ?>"</h5>
											  		</div>  	
													<div class="col-md-12 my-3">
														<h6 class="font-bold">Judul Bahasa Inggris	:</h6>
											  			<h5 class="font-light font-italic">"<?= $pj['Judul_ENlama'] ?>"</h5>
											  		</div>  	
											  		<h6 class="font-bold">Judul Baru</h6>
											  		<div class="col-md-12 my-3">
											  			<h6 class="font-bold">Judul Bahasa Indonesia	:</h6>
											  			<h5 class="font-light font-italic">"<?= $pj['Judul_BIbaru'] ?>"</h5>
											  		</div>  	
													<div class="col-md-12 my-3">
														<h6 class="font-bold">Judul Bahasa Inggris	:</h6>
											  			<h5 class="font-light font-italic">"<?= $pj['Judul_ENbaru'] ?>"</h5>
											  		</div>
											  		<div class="col-md-12 my-3">
														<h6 class="font-bold">Alasan	:</h6>
											  			<h5 class="font-light font-italic"><?= $pj['Alasan'] ?></h5>
											  		</div>
											    </div>
											    <?php if($pj['Status']=="WAIT"){ ?>
												   	<div class="modal-footer">
												   		<form method="POST" action="<?= base_url(); ?>admin/updateJudul/1/<?= $pj['Kode_Skripsi'] ?>">
												   		<input type="hidden" name="judulbi" value="<?= $pj['Judul_BIbaru']?>">
												   		<input type="hidden" name="judulen" value="<?= $pj['Judul_ENbaru']?>">
													   	<button type="submit" class="btn green white-text btn-rounded font-bold">Terima  <span class="badge badge-pill black"><i class="fas fa-check white-text"></i></span></button>
													   	</form>

													   	<a href="<?= base_url(); ?>admin/updateJudul/2/<?= $pj['Kode_Skripsi'] ?>" class="btn red white-text btn-rounded font-bold">Tolak  <span class="badge badge-pill black"><i class="fas fa-remove white-text"></i></span></a>
												    </div>
											    <?php }; ?>
											</div>

										</div>
									</div>
					          	</tr>
				          	<?php $i++; endforeach; ?>	         
				          <?php } ?>
				        </tbody>
				    </table>
		   		</div>
				
			</div>
			<!-- Panel 3 -->

		</div>
		<!-- Tab panels -->
	</div>
</main>