<div class="container-fluid" style="padding-top: 10%; padding-bottom: 10%;">
	<div>
			<div id="tambah" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				<div class="modal-dialog modal-lg" role="document">
    				<div class="modal-content" style="border-radius: 40px;">
    					<div class="modal-header primary-color" style="border-radius: 0 30px;">
					        <h5 class="modal-title font-bold white-text" id="exampleModalLabel">Tambah</h5>
					        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
					          	<span aria-hidden="true">&times;</span>
					        </button>
					    </div>

					    <div class="modal-body">
					    	<form action="<?= base_url(); ?>admin/tambahMahasiswa" method="POST" class="col-md-12 row needs-validation" enctype="multipart/form-data" novalidate>

								<div class="col-md-12 row">		                    
				                    <div class="col-md-6">
				                        <div class="md-form">
				                            <input  id="nama" type="text" class="validate form-control" name="nama" required>
				                            <label for="nama">Nama</label>
				                        </div>
				                    </div>
				                    <div class="col-md-6">
				                        <div class="md-form">
				                            <input  id="nim" type="number" class="validate form-control" name="nim" required>
				                            <label for="nim">NIM</label>
				                        </div>
				                    </div>
			                	</div>

			                    <div class="col-md-12 row">
			                    	<div class="col-md-12">
			                    		<h6>Jurusan</h6>
			                    	</div>
			                    	<div class="col-md-3">
			                    		<div class="form-check">
				                        	<input id="IT" class="form-check-input" type="radio" name="jurusan" value="TI" required>
				                        	<label for="IT" class="form-check-label">Informatika</label>
				                        </div>
			                        </div>
			                        <div class="col-md-3">
			                        	<div class="form-check">
				                        	<input id="SI" class="form-check-input" type="radio" name="jurusan" value="SI" required>
				                            <label for="SI" class="form-check-label">Sistem Informasi</label>
				                        </div>
			                        </div>
			                        <div class="col-md-3">
			                        	<div class="form-check">
				                            <input id="AK" class="form-check-input" type="radio" name="jurusan" value="AK" required>
				                            <label for="AK" class="form-check-label">Akuntansi</label>
				                        </div>
			                        </div>
			                        <div class="col-md-3">
			                        	<div class="form-check">
				                            <input id="MN" class="form-check-input" type="radio" name="jurusan" value="MN" required>
				                            <label for="MN" class="form-check-label">Manajemen</label>
				                        </div>
			                        </div>
			                        <div class="col-md-3">
			                        	<div class="form-check">
				                            <input id="MA" class="form-check-input" type="radio" name="jurusan" value="MA" required>
				                            <label for="MA" class="form-check-label">Matematika</label>
				                        </div>
			                        </div>
			                        <div class="col-md-3">
			                        	<div class="form-check">
				                            <input id="IK" class="form-check-input" type="radio" name="jurusan" value="IK" required>
				                            <label for="IK" class="form-check-label">Ilmu Komunikasi</label>
				                        </div>
			                        </div>
			                        <div class="col-md-3">
			                        	<div class="form-check">
				                            <input id="DV" class="form-check-input" type="radio" name="jurusan" value="DV" required>
				                            <label for="DV" class="form-check-label">DKV</label>
				                        </div>
			                        </div>
			                        <div class="col-md-3">
			                        	<div class="form-check">
				                            <input id="AT" class="form-check-input" type="radio" name="jurusan" value="AT" required>
				                            <label for="AT" class="form-check-label">Arsitektur</label>
				                        </div>
			                        </div>
			                    </div>
			                    <div class="col-md-12 row">
			                    	<div class="col-md-6">
				                    	<div class="md-form">
				                    		<input  id="email" type="email" class="validate form-control" name="email" required>
				                            <label for="email">Email</label>
										</div>
									</div>
									<div class="col-md-6">
				                        <div class="md-form">
				                            <input  id="telepon" type="number" class="validate form-control" name="telepon" required>
				                            <label for="telepon">Nomor Telepon</label>
				                        </div>
				                    </div>
		  						</div>

		  						<div class="col-md-12 row">
		  							<div class="col-md-4">
										    <select class="md-form mdb-select" name="semester" required>
										      	<option disabled selected>Pilih semester anda</option>
										      	<option value="1">Semester 1</option>
										      	<option value="2">Semester 2</option>
										      	<option value="3">Semester 3</option>
										      	<option value="4">Semester 4</option>
										      	<option value="5">Semester 5</option>
										      	<option value="6">Semester 6</option>
										      	<option value="7">Semester 7</option>
										      	<option value="8">Semester 8</option>
										    </select>
										    <label>Semester</label>										
		  							</div>
		  							<div class="col-md-4 form-row">
		  								<div class="col-md-12">
				                    		<h6>Program</h6>
				                    	</div>
				                    	<div class="col-md-4">
				                    		<div class="form-check">
					                        	<input id="s1" class="form-check-input" type="radio" name="program" value="Sarjana" required>
					                        	<label for="s1" class="form-check-label">S1</label>
					                        </div>
				                        </div>
				                        <div class="col-md-4">
				                        	<div class="form-check">
					                        	<input id="s2" class="form-check-input" type="radio" name="program" value="Magister" required>
					                            <label for="s2" class="form-check-label">S2</label>
					                        </div>
				                        </div>
				                        <div class="col-md-4">
				                        	<div class="form-check">
					                            <input id="d3" class="form-check-input" type="radio" name="program" value="Diploma" required>
					                            <label for="d3" class="form-check-label">D3</label>
				                        	</div>
				                        </div>
			                        </div>
			                        <div class="col-md-4 form-row">
			                        	<div class="col-md-12">
				                    		<h6>Kelas</h6>
				                    	</div>
				                    	<div class="col-md-4">
				                    		<div class="form-check">
					                        	<input id="pagi" class="form-check-input" type="radio" name="kelas" value="Pagi" required>
					                        	<label for="pagi" class="form-check-label">Pagi</label>
					                        </div>
				                        </div>
				                        <div class="col-md-4">
				                        	<div class="form-check">
					                        	<input id="malam" class="form-check-input" type="radio" name="kelas" value="Malam" required>
					                            <label for="malam" class="form-check-label">Malam</label>
					                        </div>
				                        </div>
			                        </div>
			                    </div>
					    </div>		

					   	<div class="modal-footer">
					      	<button type="submit" class="btn btn-rounded light-blue darken-1 waves-effect waves-light font-bold white-text">Tambah</button>
							</form>
					    </div>

					</div>
				</div>
			</div>


		<div class="card py-3 px-3" style="border-radius: 30px;">
				<table id="dtMaterialDesignExample" class="table table-borderless table-hover text-center font-light table-responsive" width="100%">
					<div class="col-md-12 row">
						<div class="col-md-1">
							<button type="button" class="btn btn-floating white waves-effect waves-light floating" data-toggle="modal" data-target="#tambah"><i class="fas fa-plus blue-text"></i></button>	
						</div>
						<div class="col-md-11 text-center" style="padding: auto;">
							<h2 class="font-bold py-2">Daftar Mahasiswa</h2>
						</div>
					</div>
					
			        <thead>
			          	<tr>
			          		<th class="font-bold">No</th>
			              	<th class="font-bold">Nama</th>
			              	<th class="font-bold">NIM</th>
			              	<th class="font-bold">Jurusan</th>
			              	<th class="font-bold">Semester</th>
			              	<th class="font-bold">Telepon</th>
			              	<th class="font-bold">Email</th>
			              	<th class="font-bold">Program</th>
			              	<th class="font-bold">Kelas</th>			              	
			              	<th class="font-bold">Option</th>
			          	</tr>
			        </thead>

			        <tbody>
			          <?php $i = 1; foreach ($mahasiswa as $mhs) : ?>
				          <tr id="<?= $mhs['NIM']; ?>">
				          	<td class="align-middle"><?= $i; ?></td>
				            <td class="align-middle"><?= $mhs['Nama']?></td>
				            <td class="align-middle"><?= $mhs['NIM']?></td>
				            <td class="align-middle"><?= $mhs['Nama_Jurusan']?></td>
				            <td class="align-middle"><?= $mhs['Semester']?></td>
				            <td class="align-middle"><?= $mhs['Telepon']?></td>
				            <td class="align-middle"><?= $mhs['Email']?></td>
				            <td class="align-middle"><?= $mhs['Program']?></td>
				            <td class="align-middle"><?= $mhs['Kelas']?></td>
				            <td>
				            	<button class="btn btn-floating green waves-effect waves-light" data-toggle="modal" data-target="#edit<?= $i; ?>"><i class="fas fa-pencil"></i></button>
				            	<a href="<?= base_url(); ?>admin/hapus/1/<?= $mhs['Id_Mahasiswa']; ?>" class="btn btn-floating red waves-effect waves-light"><i class="fas fa-trash"></i></a>
				            </td>
				          </tr>

							<div id="edit<?= $i; ?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
								<div class="modal-dialog modal-lg" role="document">
				    				<div class="modal-content" style="border-radius: 40px;">
				    					<div class="modal-header info-color" style="border-radius: 0 30px;">
									        <h2 class="modal-title font-bold white-text" id="exampleModalLabel"><?= $mhs['Nama']; ?></h2>
									        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
									          	<span aria-hidden="true">&times;</span>
									        </button>
									    </div>

									    <div class="modal-body">
									    	<form action="<?= base_url(); ?>admin/editMahasiswa" method="POST" class="col-md-12 row needs-validation" enctype="multipart/form-data" novalidate>

												<div class="col-md-12 row">		                    
								                    <div class="col-md-6">
								                        <div class="md-form">
								                            <input  id="nama<?= $i; ?>" type="text" class="validate form-control" name="nama" value="<?= $mhs['Nama']; ?>" required>
								                            <label for="nama<?= $i; ?>">Nama</label>
								                        </div>
								                    </div>
								                    <div class="col-md-6">
								                        <div class="md-form">
								                            <input  id="nim<?= $i; ?>" type="number" class="validate form-control" name="nim" value="<?= $mhs['NIM']; ?>"required>
								                            <label for="nim<?= $i; ?>">NIM</label>
								                        </div>
								                    </div>
							                	</div>

							                    <div class="col-md-12 row">
							                    	<div class="col-md-12">
							                    		<h6>Jurusan</h6>
							                    	</div>
							                    	<?php foreach ($jurusan as $jur) :
							                    		if($mhs['Kode_Jurusan'] == $jur['Kode_Jurusan']){
					                    			?>
							                    	<div class="col-md-3">
							                    		<div class="form-check">
								                        	<input id="<?= $jur['Kode_Jurusan'].$i; ?>" type="radio" class="form-check-input" name="jurusan" value="<?= $jur['Kode_Jurusan']; ?>" checked>
								                        	<label class="form-check-label" for="<?= $jur['Kode_Jurusan'].$i; ?>">
								                        		<?php if($jur['Kode_Jurusan'] == "DV") { ?>
								                        			DKV
								                        		<?php } else{ ?>
								                        			<?= $jur['Nama_Jurusan']; ?>	
								                        		<?php } ?>
								                        	</label>
								                        </div>
							                        </div>
							                        <?php } else {?>
							                        <div class="col-md-3">
							                    		<div class="form-check">
								                        	<input id="<?= $jur['Kode_Jurusan'].$i; ?>" type="radio" class="form-check-input" name="jurusan" value="<?= $jur['Kode_Jurusan']; ?>">
								                        	<label class="form-check-label" for="<?= $jur['Kode_Jurusan'].$i; ?>">
								                        		<?php if($jur['Kode_Jurusan'] == "DV") { ?>
								                        			DKV
								                        		<?php } else{ ?>
								                        			<?= $jur['Nama_Jurusan']; ?>	
								                        		<?php } ?>
								                        	</label>
								                        </div>
							                        </div>
							                        <?php } endforeach; ?>
							                    </div>
							                    <div class="col-md-12 row">
							                    	<div class="col-md-6">
								                    	<div class="md-form">
								                    		<input  id="email<?= $i; ?>" type="email" class="validate form-control" name="email" value="<?= $mhs['Email']; ?>" required>
								                            <label for="email<?= $i; ?>">Email</label>
														</div>
													</div>
													<div class="col-md-6">
								                        <div class="md-form">
								                            <input  id="telepon<?= $i; ?>" type="number" class="validate form-control" name="telepon" value="<?= $mhs['Telepon']; ?>" required>
								                            <label for="telepon<?= $i; ?>">Nomor Telepon</label>
								                        </div>
								                    </div>
						  						</div>

						  						<div class="col-md-12 row">
						  							<div class="col-md-4">
						  								<div class="md-form">
							  								<input  id="semester<?= $i; ?>" type="number" class="validate form-control" name="semester" value="<?= $mhs['Semester']; ?>" required>
															<label for="semester<?= $i; ?>">Semester</label>
														</div>
						  							</div>
						  							<div class="col-md-4 form-row">
						  								<div class="col-md-12">
								                    		<h6>Program</h6>
								                    	</div>
								                    	<div class="col-md-4">
								                    		<div class="form-check">
								                    			<?php if($mhs['Kode_Program']=="SP"||$mhs['Kode_Program']=="SM") {?>
									                        		<input id="s1<?= $i; ?>" class="form-check-input" type="radio" name="program" value="Sarjana" checked>
									                        	<?php } else {?>
									                        		<input id="s1<?= $i; ?>" class="form-check-input" type="radio" name="program" value="Sarjana">
									                        	<?php } ?>
									                        	<label for="s1<?= $i; ?>" class="form-check-label">S1</label>
									                        </div>
								                        </div>
								                        <div class="col-md-4">
								                        	<div class="form-check">
								                        		<?php if($mhs['Kode_Program']=="MP"||$mhs['Kode_Program']=="MM") {?>
									                        		<input id="s2<?= $i; ?>" class="form-check-input" type="radio" name="program" value="Magister" checked>
									                        	<?php } else {?>
									                        		<input id="s2<?= $i; ?>" class="form-check-input" type="radio" name="program" value="Magister">
									                        	<?php } ?>
									                            <label for="s2<?= $i; ?>" class="form-check-label">S2</label>
									                        </div>
								                        </div>
								                        <div class="col-md-4">
								                        	<div class="form-check">
								                        		<?php if($mhs['Kode_Program']=="DP"||$mhs['Kode_Program']=="DM") {?>
									                            	<input id="d3<?= $i; ?>" class="form-check-input" type="radio" name="program" value="Diploma" checked>
									                            <?php } else {?>
									                            	<input id="d3<?= $i; ?>" class="form-check-input" type="radio" name="program" value="Diploma">
									                            <?php } ?>
									                            <label for="d3<?= $i; ?>" class="form-check-label">D3</label>
								                        	</div>
								                        </div>
							                        </div>
							                        <div class="col-md-4 form-row">
							                        	<div class="col-md-12">
								                    		<h6>Kelas</h6>
								                    	</div>
								                    	<div class="col-md-6">
								                    		<div class="form-check">
								                    			<?php if($mhs['Kode_Program']=="SP"||$mhs['Kode_Program']=="MP"||$mhs['Kode_Program']=="DP") {?>
									                        		<input id="pagi<?= $i; ?>" class="form-check-input" type="radio" name="kelas" value="Pagi" checked>
									                        	<?php } else {?>
									                        		<input id="pagi<?= $i; ?>" class="form-check-input" type="radio" name="kelas" value="Pagi">
									                        	<?php } ?>
									                        	<label for="pagi<?= $i; ?>" class="form-check-label">Pagi</label>
									                        </div>
								                        </div>
								                        <div class="col-md-6">
								                        	<div class="form-check">
								                        		<?php if($mhs['Kode_Program']=="SM"||$mhs['Kode_Program']=="MM"||$mhs['Kode_Program']=="DM") {?>
									                        		<input id="malam<?= $i; ?>" class="form-check-input" type="radio" name="kelas" value="Malam" checked>
									                        	<?php } else {?>
									                        		<input id="malam<?= $i; ?>" class="form-check-input" type="radio" name="kelas" value="Malam">
									                        	<?php } ?>
									                            <label for="malam<?= $i; ?>" class="form-check-label">Malam</label>
									                        </div>
								                        </div>
							                        </div>
							                    </div>
									    </div>		

									   	<div class="modal-footer">
									      	<button type="submit" class="btn btn-rounded light-blue darken-1 waves-effect waves-light black-text font-bold white-text">Edit</button>
									      	<input type="hidden" name="Id" value="<?= $mhs['Id_Mahasiswa']; ?>">
											</form>
									    </div>

									</div>
								</div>
							</div>
			          <?php $i++; endforeach; ?>	         
			        </tbody>
			    </table>
	    </div>
	</div>
</div>