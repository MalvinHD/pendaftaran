<div class="container-fluid" style="padding-top: 10%; padding-bottom: 10%;">
    <div class="text-center my-2">
          <h2 class="font-bold">Daftar Mata Kuliah</h2>
    </div>

    <!-- Nav tabs -->
    <ul class="nav nav-tabs md-tabs nav-justified light-green lighten-1" role="tablist" style="border-radius: 20px;">
        <li class="nav-item">
          <a class="nav-link active font-weight-bold font-light" data-toggle="tab" href="#bisnis" role="tab">
            <i class="fas fa-book pr-2"></i>Bisnis</a>
        </li>
      
        <li class="nav-item">
          <a class="nav-link font-weight-bold font-light" data-toggle="tab" href="#industrikreatif" role="tab">
            <i class="fas fa-book pr-2"></i>Industri Kreatif</a>
        </li>
    </ul>
    <!-- Nav tabs -->

    <!-- Tab panels -->
    <div class="tab-content">

      <!-- Panel 1 -->
      <div class="tab-pane fade in show active" id="bisnis" role="tabpanel">
              

              <!-- Nav tabs -->
                 <div class="row">
                    <div class="col-md-3">
                      <ul class="nav md-pills pills-success flex-column" role="tablist">
                        <?php $i=1; foreach($bisnis as $bs): 
                          if($i==1){
                        ?>
                            <li class="nav-item" data-aos="fade-right">
                              <a class="nav-link active text-left font-light font-weight-bold px-2" data-toggle="tab" href="#pillbs<?= $i; ?>" role="tab" style="border-radius: 15px;"><i class="fas fa-graduation-cap mr-2"></i><?= $bs['Nama_Jurusan']; ?>
                                
                              </a>
                            </li>
                        <?php }else{ ?>
                            <li class="nav-item" data-aos="fade-right">
                              <a class="nav-link text-left font-light font-weight-bold px-2" data-toggle="tab" href="#pillbs<?= $i; ?>" role="tab" style="border-radius: 15px;"><i class="fas fa-graduation-cap mr-2"></i><?= $bs['Nama_Jurusan']; ?>
                                
                              </a>
                            </li>
                        <?php } $i++; endforeach; ?>
                            <li class="nav-item" data-aos="fade-right">
                                <a class="btn btn-rounded text-left font-light black-text font-weight-bold btntmbhmtkl transisi" data-toggle="modal" data-target="#tambahMatkul">Tambah Mata kuliah
                                  <i class="fas fa-plus ml-2"></i>
                                </a>  
                            </li>        
                      </ul>
                    </div>


                  <div class="col-md-9">
                    <!-- Tab panels -->
                    <div class="tab-content vertical">
                      <!-- Panel 1 -->
                      <?php 
                        $a = 0;
                        for ($i=1; $i <= count($bisnis)  ; $i++) { 
                        if($i==1){
                      ?>
                        <div class="tab-pane fade in show active" id="pillbs<?= $i; ?>" role="tabpanel">
                          <div class="card p-3 mt-3" style="border-radius: 30px;">
                            <table id="datatableMatkul<?= $bisnis[$a]['Kode_Jurusan'] ?>" class="table table-borderless table-hover text-center table-responsive table-responsive" width="100%">
                                <thead>
                                    <tr>
                                      <th class="font-bold">No</th>
                                      <th class="font-bold">Nama Mata Kuliah</th>
                                      <th class="font-bold">Semester</th>
                                      <th class="font-bold">Option</th>
                                    </tr>
                                </thead>
                                <tbody class="font-light">
                                    <?php 
                                      $no = 1;
                                      foreach($matkul as $mk):
                                      if($mk['Kode_Jurusan'] == $bisnis[$a]['Kode_Jurusan']){
                                    ?>
                                        <tr>
                                          <td><?= $no ?></td>
                                          <td><?= $mk['Nama_Matkul']; ?></td>
                                          <td><?= $mk['Semester']; ?></td>
                                          <td>
                                              <a class="btn btn-sm btn-rounded green white-text" data-toggle="modal" data-target="#editMatkul<?= $mk['Id_Matkul']; ?>"><i class="fas fa-edit"></i></a>
                                              <a href="<?= base_url(); ?>admin/hapus/3/<?= $mk['Id_Matkul']?>" class="btn btn-sm btn-rounded red white-text"><i class="fas fa-remove"></i></a>
                                          </td>


                                          <div id="editMatkul<?= $mk['Id_Matkul']; ?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                              <div class="modal-dialog modal-lg" role="document">
                                                  <div class="modal-content" style="border-radius: 40px;">

                                                    <div class="modal-header success-color" style="border-radius: 0 30px;">
                                                        <h3 class="modal-title font-bold white-text" id="exampleModalLabel">Edit Mata Kuliah</h3>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>

                                                    <div class="modal-body">
                                                      <form action="<?= base_url(); ?>admin/editMatkul/<?= $mk['Id_Matkul']; ?>" method="POST" class="col-md-12 row needs-validation" enctype="multipart/form-data" novalidate>
                                                      <div class="col-md-12">                       
                                                          <div class="md-form">
                                                                <input  id="namaMatkul<?= $mk['Id_Matkul']; ?>" type="text" class="validate form-control font-light" name="nama" value="<?= $mk['Nama_Matkul']; ?>" required>
                                                                <label class="font-light" for="namaMatkul<?= $mk['Id_Matkul']; ?>">Nama Mata Kuliah</label>
                                                          </div>
                                                      </div>
                                                      <div class="col-md-12">
                                                          <div class="md-form">
                                                              <input  id="semester<?= $mk['Id_Matkul']; ?>" type="number" class="validate form-control font-light" name="semester" value="<?= $mk['Semester']; ?>" required>
                                                              <label class="font-light" for="semester<?= $mk['Id_Matkul']; ?>">Semester</label>
                                                          </div>
                                                      </div>    
                                                      <div class="modal-footer col-md-12 mt-5">
                                                          <button type="submit" class="btn btn-rounded success-color waves-effect waves-light white-text font-bold">Edit</button>
                                                      </div>
                                                      </form>
                                                    </div>

                                                </div>
                                              </div>
                                            </div>
                                        </tr>
                                    <?php $no++; }  endforeach; ?>
                                </tbody>
                            </table>
                          </div>
                        </div>
                      <?php }else{ ?>
                        <div class="tab-pane fade" id="pillbs<?= $i; ?>" role="tabpanel">
                            <div class="card p-3 mt-3" style="border-radius: 30px;">
                              <table id="datatableMatkul<?= $bisnis[$a]['Kode_Jurusan'] ?>" class="table table-borderless table-hover text-center table-responsive" width="100%">
                                  <thead>
                                      <tr>
                                        <th class="font-bold">No</th>
                                        <th class="font-bold">Nama Mata Kuliah</th>
                                        <th class="font-bold">Semester</th>
                                        <th class="font-bold">Option</th>
                                      </tr>
                                  </thead>
                                  <tbody class="font-light">
                                      <?php 
                                        $no = 1;
                                        foreach($matkul as $mk):
                                        if($mk['Kode_Jurusan'] == $bisnis[$a]['Kode_Jurusan']){
                                      ?>
                                          <tr>
                                            <td><?= $no ?></td>
                                            <td><?= $mk['Nama_Matkul']; ?></td>
                                            <td><?= $mk['Semester']; ?></td>
                                            <td>
                                                <a class="btn btn-sm btn-rounded green white-text" data-toggle="modal" data-target="#editMatkul<?= $mk['Id_Matkul']; ?>"><i class="fas fa-edit"></i></a>
                                                <a href="<?= base_url(); ?>admin/hapus/3/<?= $mk['Id_Matkul']?>" class="btn btn-sm btn-rounded red white-text"><i class="fas fa-remove"></i></a>
                                            </td>


                                             <div id="editMatkul<?= $mk['Id_Matkul']; ?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                              <div class="modal-dialog modal-lg" role="document">
                                                  <div class="modal-content" style="border-radius: 40px;">

                                                    <div class="modal-header success-color" style="border-radius: 0 30px;">
                                                        <h3 class="modal-title font-bold white-text" id="exampleModalLabel">Edit Mata Kuliah</h3>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>

                                                    <div class="modal-body">
                                                      <form action="<?= base_url(); ?>admin/editMatkul/<?= $mk['Id_Matkul']; ?>" method="POST" class="col-md-12 row needs-validation" enctype="multipart/form-data" novalidate>
                                                      <div class="col-md-12">                       
                                                          <div class="md-form">
                                                                <input  id="namaMatkul<?= $mk['Id_Matkul']; ?>" type="text" class="validate form-control font-light" name="nama" value="<?= $mk['Nama_Matkul']; ?>" required>
                                                                <label class="font-light" for="namaMatkul<?= $mk['Id_Matkul']; ?>">Nama Mata Kuliah</label>
                                                          </div>
                                                      </div>
                                                      <div class="col-md-12">
                                                          <div class="md-form">
                                                              <input  id="semester<?= $mk['Id_Matkul']; ?>" type="number" class="validate form-control font-light" name="semester" value="<?= $mk['Semester']; ?>" required>
                                                              <label class="font-light" for="semester<?= $mk['Id_Matkul']; ?>">Semester</label>
                                                          </div>
                                                      </div>    
                                                      <div class="modal-footer col-md-12 mt-5">
                                                          <button type="submit" class="btn btn-rounded success-color waves-effect waves-light white-text font-bold">Edit</button>
                                                      </div>
                                                      </form>
                                                    </div>

                                                </div>
                                              </div>
                                            </div>
                                          </tr>
                                      <?php  $no++; }  endforeach; ?>
                                  </tbody>
                              </table>
                            </div>
                        </div>
                      <?php } $a++; } ?>
                      <!-- Panel 1 -->
                    </div>
                  </div>
                </div>
                <!-- Nav tabs -->
      </div>
      <!-- Panel 1 -->

    
      <!-- Panel 2 -->
      <div class="tab-pane fade" id="industrikreatif" role="tabpanel">
            <!-- Nav tabs -->
                <div class="row">

                    <div class="col-md-3">
                      <ul class="nav md-pills pills-success flex-column" role="tablist">
                        <?php $i=1; foreach($industri as $id): 
                          if($i==1){
                        ?>
                            <li class="nav-item" data-aos="fade-right">
                              <a class="nav-link text-left active font-weight-bold font-light px-2" data-toggle="tab" href="#pillid<?= $i; ?>" role="tab" style="border-radius: 15px;"><i class="fas fa-graduation-cap mr-2"></i><?= $id['Nama_Jurusan']; ?>
                                
                              </a>
                            </li>
                        <?php }else{ ?>
                            <li class="nav-item" data-aos="fade-right">
                              <a class="nav-link text-left font-light font-weight-bold px-2" data-toggle="tab" href="#pillid<?= $i; ?>" role="tab" style="border-radius: 15px;"><i class="fas fa-graduation-cap mr-2"></i><?= $id['Nama_Jurusan']; ?>
                                
                              </a>
                            </li>
                        <?php } $i++; endforeach; ?>                        
                            <li class="nav-item" data-aos="fade-right">
                                <a class="btn btn-rounded text-left font-light font-weight-bold black-text btntmbhmtkl transisi" data-toggle="modal" data-target="#tambahMatkul">Tambah Mata kuliah
                                  <i class="fas fa-plus ml-2"></i>
                                </a>  
                            </li>
                      </ul>
                    </div>



                  <div class="col-md-9">
                    <!-- Tab panels -->
                    <div class="tab-content vertical">
                      <!-- Panel 1 -->
                      <?php 
                        $a = 0;
                        for ($i=1; $i <= count($industri)  ; $i++) { 
                        if($i==1){
                      ?>
                          <div class="tab-pane fade in show active" id="pillid<?= $i; ?>" role="tabpanel">
                              <div class="card p-3 mt-3" style="border-radius: 30px;">
                                  <table id="datatableMatkul<?= $industri[$a]['Kode_Jurusan'] ?>" class="table table-borderless table-hover text-center table-responsive" width="100%">
                                      <thead>
                                          <tr>
                                            <th class="font-bold">No</th>
                                            <th class="font-bold">Nama Mata Kuliah</th>
                                            <th class="font-bold">Semester</th>
                                            <th class="font-bold">Option</th>
                                          </tr>
                                      </thead>
                                      <tbody class="font-light">
                                          <?php 
                                            $no = 1;
                                            foreach($matkul as $mk):
                                            if($mk['Kode_Jurusan'] == $industri[$a]['Kode_Jurusan']){
                                          ?>
                                              <tr>
                                                <td><?= $no ?></td>
                                                <td><?= $mk['Nama_Matkul']; ?></td>
                                                <td><?= $mk['Semester']; ?></td>
                                                <td>
                                                  <a class="btn btn-sm btn-rounded green white-text" data-toggle="modal" data-target="#editMatkul<?= $mk['Id_Matkul']; ?>"><i class="fas fa-edit"></i></a>
                                                  <a href="<?= base_url(); ?>admin/hapus/3/<?= $mk['Id_Matkul']?>" class="btn btn-sm btn-rounded red white-text"><i class="fas fa-remove"></i></a>
                                                </td>

                                                 <div id="editMatkul<?= $mk['Id_Matkul']; ?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog modal-lg" role="document">
                                                        <div class="modal-content" style="border-radius: 40px;">

                                                          <div class="modal-header success-color" style="border-radius: 0 30px;">
                                                              <h3 class="modal-title font-bold white-text" id="exampleModalLabel">Edit Mata Kuliah</h3>
                                                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                  <span aria-hidden="true">&times;</span>
                                                              </button>
                                                          </div>

                                                          <div class="modal-body">
                                                            <form action="<?= base_url(); ?>admin/editMatkul/<?= $mk['Id_Matkul']; ?>" method="POST" class="col-md-12 row needs-validation" enctype="multipart/form-data" novalidate>
                                                            <div class="col-md-12">                       
                                                                <div class="md-form">
                                                                      <input  id="namaMatkul<?= $mk['Id_Matkul']; ?>" type="text" class="validate form-control font-light" name="nama" value="<?= $mk['Nama_Matkul']; ?>" required>
                                                                      <label class="font-light" for="namaMatkul<?= $mk['Id_Matkul']; ?>">Nama Mata Kuliah</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <div class="md-form">
                                                                    <input  id="semester<?= $mk['Id_Matkul']; ?>" type="number" class="validate form-control font-light" name="semester" value="<?= $mk['Semester']; ?>" required>
                                                                      <label class="font-light" for="semester<?= $mk['Id_Matkul']; ?>">Semester</label>
                                                                </div>
                                                            </div>                                                                
                                                            <div class="modal-footer col-md-12 mt-5">
                                                                <button type="submit" class="btn btn-rounded success-color waves-effect waves-light white-text font-bold">Edit</button>
                                                            </div>
                                                            </form>
                                                          </div>

                                                      </div>
                                                    </div>
                                                  </div>
                                              </tr>
                                          <?php $no++; }  endforeach; ?>
                                      </tbody>
                                  </table>
                              </div>
                          </div>
                      <?php } else { ?>
                          <div class="tab-pane fade" id="pillid<?= $i; ?>" role="tabpanel">
                              <div class="card p-3 mt-3" style="border-radius: 30px;">
                                  <table id="datatableMatkul<?= $industri[$a]['Kode_Jurusan'] ?>" class="table table-borderless table-hover text-center table-responsive" width="100%">
                                      <thead>
                                          <tr>
                                            <th class="font-bold">No</th>
                                            <th class="font-bold">Nama Mata Kuliah</th>
                                            <th class="font-bold">Semester</th>
                                            <th class="font-bold">Option</th>
                                          </tr>
                                      </thead>
                                      <tbody class="font-light">
                                          <?php 
                                            $no = 1;
                                            foreach($matkul as $mk):
                                            if($mk['Kode_Jurusan'] == $industri[$a]['Kode_Jurusan']){
                                          ?>
                                              <tr>
                                                <td><?= $no ?></td>
                                                <td><?= $mk['Nama_Matkul']; ?></td>
                                                <td><?= $mk['Semester']; ?></td>
                                                <td>
                                                    <a class="btn btn-sm btn-rounded green white-text" data-toggle="modal" data-target="#editMatkul<?= $mk['Id_Matkul']; ?>"><i class="fas fa-edit"></i></a>
                                                    <a href="<?= base_url(); ?>admin/hapus/3/<?= $mk['Id_Matkul']?>" class="btn btn-sm btn-rounded red white-text"><i class="fas fa-remove"></i></a>
                                                </td>

                                                 <div id="editMatkul<?= $mk['Id_Matkul']; ?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog modal-lg" role="document">
                                                        <div class="modal-content" style="border-radius: 40px;">

                                                          <div class="modal-header success-color" style="border-radius: 0 30px;">
                                                              <h3 class="modal-title font-bold white-text" id="exampleModalLabel">Edit Mata Kuliah</h3>
                                                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                  <span aria-hidden="true">&times;</span>
                                                              </button>
                                                          </div>

                                                          <div class="modal-body">
                                                            <form action="<?= base_url(); ?>admin/editMatkul/<?= $mk['Id_Matkul']; ?>" method="POST" class="col-md-12 row needs-validation" enctype="multipart/form-data" novalidate>
                                                            <div class="col-md-12">                       
                                                                <div class="md-form">
                                                                      <input  id="namaMatkul<?= $mk['Id_Matkul']; ?>" type="text" class="validate form-control font-light" name="nama" value="<?= $mk['Nama_Matkul']; ?>" required>
                                                                      <label class="font-light" for="namaMatkul<?= $mk['Id_Matkul']; ?>">Nama Mata Kuliah</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <div class="md-form">
                                                                      <input  id="semester<?= $mk['Id_Matkul']; ?>" type="number" class="validate form-control font-light" name="semester" value="<?= $mk['Semester']; ?>" required>
                                                                      <label class="font-light" for="semester<?= $mk['Id_Matkul']; ?>">Semester</label>
                                                                </div>
                                                            </div>    
                                                            <div class="modal-footer col-md-12 mt-5">
                                                                <button type="submit" class="btn btn-rounded success-color waves-effect waves-light white-text font-bold">Edit</button>
                                                            </div>
                                                            </form>
                                                          </div>

                                                      </div>
                                                    </div>
                                                  </div>                                      
                                              </tr>
                                          <?php $no++; }  endforeach; ?>
                                      </tbody>
                                  </table>
                              </div>
                          </div>
                      <?php } $a++; } ?>
                      <!-- Panel 1 -->
                    </div>
                  </div>
                </div>
                <!-- Nav tabs -->
      </div>
      <!-- Panel 2 -->

    </div>
    <!-- Tab panels -->
</div>


<!-- ----- -->
<!-- MODAL -->
<!-- ----- -->

    <div id="tambahMatkul" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content" style="border-radius: 40px;">

              <div class="modal-header info-color" style="border-radius: 0 30px;">
                  <h3 class="modal-title font-bold white-text" id="exampleModalLabel">Tambah Mata Kuliah Bisnis</h3>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>

              <div class="modal-body">
                <form class="needs-validation" action="<?= base_url(); ?>admin/tambahMatkul" method="POST" class="col-md-12 row" enctype="multipart/form-data" novalidate>
                <div class="col-md-12">                       
                    <div class="md-form">
                          <input  id="namaMatkulBisnis" type="text" class="validate form-control font-light" name="nama" required>
                          <label class="font-light" for="namaMatkulBisnis">Nama Mata Kuliah</label>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="md-form">
                        <select class="md-form mdb-select font-light" name="semester" required>
                            <option class="font-light" disabled selected>Pilih semester</option>
                            <option class="font-light" value="1">Semester 1</option>
                            <option class="font-light" value="2">Semester 2</option>
                            <option class="font-light" value="3">Semester 3</option>
                            <option class="font-light" value="4">Semester 4</option>
                            <option class="font-light" value="5">Semester 5</option>
                            <option class="font-light" value="6">Semester 6</option>
                            <option class="font-light" value="7">Semester 7</option>
                            <option class="font-light" value="8">Semester 8</option>
                        </select>
                        <label>Semester</label>                   
                    </div>
                </div>    
                <div class="col-md-12">
                    <div class="md-form">
                        <select class="md-form mdb-select font-light" name="jurusan">
                            <option class="font-light" disabled selected>Pilih Jurusan</option>
                            <?php foreach($jurusan as $jur): ?>
                                <option class="font-light" value="<?= $jur['Kode_Jurusan']; ?>"><?= $jur['Nama_Jurusan'] ?></option>
                            <?php endforeach; ?>
                        </select>
                        <label>Semester</label>                   
                    </div>
                </div>
                <div class="modal-footer col-md-12 mt-5">
                    <button type="submit" class="btn btn-rounded light-blue darken-1 waves-effect waves-light white-text font-bold">Tambah</button>
                </div>
                </form>
              </div>

          </div>
        </div>
      </div>
<!-- ----- -->
<!-- MODAL -->
<!-- ----- -->