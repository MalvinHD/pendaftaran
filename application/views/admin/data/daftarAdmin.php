<div class="container" style="padding-top: 10%; padding-bottom: 10%;">

	<div>
			<div id="tambah" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				<div class="modal-dialog modal-lg" role="document">
    				<div class="modal-content" style="border-radius: 40px;">
    					<div class="modal-header info-color" style="border-radius: 0 30px;">
					        <h2 class="modal-title font-bold white-text" id="exampleModalLabel">Tambah</h2>
					        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
					          	<span aria-hidden="true">&times;</span>
					        </button>
					    </div>

					    <div class="modal-body">
					    	<form action="<?= base_url(); ?>admin/tambahAdmin" method="POST" class="col-md-12 row needs-validation" enctype="multipart/form-data" novalidate>

								<div class="col-md-12 row">		                    
				                    <div class="col-md-6">
				                        <div class="md-form">
				                            <input  id="nama" type="text" class="validate form-control" name="nama" required>
				                            <label for="nama">Nama</label>
				                        </div>
				                    </div>
				                    <div class="col-md-6">
				                        <div class="md-form">
				                            <input  id="kode" type="number" class="validate form-control" name="kode" required>
				                            <label for="kode">Kode Admin</label>
				                        </div>
				                    </div>
			                	</div>

			                    <div class="col-md-12 row">
			                    	<div class="col-md-12">
			                    		<h6>Jabatan</h6>
			                    	</div>
			                    	<div class="col-md-4">
			                    		<div class="form-check">
				                        	<input id="manager" class="form-check-input" type="radio" name="jabatan" value="Manager" required>
				                        	<label for="manager" class="form-check-label">Manager</label>
				                        </div>
			                        </div>
			                        <div class="col-md-4">
			                        	<div class="form-check">
				                        	<input id="finance" class="form-check-input" type="radio" name="jabatan" value="Finance" required>
				                            <label for="finance" class="form-check-label">Finance</label>
				                        </div>
			                        </div>
			                        <div class="col-md-4">
			                        	<div class="form-check">
				                            <input id="sda" class="form-check-input" type="radio" name="jabatan" value="SDA" required>
				                            <label for="sda" class="form-check-label">Sumber Daya Mahasiswa</label>
				                        </div>
			                        </div>
			                    </div>
			                   <div class="col-md-12">
			                   		<div class="md-form">
			                   			<input id="email" class="form-control validate" type="email" name="email" required>
			                   			<label for="email">Email</label>
			                   		</div>
			                   </div>
			                    <div class="col-md-12">			                    	
				                    <div class="md-form">
				                    	<input  id="password" type="password" class="validate form-control" name="password" required>
				                        <label for="password">Password</label>
									</div>									
		  						</div>
		  						<div class="col-md-12">			                    	
				                    <div class="md-form">
				                    	<input  id="password1" type="password" class="validate form-control" name="password1" required>
				                        <label for="password1">Confitm Password</label>
									</div>									
		  						</div>
					    </div>		

					   	<div class="modal-footer">
					      	<button type="submit" class="btn btn-rounded light-blue darken-1 waves-effect waves-light black-text font-bold white-text">Tambah</button>
							</form>
					    </div>

					</div>
				</div>
			</div>


		<div class="card py-3 px-3" style="border-radius: 30px;">
				<table class="table table-borderless table-hover text-center font-light table-responsive" width="100%">
					<div class="col-md-12 row">
						<div class="col-md-1">
							<button type="button" class="btn btn-floating white waves-effect waves-light floating" data-toggle="modal" data-target="#tambah"><i class="fas fa-plus blue-text"></i></button>	
						</div>
						<div class="col-md-11 text-center" style="padding: auto;">
							<h2 class="font-bold py-2">Daftar Admin</h2>
						</div>
					</div>
			        <thead>
			          	<tr>
			          		<th class="font-bold">No</th>
			          		<th class="font-bold">Kode Admin</th>
			              	<th class="font-bold">Nama</th>
			              	<th class="font-bold">Email</th>
			              	<th class="font-bold">Jabatan</th>
			              	<th class="font-bold">Option</th>
			          	</tr>
			        </thead>

			        <tbody>
			          <?php $i = 1; foreach ($admin as $ad) : ?>
				          <tr>	
				          	<td class="align-middle"><?= $i; ?></td>
				          	<td class="align-middle"><?= $ad['Kode_Admin']?></td>
				            <td class="align-middle"><?= $ad['Nama']?></td>
				            <td class="align-middle"><?= $ad['Email']?></td>
				            <td class="align-middle"><?= $ad['Jabatan']?></td>	
				            <td>
				            	<button class="btn btn-floating green waves-effect waves-light" data-toggle="modal" data-target="#edit<?= $i; ?>"><i class="fas fa-pencil"></i></button>
				            	<a href="<?= base_url(); ?>admin/hapus/2/<?= $ad['Id_Admin']; ?>" class="btn btn-floating red waves-effect waves-light"><i class="fas fa-trash"></i></a>
				            </td>
				          </tr>


				          	<div id="edit<?= $i; ?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
								<div class="modal-dialog modal-lg" role="document">
				    				<div class="modal-content" style="border-radius: 40px;">
				    					<div class="modal-header info-color" style="border-radius: 0 30px;">
									        <h5 class="modal-title font-bold white-text" id="exampleModalLabel"><?= $ad['Nama']; ?></h5>
									        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
									          	<span aria-hidden="true">&times;</span>
									        </button>
									    </div>

									    <div class="modal-body">
									    	<form action="<?= base_url(); ?>admin/editAdmin" method="POST" class="col-md-12 row needs-validation" enctype="multipart/form-data" novalidate>

												<div class="col-md-12 row">		                    
								                    <div class="col-md-6">
								                        <div class="md-form">
								                            <input  id="nama<?= $i; ?>" type="text" class="validate form-control" name="nama" value="<?= $ad['Nama']; ?>" required>
								                            <label for="nama<?= $i; ?>">Nama</label>
								                        </div>
								                    </div>
								                    <div class="col-md-6">
								                        <div class="md-form">
								                            <input  id="kode<?= $i; ?>" type="number" class="validate form-control" name="kode" value="<?= $ad['Kode_Admin']; ?>"required>
								                            <label for="kode<?= $i; ?>">Kode Admin</label>
								                        </div>
								                    </div>
							                	</div>

							                	<div class="col-md-12">
							                   		<div class="md-form">
							                   			<input id="email" class="form-control validate" type="email" name="email" value="<?= $ad['Email']; ?>" required>
							                   			<label for="email">Email</label>
							                   		</div>
							                   	</div>


							                    <div class="col-md-12 row">
							                    	<div class="col-md-12">
							                    		<h6>Jabatan</h6>
							                    	</div>
							                    	<div class="col-md-4">
							                    		<div class="form-check">
							                    			<?php if($ad['Jabatan'] == "Manager"){ ?>
								                        		<input id="manager<?= $i; ?>" class="form-check-input" type="radio" name="jabatan" value="Manager" checked required>
								                        	<?php } else {?>
								                        		<input id="manager<?= $i; ?>" class="form-check-input" type="radio" name="jabatan" value="Manager" required>
								                        	<?php } ?>
								                        	<label for="manager<?= $i; ?>" class="form-check-label">Manager</label>
								                        </div>
							                        </div>
							                        <div class="col-md-4">
							                        	<div class="form-check">
							                        		<?php if($ad['Jabatan'] == "Finance"){ ?>
								                        		<input id="finance<?= $i; ?>" class="form-check-input" type="radio" name="jabatan" value="Finance" checked required>
								                        	<?php } else {?>
								                        		<input id="finance<?= $i; ?>" class="form-check-input" type="radio" name="jabatan" value="Finance" required>
								                        	<?php } ?>
								                            <label for="finance<?= $i; ?>" class="form-check-label">Finance</label>
								                        </div>
							                        </div>
							                        <div class="col-md-4">
							                        	<div class="form-check">
							                        		<?php if($ad['Jabatan'] == "SDA"){ ?>
								                            	<input id="sda<?= $i; ?>" class="form-check-input" type="radio" name="jabatan" value="SDA" checked required>
								                            <?php } else {?>
								                            	<input id="sda<?= $i; ?>" class="form-check-input" type="radio" name="jabatan" value="SDA" required>
								                            <?php } ?>
								                            <label for="sda<?= $i; ?>" class="form-check-label">Sumber Daya Mahasiswa</label>
								                        </div>
							                        </div>
							                    </div>
									    </div>		

									   	<div class="modal-footer">
									      	<button type="submit" class="btn btn-rounded info-color waves-effect waves-light white-text font-bold">Edit</button>
									      	<input type="hidden" name="Id" value="<?= $ad['Id_Admin']; ?>">
											</form>
									    </div>

									</div>
								</div>
							</div>


			          <?php $i++; endforeach; ?>	         
			        </tbody>
			    </table>
	    </div>
	</div>
</div>