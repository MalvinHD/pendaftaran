<div class="container-fluid" style="padding-top: 10%; padding-bottom: 10%;">

		<div id="tambah" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	        <div class="modal-dialog modal-lg" role="document">
	            <div class="modal-content" style="border-radius: 40px;">

	              <div class="modal-header info-color" style="border-radius: 0 30px;">
	                  <h3 class="modal-title font-bold white-text" id="exampleModalLabel">Tambah Jadwal Ujian</h3>
	                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                      <span aria-hidden="true">&times;</span>
	                  </button>
	              </div>

	              <div class="modal-body">
	                <form action="<?= base_url(); ?>admin/tambahJadwal" method="POST" class="col-md-12 row m-0 needs-validation" enctype="multipart/form-data" novalidate>
	                <div class="col-md-12 row">
		                 <div class="col-md-4">
		                    <div class="md-form">
		                        <select id="selectJurusan" class="md-form mdb-select font-light" name="jurusan" searchable="Cari..."required>
		                            <option class="font-light" disabled selected>Pilih Jurusan</option>
		                            <?php foreach($jurusan as $jur): ?>
		                                <option class="font-light" value="<?= $jur['Kode_Jurusan']; ?>"><?= $jur['Nama_Jurusan'] ?></option>
		                            <?php endforeach; ?>
		                        </select>
		                        <label>Jurusan</label>                   
		                    </div>
		                </div>
		                <div class="col-md-4">
		                    <div class="md-form">
		                        <select id="selectSemester" class="md-form mdb-select font-light" name="semester" required>
		                            <option class="font-light" disabled selected>Pilih semester</option>
		                            <option class="font-light" value="1">Semester 1</option>
		                            <option class="font-light" value="2">Semester 2</option>
		                            <option class="font-light" value="3">Semester 3</option>
		                            <option class="font-light" value="4">Semester 4</option>
		                            <option class="font-light" value="5">Semester 5</option>
		                            <option class="font-light" value="6">Semester 6</option>
		                            <option class="font-light" value="7">Semester 7</option>
		                            <option class="font-light" value="8">Semester 8</option>
		                        </select>
		                        <label>Semester</label>                   
		                    </div>
		                </div>    
		                <div class="col-md-4">
		                	<div class="md-form">
		                        <select id="selectMatkul" class="md-form mdb-select font-light" name="matkul" searchable="Cari..." required>
		                        	<option class="font-light" disabled selected>Pilih Mata Kuliah</option>
		                        </select>
		                        <label>Mata Kuliah</label>                   
		                    </div>
		                </div>
		            </div>
					<div class="col-md-12 row my-3">
						<div class="col-md-4">
							<div class="md-form">
								<input  id="tanggal" type="date" class="validate form-control" name="tanggal" required>
								<label class="font-weight-bold black-text" for="tanggal">Tanggal Ujian</label>
							</div>
						</div>
						<div class="col-md-4">
							<div class="md-form">
								<input  id="jam" type="time" class="validate form-control" name="jam" required>
								<label class="font-weight-bold black-text" for="jam">Jam Ujian</label>
							</div>
						</div>
						<div class="col-md-4">
							<div class="md-form">
								<input  id="kelas" type="text" class="validate form-control" name="ruang" required>
								<label class="font-weight-bold" for="kelas">Ruang/Kelas Ujian</label>
							</div>
						</div>
					</div>													  			
	                <div class="modal-footer col-md-12">
	                    <button type="submit" class="btn btn-rounded light-blue darken-1 waves-effect waves-light white-text font-bold">Tambah</button>
	                </div>
	                </form>
	              </div>

	          </div>
	        </div>
	      </div>



		<div class="card py-3 px-3" style="border-radius: 30px;">
				<table id="dtMaterialDesignExample" class="table table-borderless table-hover text-center font-light table-responsive">
					<div class="col-md-12 row">
						<div class="col-md-1">
							<button type="button" class="btn btn-floating white waves-effect waves-light floating" data-toggle="modal" data-target="#tambah"><i class="fas fa-plus blue-text"></i></button>	
						</div>
						<div class="col-md-11 text-center" style="padding: auto;">
							<h2 class="font-bold py-2">Jadwal Ujian Susulan</h2>
						</div>
					</div>
			        <thead>
			          	<tr>
			          		<th class="font-bold">No</th>
			          		<th class="font-bold">Kode Ujian</th>
			              	<th class="font-bold">Mata Kuliah</th>
			              	<th class="font-bold">Jurusan</th>
			              	<th class="font-bold">Semester</th>
			              	<th class="font-bold">Tanggal</th>
			              	<th class="font-bold">Jam</th>
			              	<th class="font-bold">Ruang</th>
			              	<th class="font-bold">Opsi</th>
			          	</tr>
			        </thead>

			        <tbody>
			        <?php if($jadwal['status']) { ?>
			          <?php $i = 1; foreach ($jadwal['data'] as $jd) : ?>
				        <tr id="<?= $jd['Kode_Ujian']?>">	
				          	<td class="align-middle"><?= $i; ?></td>
				          	<td class="align-middle"><?= $jd['Kode_Ujian']?></td>
				            <td class="align-middle"><?= $jd['Nama_Matkul']?></td>
				            <td class="align-middle"><?= $jd['Nama_Jurusan']?></td>	
				            <td class="align-middle"><?= $jd['Semester']?></td>	
				            <td class="align-middle"><?= date("d F Y", strtotime($jd['Tanggal'])); ?></td>	
				            <td class="align-middle"><?= date("H:i", strtotime($jd['Jam'])); ?></td>	
				            <td class="align-middle"><?= $jd['Ruang']?></td>	
				            <td>
				            	<button class="btn btn-floating green waves-effect waves-light" data-toggle="modal" data-target="#edit<?= $i; ?>"><i class="fas fa-pencil"></i></button>
				            	<a href="<?= base_url(); ?>admin/hapus/4/<?= $jd['Id_Ujian']; ?>" class="btn btn-floating red waves-effect waves-light"><i class="fas fa-trash"></i></a>
				            </td>
				        </tr>

				        <div id="edit<?= $i; ?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					        <div class="modal-dialog modal-lg" role="document">
					            <div class="modal-content" style="border-radius: 40px;">

					              <div class="modal-header success-color" style="border-radius: 0 30px;">
					                  <h3 class="modal-title font-bold white-text" id="exampleModalLabel">Edit Jadwal Ujian</h3>
					                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
					                      <span aria-hidden="true">&times;</span>
					                  </button>
					              </div>

					              <div class="modal-body">
					                <form action="<?= base_url(); ?>admin/tambahJadwal" method="POST" class="col-md-12 row m-0 needs-validation" enctype="multipart/form-data" novalidate>
					                <div class="col-md-12 row">
						                 <div class="col-md-4">
						                    <div class="md-form">
						                        <input id="jurusan<?= $i; ?>" type="text" class="validate form-control" name="jurusan" value="<?= $jd['Kode_Jurusan']?>" disabled>
						                        <label for="jurusan<?= $i; ?>">Kode Jurusan</label>                   
						                    </div>
						                </div>
						                <div class="col-md-4">
						                    <div class="md-form">
						                        <input id="semester<?= $i; ?>" type="text" class="validate form-control" name="semester" value="Semester <?= $jd['Semester']?>" disabled>
						                        <label for="semester<?= $i; ?>">Semester</label>  
						                    </div>
						                </div>    
						                <div class="col-md-4">
						                	<div class="md-form">
						                        <select id="selectMatkul" class="md-form mdb-select font-light" name="matkul" searchable="Cari..." required>
						                        	<option class="font-light" disabled selected>Pilih Mata Kuliah</option>
						                        	<?php foreach($matkul as $mk) :
						                        		if($mk['Semester']==$jd['Semester'] && $mk['Kode_Jurusan']==$jd['Kode_Jurusan']){
						                        	?>
						                        		<option class="font-light" value="$mk['Nama_Matkul']"><?= $mk['Nama_Matkul'] ?></option>
						                        	<?php } endforeach; ?>
						                        </select>
						                        <label>Mata Kuliah</label>                   
						                    </div>
						                </div>
						            </div>
									<div class="col-md-12 row my-3">
										<div class="col-md-4">
											<div class="md-form">
												<input  id="tanggal" type="date" class="validate form-control" name="tanggal" value="<?= $jd['Tanggal']?>" required>
												<label class="font-weight-bold black-text" for="tanggal">Tanggal Ujian</label>
											</div>
										</div>
										<div class="col-md-4">
											<div class="md-form">
												<input  id="jam" type="time" class="validate form-control" name="jam" value="<?= $jd['Jam']?>" required>
												<label class="font-weight-bold black-text" for="jam">Jam Ujian</label>
											</div>
										</div>
										<div class="col-md-4">
											<div class="md-form">
												<input  id="kelas" type="text" class="validate form-control" name="ruang" value="<?= $jd['Ruang']?>" required>
												<label class="font-weight-bold" for="kelas">Ruang/Kelas Ujian</label>
											</div>
										</div>
									</div>													  			
					                <div class="modal-footer col-md-12">
					                    <button type="submit" class="btn btn-rounded success-color waves-effect waves-light white-text font-bold">Edit</button>
					                </div>
					                </form>
					              </div>

					          </div>
					        </div>
					      </div>


				          	


			          <?php $i++; endforeach; ?>	         
			        <?php } ?>
			        </tbody>
			    </table>
	    </div>
	</div>
</div>