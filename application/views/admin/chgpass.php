<?php 
	$dataadmin = $admin['data'];

	$da = $dataadmin;
	
?>

<div class="container col-md-12">
	<div class="row">
		<div class="card py-3 px-3 my-5 col-md-8 mx-auto" style="border-radius: 30px;">
			<div class="card-title">
				<h1>Hai, <?= $da['Nama']; ?>.</h1>
			</div>
			<div class="card-body">			
					<div class="col-md-12">
						Disini adalah informasi mengenai anda :
					</div>

					<div class="col-md-12 mt-4">
						<div>
							Kode admin<span class="ml-2">: <?= $da['Kode_Admin']?></span>
						</div>
					</div>

					<div class="col-md-12 my-2">
						<div>
							Nama<span class="ml-5">: <?= $da['Nama']?></span>
						</div>
					</div>

					<div class="col-md-12 my-2">
						<div>
							Email<span class="ml-5">: <?= $da['Email']?></span>
						</div>
					</div>

					<div class="col-md-12 my-2">
						<div>
							Jabatan<span class="ml-4">: <?= $da['Jabatan']?></span>
						</div>
					</div>

					<div class="col-md-12 my-2">
						<form class="row needs-validation" action="<?= base_url(); ?>admin/chgpw/<?= $da['Status']; ?>" method="POST" >
							<div class="col-md-6">			                    	
				                <div class="md-form">
				                   	<input  id="password" type="password" class="validate form-control" name="password" required>
				                    <label for="password">Password</label>
								</div>									
		  					</div>
		  					
		  					<div class="col-md-6">			                    	
				                <div class="md-form">
				                   	<input  id="password1" type="password" class="validate form-control" name="password1" required>
				                    <label for="password1">Confirm Password</label>
								</div>									
		  					</div>
							<div class="col-md-12">
								<input type="hidden" name="id" value="<?= $da['Id_Admin']; ?>">
								<button type="submit" class="btn btn-info btn-rounded float-right font-bold">Submit</button>
							</div>
						</form>
					</div>
			</div>



		</div>
	</div>
</div>