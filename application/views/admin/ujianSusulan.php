<div class="container" style="padding-top: 10%; padding-bottom: 10%;">
    <div class="card py-3 px-3" style="border-radius: 30px;">
        <table id="dtMaterialDesignExample" class="table table-borderless table-hover text-center table-responsive">
              <div class="text-center my-2">
                  <h2 class="font-bold">Mahasiswa Yang Susulan</h2>
              </div>
              <thead>
                  <tr>
                    <th class="font-bold">No</th>
                    <th class="font-bold">Kode Ujian</th>
                    <th class="font-bold">NIM</th>
                    <th class="font-bold">Nama Jurusan</th>
                    <th class="font-bold">Mata Kuliah</th>
                    <th class="font-bold">Tanggal</th>
                    <th class="font-bold">Jam</th>
                    <th class="font-bold">Ruang</th>
              <tbody>
                <?php if($ujiansusulan['status'] ) {?>
                  <?php $i = 1; foreach ($ujiansusulan['data'] as $uj) : ?>
                    <tr>  
                      <td><?= $i; ?></td>
                      <td><a href="<?= base_url() ?>admin/dataJadwalUjian#<?= $uj['Kode_Ujian']; ?>"><?= $uj['Kode_Ujian']?></a></td>
                      <td><a href="<?= base_url() ?>admin/dataMahasiswa#<?= $uj['NIM']; ?>"><?= $uj['NIM']?></a></td>
                      <td><?= $uj['Nama_Jurusan']?></td> 
                      <td><?= $uj['Nama_Matkul']?></td> 
                      <td><?= date("d F Y", strtotime($uj['Tanggal'])); ?></td> 
                      <td><?= date("H:i", strtotime($uj['Jam'])); ?></td> 
                      <td><?= $uj['Ruang']?></td> 
                    </tr>
                  <?php $i++; endforeach; ?>           
                <?php } ?>
              </tbody>
          </table>
      </div>
</div>