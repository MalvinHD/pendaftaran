<div class="container-fluid" style="padding-top: 10%; padding-bottom: 10%;">
    <div class="card py-3 px-3" style="border-radius: 30px;">
        <table id="dtMaterialDesignExample" class="table table-borderless table-hover text-center table-responsive" width="100%" >
              <div class="text-center my-2">
                  <h2 class="font-bold">Daftar Skripsi</h2>
              </div>
              <thead>
                  <tr>
                    <th class="font-bold">No</th>
                    <th class="font-bold">Kode.Sk</th>
                    <th class="font-bold">NIM</th>
                    <th class="font-bold">Kode.Jur</th>
                    <th class="font-bold">Judul Bahasa Indonesia</th>
                    <th class="font-bold">Judul Bahasa Inggris</th>
                  </tr>
              </thead>

              <tbody>
                <?php if($skripsi['status']) { ?>
                <?php $i = 1; foreach ($skripsi['data'] as $sk) : ?>
                  <tr>  
                    <td><?= $i; ?></td>
                    <td><?= $sk['Kode_Skripsi']?></td>
                    <td><a href="<?= base_url() ?>admin/dataMahasiswa#<?= $sk['NIM']; ?>"><?= $sk['NIM']?></a></td>
                    <td><?= $sk['Kode_Jurusan']?></td> 
                    <td><?= $sk['Judul_BI']?></td> 
                    <td><?= $sk['Judul_EN']?></td> 
                  </tr>
                <?php $i++; endforeach; ?>    
                <?php } ?>       
              </tbody>
          </table>
      </div>
</div>