<style>
.footer {
   position: fixed;
   left: 0;
   bottom: 0;
   width: 100%;
   background-color: red;
   color: white;
   text-align: center;
}
</style>
      <div data-aos="fade-up">
        <footer class="page-footer col l6 s12" style="background-color: #3f4c6b; ">
          <div class="container">
              <div class="row">
                <div class="col l4 s12" style="padding-right: 50px; width: 300px;">
                  <h5 class="white-text font-bold"><i class="fas fa-building"></i>  Kalbis Institute</h5>
                  <h6 class="grey-text text-lighten-4 font-light" style="float: left;">
                      Menjadi perguruan tinggi pilihan di bidang sains, teknologi dan bisnis, menghasilkan lulusan berkualitas dan siap terap, serta berorientasi pada inovasi, kewirausahaan dan globalisasi.
                  </h6>
                </div>

                <div class="col l4 s12" style="padding-left: 100px;">
                  <h5 class="white-text font-bold"><i class="fas fa-info-circle"></i>  More About Us!</h5>
                  <ul class="font-light">
                    <li><a class="grey-text text-lighten-3 font-bold" href="http://kalbis.ac.id/">Kalbis Institute</a></li>
                    <div class="icon" style="margin: auto;">
                        <a class="grey-text text-lighten-3 hoverable" href="https://www.instagram.com/operasionalkalbis/" target="_blank">
                          <li class="fab fa-instagram" style="font-size:50px; "></li>
                        </a>
                        <a class="hoverable" href="mailto:student.service@kalbis.ac.id" target="_blank">
                          <li class="fas fa-envelope white-text" style="font-size: 45px; padding-left: 9px;"></li>
                        </a>
                    </div>
                  </ul>
                </div>

                  <div class="col l4 s12" style="padding-left: 130px; color: white;">
                    <h5 class="font-bold"><i class="fas fa-map-pin"></i>  Location</h5>
                     <p class="font-light"><span>Jl. Pulomas Selatan Kav. No.22, Kayu Putih, Kec. Pulo Gadung, Kota Jakarta Timur</span></p>
                  </div>
              </div>
          </div>
          <div class="footer-copyright" style="text-align: center;">
            <div class="container font-light">
              <p>© 2020 Design Created By<span class="font-bold"> LAG Developer Team</span></p>
            </div>
          </div>

        </footer>

    </div>

</body>

 <div class="fixed-action-btn horizontal">
    <a class="btn-floating btn-large white floating transisi">
      <i class="large material-icons black-text">blur_on</i>
    </a>
    <ul>
      <li>
        <a href="#modal1" class="btn-floating white tooltipped modal-trigger floating transisi" data-position="top" data-delay="35" data-tooltip="Profile">
         <i class="material-icons black-text">account_circle</i>
        </a>
      </li>
      <li>
        <a href="index" class="btn-floating white tooltipped floating transisi" data-position="top" data-delay="35" data-tooltip="Home">
          <i class="material-icons black-text">home</i>
        </a>
      </li>
    </ul>
  </div>

  <!-- Modal Structure -->
  <div id="modal1" class="modal" style="color: #000; border-radius: 20px;">
    <div class="modal-content" style=" margin: 0px; padding: 0px;">
      <div class="modal-title" style="background-color: #203A43;">
          <h2 class="font-bold white-text" style="padding-left: 3%; padding-top: 3%; padding-bottom: 3%;"><?= $this->session->userdata('nama'); ?></h2>
      </div>
      <div style="padding: 0 3%;">
          <p class="font-light">NIM       : <?= $this->session->userdata('nim'); ?></p>
          <p class="font-light">Jurusan   : <?= $this->session->userdata('namaJurusan'); ?></p>
          <p class="font-light">Fakultas  : <?= $this->session->userdata('fakultas'); ?></p>
          <p class="font-light">Program   : <?= $this->session->userdata('program'); ?></p>
          <p class="font-light">Kelas     : <?= $this->session->userdata('kelas'); ?></p>
          <p class="font-light">Semester  : <?= $this->session->userdata('semester'); ?></p>
          <p class="font-light">Email     : <?= $this->session->userdata('email'); ?></p>
          <p class="font-light">No.Telp   : <?= $this->session->userdata('telepon'); ?></p>
      </div>
    </div>
  </div>
  
  <!-- Compiled and minified JavaScript -->
  <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script src="../materialize/js/materialize.js"></script>
  <script src="../materialize/js/materialize.min.js"></script>
  <script src="../materialize/js/sweetalert2.all.min.js"></script>
  <script src="../materialize/js/sweetalert2.min.js"></script>
  <script src="../MDB/js/aos.js"></script>
  <script src="../materialize/js/script.js"></script>
  <!-- nyoba doang <script type="text/javascript" src="../materialize/js/sweetalert2.min.js"></script> -->


</html>