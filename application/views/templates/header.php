<!DOCTYPE html>
<html lang="en">
<head>
	
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

	<!-- Compiled and minified CSS -->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  	<link type="text/css" rel="stylesheet" href="materialize/css/materialize.min.css">
  	<link type="text/css" rel="stylesheet" href="MDB/css/aos.css">
  	<link rel="stylesheet" type="text/css" href="materialize/css/sweetalert2.min.css">
  	<link type="text/css" rel="stylesheet" href="materialize/css/style.css">

	<title><?= $judul ?></title>

<style type="text/css">

body {
	margin: auto;
	background: #203A43;
	min-height: 100vh;
}
</style>


</head>

<body>