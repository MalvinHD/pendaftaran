<!DOCTYPE html>
<html lang="en">
<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  	<!-- <link type="text/css" rel="stylesheet" href="materialize/css/materialize.min.css"> -->
  	<link rel="stylesheet" href="MDB/css/font-awesome.min.css">
  	<link rel="stylesheet" href="MDB/css/bootstrap.min.css">
  	<link rel="stylesheet" href="MDB/css/mdb.css">
  	<link rel="stylesheet" href="MDB/css/aos.css">
    <link rel="stylesheet" href="MDB/css/all.css">
  	<link rel="stylesheet" href="MDB/css/owl.carousel.min.css">
  	<link rel="stylesheet" href="MDB/css/jquery.fancybox.min.css">
  	<link rel="stylesheet" href="MDB/css/style.css">

	
	<title><?= $judul ?></title>

</head>

<body class="font-light" style="background: #203A43 url(images/bg.png) no-repeat fixed center; width: 100%; min-height: 100%; position: fixed;>