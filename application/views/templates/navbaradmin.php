<!-- NAVBAR -->
<div>
	<nav class="navbar navbar-dark fixed-top navbar-expand-lg transisi white custom py-3">
      <div class="container col-lg-10">

        <a class="navbar-brand p-0" href="<?= base_url(); ?>admin/home">
            <img id="logo" src="../images/logo.png" class="d-inline-block align-bottom mr-2" height="60">
        </a>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarmenu" aria-controls="navbarmenu" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarmenu">
            <ul class="navbar-nav ml-auto text-uppercase smooth-scroll list-unstyled">
                <li class="nav-item mx-2">
                    <a class="nav-link font-light black-text btn btn-flat" href="<?= base_url(); ?>admin/home">Home</a>
                </li>
                <li class="nav-item mx-2 dropdown">
                    <a class="nav-link font-light dropdown-toggle black-text btn btn-flat" id="ujsul" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Ujian Susulan</a>
                    <div class="dropdown-menu dropdown-secondary" aria-labelledby="ujsul">
                        <a class="dropdown-item font-light" href="<?= base_url(); ?>admin/dataJadwalUjian">Jadwal Ujian</a>
                        <a class="dropdown-item font-light" href="<?= base_url(); ?>admin/dataUjianSusul">Mahasiswa Ujian Susulan</a>                        
                    </div>
                </li>

                <li class="nav-item mx-2">
                    <a class="nav-link font-light black-text btn btn-flat" href="<?= base_url(); ?>admin/dataSkripsi">Judul Skripsi</a>
                </li>

                <li class="nav-item mx-2 dropdown">
                    <a class="nav-link font-light dropdown-toggle black-text btn btn-flat" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Data</a>
                    <div class="dropdown-menu dropdown-secondary" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item font-light" href="<?= base_url(); ?>admin/dataMatkul">Mata Kuliah</a>
                        <a class="dropdown-item font-light" href="<?= base_url(); ?>admin/dataMahasiswa">Mahasiswa</a>
                        <a class="dropdown-item font-light" href="<?= base_url(); ?>admin/dataAdmin">Admin</a>
                    </div>
                </li>

                <li class="nav-item mx-2 dropdown">
                    <a class="nav-link font-light dropdown-toggle black-text btn btn-flat" id="menuadmin" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">About</a>
                    <div class="dropdown-menu dropdown-secondary" aria-labelledby="menuadmin">
                        <!-- <a class="dropdown-item font-light" href="<?= base_url(); ?>admin/dataMatkul">Edit Profil</a> -->
                        <a class="dropdown-item font-light" data-toggle="modal" data-target="#chgpass">Change Password</a>
                        <a class="dropdown-item font-light" href="<?= base_url(); ?>admin/logout">Log Out</a>
                    </div>
                </li>
            </ul>
        </div>

      </div>
    </nav>
 <div>


        <div id="chgpass" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content" style="border-radius: 40px;">

                  <div class="modal-header danger-color" style="border-radius: 0 30px;">
                      <h3 class="modal-title font-bold white-text" id="exampleModalLabel">Change Password</h3>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                      </button>
                  </div>

                  <div class="modal-body">
                    <form action="<?= base_url(); ?>admin/gantipassword" method="POST" class="col-md-12 row m-0 needs-validation" enctype="multipart/form-data" novalidate>
                    <div class="col-md-12 mt-3">
                        <div class="md-form">
                            <input  id="passlama" type="password" class="validate form-control" name="passlama" required>
                            <label class="font-weight-bold black-text" for="passlama">Current Password</label>
                        </div>
                    </div>
                    <div class="col-md-12  mt-3">
                        <div class="md-form">
                            <input  id="passbaru" type="password" class="validate form-control" name="password" required>
                            <label class="font-weight-bold black-text" for="passbaru">New Password</label>
                        </div>
                    </div>                              
                    <div class="col-md-12 mt-3">
                        <div class="md-form">
                            <input  id="passbaru1" type="password" class="validate form-control" name="password1" required>
                            <label class="font-weight-bold black-text" for="passbaru1">Confirm Password</label>
                        </div>
                    </div>                                                                                              
                    <div class="modal-footer col-md-12">
                        <input type="hidden" name="id" value="<?= $this->session->userdata('id'); ?>">
                        <button type="submit" class="btn btn-rounded danger-color waves-effect waves-light white-text font-bold">Ubah</button>
                    </div>
                    </form>
                  </div>

              </div>
            </div>
        </div>

<!-- NAVBAR -->