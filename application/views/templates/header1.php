<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<!-- Compiled and minified CSS -->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  	<link type="text/css" rel="stylesheet" href="../materialize/css/materialize.min.css">
  	<link type="text/css" rel="stylesheet" href="../MDB/css/aos.css">
  	<link rel="stylesheet" type="text/css" href="../MDB/css/all.css">
  	<link rel="stylesheet" type="text/css" href="../materialize/css/sweetalert2.min.css">
  	<link type="text/css" rel="stylesheet" href="../materialize/css/style.css">
  	
    <!--Let browser know website is optimized for mobile-->
	<style type="text/css">
	body{
		background-color: #203A43;
	}
	nav{
		background-color: white; 
		/*padding-right: 200vh;*/
	}
	</style>
	<title><?= $judul; ?></title>
</head>

<body>
	<div>
		<nav class="navbar navcustom transisi" style="height: 90px;"> 
			<div class="container" style="width: 100%">
				<img  class="left" src="../images/kalbis.png" style="height: 80px; width: 230px; padding-top: 10px;">
		  		<a class='black-text btnside right' href='#' data-activates='slide-out' style="margin: auto 0;"><i class="material-icons">menu</i></a>
	  		</div>
	    </nav>
			<!-- Modal Structure -->
			<div id="modal" class="modal modal-fixed-footer" style="color: #000;">
			    <div class="modal-content">
					<h1>Change Password</h1>
					<?php echo $this->session->flashdata('message'); ?>
				    <form method="POST" action="<?= base_url(); ?>user/change_pass">
				    <table>
				    <tr>
				    <td>Enter your existing password:</td>
				    <input type="hidden" name="id" value="<?= $this->session->userdata('id'); ?>">
				    <td><input type="password" size="10" name="current_password"></td>
				    <?= form_error('current_password', '<small class="form-text text-danger pl-3">', '</small>'); ?>
				    </tr>
				  	<tr>
				    <td>Enter your new password:</td>
				    <td><input type="password" size="10" name="new_password1"></td>
				    <?= form_error('new_password1', '<small class="form-text text-danger pl-3">', '</small>'); ?>
				    </tr>
				    <tr>
				    <td>Re-enter your new password:</td>
				    <td><input type="password" size="10" name="new_password2"></td>
				    <?= form_error('new_password2', '<small class="form-text text-danger pl-3">', '</small>'); ?>
				    </tr>
				    </table>
				</div>
			    <div class="modal-footer">
			      <button type="submit" class="modal-action modal-close waves-effect waves-green btn-flat">Change Password</button>
			      		<?php if ($this->session->flashdata('message')) : ?>
                          <div class="row">
                            <div class="col s6 center">
                              <?php endif; ?>
                            </div>
                          </div>
			      </form>
			    </div>
			</div>
	</div>

		<ul id="slide-out" class="side-nav">
		    <li>
		    	<div class="user-view">
			      	<div class="background" style="background-color: #203A43;"></div>
			      	<img class="circle" style="width: 100px; height: 100px;" src="../images/atlass.jpg">
			      	<span class="white-text name"><?= $this->session->userdata('nama'); ?></span>
			      	<span class="white-text email"><?= $this->session->userdata('email'); ?></span>
		    	</div>
		    </li>
		    <div class="background-down">
			    <li>
			    	<a class="black-text waves-effect waves-light modal-trigger font-light" href="#modal"><i class="material-icons left">security</i>Change Password</a>
			    </li>
			    <li>
			    	<a class="black-text waves-effect waves-light font-light" href="<?= base_url(); ?>user/cekStatus"><i class="material-icons left">today</i>Status</a>
			    </li>
			    <li>
			    	<div class="divider"></div>
			    </li>
			    <li>
			    	<a class="black-text waves-effect waves-light font-light" style="font-size: 14pt;" href="<?= base_url();?>home/logout"><i class="material-icons left">chevron_right</i>Log Out</a>
			    </li>
			</div>
		</ul>	
