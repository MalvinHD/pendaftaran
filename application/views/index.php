  <style type="text/css">
	.page-footer {
    flex: 1 0 auto;
  	margin-left: -5%;
  	background: #2c3e50;
  }

  .navbar{
  	box-shadow: 0px 5px 5px #304352;
  	height: 90px;
  	background: none;
  	flex: 1 0 auto;
  }
  canvas {
  display: block;
  width: 100vw;
  height: 100vh;
  }
  body,
  html{
    position: absolute;
    display: flex;
    flex-wrap: wrap;
    justify-content: center;
    align-items: center;
    width: 100%;
    height: 90.8vh;
    background-color: #fff;
  }

  /*footer css*/
  @import url(https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400);
  body{
    font-weight:300;
    background-color:lightblue;
    color:#FFF;
  }
  section{
    text-align:center;
  }
  .footer #button{
    width:35px;
    height:35px;
    border-left: 25px solid transparent;
    border-right: 25px solid transparent;
    border-bottom: 50px solid #555;
    margin:0 auto;
    position:relative;
    -webkit-transition: all 1s ease;
    -moz-transition: all 1s ease;
    -o-transition: all 1s ease;
    -ms-transition: all 1s ease;
    transition: all 1s ease;
  }
  .footer #button:hover{
    width:35px;
    height:35px;
    border: #3A3A3A 12px solid;
    -webkit-transition: all 1s ease;
    -moz-transition: all 1s ease;
    -o-transition: all 1s ease;
    -ms-transition: all 1s ease;
    transition: all 1s ease;
    position:relative;
  }
  .footer {
    bottom:0;
    left:0;
    position:fixed;
    width: 100%;
    height: 2em;
    overflow:hidden;
    margin:0 auto;
    -webkit-transition: all 1s ease;
    -moz-transition: all 1s ease;
    -o-transition: all 1s ease;
    -ms-transition: all 1s ease;
    transition: all 1s ease;
    z-index:999;
  }
  .footer:hover {
    -webkit-transition: all 1s ease;
      -moz-transition: all 1s ease;
      -o-transition: all 1s ease;
      -ms-transition: all 1s ease;
      transition: all 1s ease;
    height: 22em;
  }
  .footer #container{
    margin-top:5px;
    width:100%;
    height:100%;
    position:relative;
    top:0;
    left:0;
    background: #3f4c6b;
  }
  .footer #cont{
    position:relative;
    top:-30px;
    right:190px;
    width:150px;
    height:auto;
    margin:0 auto;
  }
  .footer_center{
    width:500px;
    float:left;
    text-align:center;
  }
  .footer h3{
    font-family: 'Helvetica';
    font-size: 30px;
    font-weight: 100;
    margin-top:70px;
    margin-left:40px;
  }/*end footer css*/
  
  .banner-overlay {
    width: 100%;
    height: auto;
    position: absolute;
    background-color: rgba(0, 0, 0, 0.1);
    top: 0;
    left: 0;
    opacity: 1;
    transform: scale(1);
    transition: 0.3s all ease;
    -webkit-transition: 0.3s all ease;
    -moz-transition: 0.3s all ease;
    -o-transition: 0.3s all ease;
    border-radius: 20px;
  }

</style>
	
	<div class="row col s12" style="height: 100vh;">
    
      <canvas id="c"></canvas>
	
			<div class="col s4 offset-s4 banner-overlay" style="margin-top: 10%;" data-aos="fade-up" data-aos-delay="400">
				  <div class="card" style="border-radius: 20px;">
					     <div class="card-content">
						      <div class="card-title">
							         <img src="images/kalbis.png" style="width: 100%;">
						      </div>
						      
                  <div>
        							<form class="form-login" method="post" action="<?= base_url(); ?>home">
        								<input type="text" class="black-text font-light" name="nim" placeholder="NIM" value='<?= set_value('nim'); ?>'>
        								<?= form_error('nim', '<small class="form-text black-text text-danger pl-3">', '</small>'); ?>

        								<input type="password" class="black-text font-light" name="password" placeholder="Password" value='<?= set_value('password'); ?>'>
        								<?= form_error('password', '<small class="form-text black-text text-danger pl-3">', '</small>'); ?>
                        <?php if ($this->session->flashdata('message')) : ?>
                          <div class="row">
                            <div class="col s6 center">
                              <?php endif; ?>
                            </div>
                          </div>
                          <?php echo $this->session->flashdata('message'); ?>
        								<div class="card-action" style="text-align: center; height: 95px;">
                            <button class="waves-effect waves-light btn close font-bold col s12" data-dismiss="alert" name="login" style="margin:auto; margin-top: 5px; " type="submit" value="LOGIN">LOGIN</button>
                                </div>
                              </div>
                            </div>
        								</div>
        							</form>
        					</div>
					     </div>
				  </div>
			</div>
  </div>
<div class="footer">
    <div id="button"></div>
        <div id="container">
            <div id="cont">
                <div class="footer_center">
                    <h3 class="white-text">Kalbis Institute</h3>
                    <p class="grey-text text-lighten-4">Menjadi perguruan tinggi pilihan di bidang sains, teknologi dan bisnis, menghasilkan lulusan berkualitas dan siap terap, serta berorientasi pada inovasi, kewirausahaan dan globalisasi.</p>
                    <h5 class="white-text">More About Us!</h5>
                    <ul>
                      <li><a class="grey-text text-lighten-3" href="http://kalbis.ac.id/">Kalbis Institute</a></li>
                    </ul>

                    <div class="footer-copyright">
                        © 2020 Design Created by LAG Developer Team
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


