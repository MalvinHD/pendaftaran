<style type="text/css">
  .card{
    background-color: transparent;
    box-shadow: none;
  }

    .container .card{
        position: relative;
        cursor: pointer;
    }

    .container .card .face{
        width: 300px;
        height: 200px;
        transition: 0.5s;
    }

    .container .card .face.face1{
        position: relative;
        background: #333;
        display: flex;
        justify-content: center;
        align-items: center;
        z-index: 1;
        transform: translateY(100px);
    }

    .container .card:hover .face.face1{
        background: #a8e063;
        transform: translateY(0);
    }

    .container .card .face.face1 .content{
        opacity: 0.2;
        transition: 0.5s;
    }

    .container .card:hover .face.face1 .content{
        opacity: 1;
    }

    .container .card .face.face1 .content img{
        max-width: 100px;
    }

    .container .card .face.face1 .content h3{
        margin: 10px 0 0;
        padding: 0;
        color: #fff;
        text-align: center;
        font-size: 1.5em;
    }

    .container .card .face.face2{
        position: relative;
        background: #fff;
        display: flex;
        justify-content: center;
        align-items: center;
        padding: 20px;
        box-sizing: border-box;
        box-shadow: 0 20px 50px rgba(0, 0, 0, 0.8);
        transform: translateY(-100px);
    }

    .container .card:hover .face.face2{
        transform: translateY(0);
    }

    .container .card .face.face2 .content p{
        margin: 0;
        padding: 0;
    }

    .container .card .face.face2 .content a{
        margin: 15px 0 0;
        display:  inline-block;
        text-decoration: none;
        font-weight: 900;
        color: #333;
        padding: 5px;
        border: 1px solid #333;
    }

    .container .card .face.face2 .content a:hover{
        background: #333;
        color: #fff;
    }
    .halDepan {
      background-position: center center;
      margin-left: 23px;
    }
    .parallax-container {
      margin-bottom: 60px;
      height: 600px;
    }

    .parallax_img{
      width: 100px;
    }
    .page-footer {
  /*  flex: 1 0 auto;*/
    background: #2c3e50;
  }
  .banner-overlay {
    width: 100%;
    height: auto;
    margin-left: 10vh;
    margin-top: 60vh;
    position: absolute;
    opacity: 1;
    transform: scale(1);
    transition: 0.3s all ease;
    -webkit-transition: 0.3s all ease;
    -moz-transition: 0.3s all ease;
    -o-transition: 0.3s all ease;
    border-radius: 20px;
  }


</style>
  <div class="parallax-container col s6">
    <div class="parallax" style="width: 100%; height: 120%"><img src="../images/undraw.svg" style="background: white;"></div>
    <h1 class="banner-overlay font-bold" data-aos="zoom-in-up" data-aos-anchor-placement="top-bottom"  data-aos-duration="1500">Welcome To Our Website</h1>
  </div>

  <h2 class="font-bold" style="color: white; padding-left: 25px; height: 100px;" data-aos="fade-up">
    Hai, <?= $this->session->userdata('nama'); ?>      
  </h2>
  
  <h5 class="font-light" style="color: white; padding-left: 25px;" data-aos="fade-up">
      Ini adalah Website Layanan Mahasiswa Kalbis Institute.
  </h5>
  
    <div class="container" data-aos="fade-up" data-aos-duration="800">
        <div class="col s12 row">
            <div class="card col s12 l4">
                <div class="face face1">
                    <div class="content">
                        <img src="../images/design1.png" style="margin-left: 15px;">
                        <h3 class="font-bold">Ujian Susulan</h3>
                    </div>
                </div>
                <div class="face face2">
                    <div class="content">
                        <p class="font-light">Form ujian susulan untuk mahasiswa yang mengulang ujian(UTS/UAS).</p>
                            <a href="ujiansusulan" class="font-bold transisi">Daftar Form!</a>
                    </div>
                </div>
            </div>
            
            <div class="card col s12 l4 m4">
                <div class="face face1">
                    <div class="content">
                        <img src="../images/design1.png" style="margin-left: 12vh;">
                        <h3 class="font-bold">Pengajuan Proposal Skripsi</h3>
                    </div>
                </div>
                <div class="face face2">
                    <div class="content">
                        <p class="font-light">Formulir Pengajuan proposal skripsi untuk mahasiswa yang akan melakukan tugas akhir.</p>
                            <a href="Proposal" class="font-bold transisi">Daftar Form!</a>
                    </div>
                </div>
            </div>
            
            <div class="card col s12 l4 m4">
                <div class="face face1">
                    <div class="content">
                        <img src="../images/design1.png" style="margin-left: 10vh;">
                        <h3 class="font-bold">Perubahan Judul Skripsi</h3>
                    </div>
                </div>
                <div class="face face2">
                    <div class="content">
                        <p class="font-light">Formulir perubahan judul skripsi untuk mahasiswa yang ingin mengubah judul skripsi yang sudah dibuat.</p>
                            <a href="formPerubahanJudul" class="font-bold transisi">Daftar Form!</a>
                    </div>
                </div>
            </div>
        </div>
    </div>