<div data-aos="zoom-out-down">
<div class="table-status container" style="margin-top: 100px; min-height: 60vh;">
<table class="responsive-table" style="color: #fff;">
        <thead>
          <tr>
              <th>No</th>
              <th>Kode</th>
              <th>Jenis</th>
              <th>Judul/Mata Kuliah</th>
              <th>Tanggal Diajukan</th>
              <th>Status</th>
          </tr>
        </thead>

        <tbody>
          <?php $i = 1;  if($skripsi['status']){ ?>
            <?php foreach ($skripsi['data'] as $sk) : ?>
            <tr>
              <td><?= $i; ?></td>
              <td><?= $sk['Kode_Skripsi']?></td>
              <td>Proposal Pengajuan Skripsi</td>
              <td><?= $sk['Judul_BI']?></td>                   
              <td><?= date("d F Y", strtotime($sk['Tanggal_Diajukan'])); ?></td>                   
              <td><?= $sk['Status']?></td>                   
            </tr>
            <?php $i++; endforeach; ?>
          <?php } ?>

          <?php if($daftarubahjudul['status']){ ?>
            <?php foreach ($daftarubahjudul['data'] as $duj) : ?>
            <tr>
              <td><?= $i; ?></td>
              <td><?= $duj['Kode_Skripsi']?></td>
              <td>Perubahan Judul Skripsi</td>
              <td><?= $duj['Judul_BIbaru']?></td>                   
              <td><?= date("d F Y", strtotime($duj['Tanggal_Diajukan'])); ?></td>                   
              <td><?= $duj['Status']?></td>                   
            </tr>
            <?php $i++; endforeach; ?>
          <?php } ?>

          <?php if($ujian['status']){?>
            <?php foreach ($ujian['data'] as $us) : ?>
            <tr>
              <td><?= $i; ?></td>
              <td><?= $us['Kode_Form']?></td>
              <td>Ujian Susulan</td>
              <td><?= $us['Nama_Matkul']?></td>                   
              <td><?= date("d F Y", strtotime($us['Tanggal_Diajukan'])); ?></td>                   
              <td><?= $us['Status']?></td>                   
            </tr>
            <?php $i++; endforeach; ?>
          <?php } ?>

        </tbody>
      </table>
    </div>
  </div>