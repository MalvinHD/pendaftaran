<style type="text/css">
	body{
		background: #203A43;
		color: #ffffff;
	}
	div.row{
		padding: 15px;
        color: black;
	}
    [type="radio"].with-gap:disabled:checked+label:after{
        background-color: #203A43;
    }
    [type="radio"].with-gap:disabled:checked+label:before{
        background-color: white;
    }
</style>

<body>
    
    <div class="container" style="padding-top: 20px;">
        <div class="row">

            <?php if($cekproposal['status']) {?>
            <div style="height: 60vh;">
                <h1 class="white-text font-bold">
                    Proposal Skripsi Yang Anda Ajukan Sedang Diproses.
                </h1>
            </div>

        <?php } else {?>

            <?php if($skripsi['status']){ ?>
                <div style="height: 60vh;">
                    <h1 class="white-text font-bold">
                        Anda Sudah Mengajukan Proposal Skripsi, Jadi Tidak Bisa Mengajukan Lagi.
                    </h1>
                </div>                
            <?php } else { ?>
            <div>
                <div class="card col s12 offset-s1 z-depth-12" style="border-radius: 20px;" data-aos="fade-up" data-aos-duration="1000">
                    <div class="card-content">
                        <h4 class="font-bold" style="padding-left: 30px;" data-aos="fade-down" data-aos-duration="1000">Formulir Pengajuan Proposal Tugas Akhir</h4>
                        <div class="row">
                            <form class="col-s12" method="post" action="<?= base_url() ?>user/ajuinproposal">
                                <div class="row">
                                    <div class="col s12">
                                        <div class="font-light col s6">
                                            <h6 class="col s12 font-bold">Tahun Akademik</h6>
                                                <div class="input-field col s5" style="margin-top: 0;">
                                                    <input id="tahunA" type="number" name="tahunA" class="validate" placeholder="Dari">
                                                     <?= form_error('tahunA', '<small class="form-text black-text text-danger pl-3">', '</small>');?>
                                                </div>
                                                <div class="col s1" style="padding-top: 5%;">
                                                    /    
                                                </div>
                                                <div class="input-field col s5" style="margin-top: 0;">
                                                    <input id="tahunB" type="number" name="tahunB" class="validate" placeholder="Ke">
                                                     <?= form_error('tahunB', '<small class="form-text black-text text-danger pl-3">', '</small>');?>
                                                </div>
                                        </div>
                                        <div class="input-field font-light col s6">
                                        	<label for="semester">Semester</label>
                                        	<input id="semester" type="text" name="semester" value="Semester <?= $this->session->userdata('semester'); ?>" class="font-light validate black-text" disabled>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="input-field font-light col s6">
                                            <input id="nim" type="text" name="nim" value="<?= $this->session->userdata('nim'); ?>" class="font-light validate  black-text" disabled>
                                            <input type="hidden" name="nim" value="<?= $this->session->userdata('nim'); ?>">
                                            <label for="nim">NIM</label>
                                        </div>
                                        <div class="input-field font-light col s6">
                                            <input id="telp" type="text" name="telp" value="<?= $this->session->userdata('telepon'); ?>" class="font-light validate black-text" disabled>
                                            <label for="telp">Telepon</label>
                                        </div>
                                		<div class="input-field font-light col s6">
                                            <input id="nama" type="text" name="nama" value="<?= $this->session->userdata('nama'); ?>" class="font-light validate black-text" disabled>
                                            <label for="nama">Nama Lengkap</label>
                                        </div>
                                        <div class="input-field font-light col s6">
                                            <input id="email" type="text" name="email" value="<?= $this->session->userdata('email'); ?>" class="font-light validate black-text" disabled>
                                            <label for="email">Email</label>
                                        </div>
                                    </div>

                                    <div class="col s6">
                                        <h6 class="font-bold">Fakultas</h6>
                                        <div class="col s6">
                                            <?php if($this->session->userdata('fakultas') == "Bisnis"){ ?>
                                                <input class="with-gap" id="fb" style="border:  2px solid yellow;" type="radio" name="fakultas" value="Fakultas Bisnis" checked disabled>
                                            <?php } else { ?>
                                                <input class="with-gap" id="fb" style="border:  2px solid yellow;" type="radio" name="fakultas" value="Fakultas Bisnis" disabled>
                                            <?php } ?>
                                            <label for="fb" class="black-text">Bisnis</label>
                                        </div>
                                        <div>
                                            <?php if($this->session->userdata('fakultas') == "Industri Kreatif"){ ?>
                                                <input class="with-gap" id="ik" type="radio" name="fakultas" value="Industri Kreatif" checked disabled>
                                            <?php } else { ?>
                                                <input class="with-gap" id="ik" type="radio" name="fakultas" value="Industri Kreatif" disabled>
                                            <?php } ?>
                                            <label for="ik" class="black-text">Industri Kreatif</label>
                                        </div>
                                    </div>

                                    <div class="col s5">
                                        <div class="input-field font-light">
                                            <select name="semester" id="semester">
                                                <?php foreach($jurusan as $jur) : 
                                                    if($this->session->userdata('kodeJurusan') == $jur['Kode_Jurusan']){
                                                ?>
                                                    <option value="<?= $jur['Kode_Jurusan']?>" selected disabled><?= $jur['Nama_Jurusan']; ?></option>
                                                <?php }
                                                    else {
                                                ?>
                                                    <option value="<?= $jur['Kode_Jurusan']?>" disabled><?= $jur['Nama_Jurusan']; ?></option>
                                                <?php } endforeach; ?>
                                            </select>
                                            <label class="font-light" for="semester">Program Studi</label>
                                            <input type="hidden" name="jurusan" value="<?= $this->session->userdata('kodeJurusan'); ?>">
                                        </div>
                                    </div>
                                    <div>
                                    <div class="col s12">
                                        <h6 class="font-bold">Judul</h6>
                                    </div>
                                        <div class="row">
                                            <div class="input-field col s12">
                                                <input id="ja" type="text" name="ja" class="validate font-light">
                                                <small class="form-text black-text text-danger pl-3"><?= form_error('ja') ?></small>
                                                <label for="ja">Bahasa Indonesia</label>
                                            </div>
                                            <div class="input-field col s12">
                                                <input id="jb" type="text" name="jb" class="validate font-light">
                                                 <?= form_error('jb', '<small class="form-text black-text text-danger pl-3">', '</small>');?>
                                                <label for="jb">Bahasa Inggris</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-action center" style="padding-top: 7%;">
                                    <button class="btn waves-effect waves-light" name="action" type="submit" style="">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        <?php }} ?>
        </div>
    </div>

</body>

