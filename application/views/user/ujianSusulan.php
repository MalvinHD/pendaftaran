<style type="text/css">
    .transbox {
        width: 150vh;
        height: 125vh;
        background-color: #ffffff;
        border-radius: 15px;
        background: white;
    }

    body {
        background-color: #203A43;
        color: black;
    }

    div.row {
        padding: 10px;
    }

    .card1 {
        display: block;
        position: relative;
        max-width: 262px;
        background-color: #f2f8f9;
        border-radius: 4px;
        padding: 32px 24px;
        margin: 12px;
        text-decoration: none;
        z-index: 0;
        overflow: hidden;
    }
</style>

<body>
    <div class="container" style="padding-top: 20px;">
        <div class="row">
            <div class="col-md-6">
                <div class="transbox">
                    <div class="row">
                        <form class="col s12"  method="post" action="<?= base_url(); ?>user/ujian" >
                          <h4 class="font-bold" style="padding-left: 30px;" data-aos="fade-down" data-aos-duration="1000">Form Pengajuan Ujian Susulan</h4>
                            <div class="col s12">
                                <div class="font-light col s6">
                                    <h6 class="col s12">Tahun Akademik</h6>
                                    <div class="input-field col s5" style="margin-top: 0;">
                                        <input id="tahunC" type="number" name="tahunC" class="validate" placeholder="Dari">
                                        <?= form_error('tahunC', '<small class="form-text black-text text-danger pl-3">', '</small>');?>
                                    </div>
                                    <div class="col s1" style="padding-top: 5%;">

                                    </div>
                                    <div class="input-field col s5" style="margin-top: 0;">
                                        <input id="tahunD" type="number" name="tahunD" class="validate" placeholder="Ke">
                                        <?= form_error('tahunD', '<small class="form-text black-text text-danger pl-3">', '</small>');?>
                                    </div>
                                </div>
                                <div class="col s6 font-light">
                                    <h6>Fakultas</h6>
                                    <div class="col s6" style="margin-bottom: 1cm">
                                        <?php if ($this->session->userdata('fakultas') == "Bisnis") { ?>
                                            <input class="with-gap" id="fb" style="border:  2px solid yellow; margin-top: 0.5cm;" type="radio" name="fakultas" value="Fakultas Bisnis" checked disabled>
                                        <?php } else { ?>
                                            <input class="with-gap" id="fb" style="border:  2px solid yellow;" type="radio" name="fakultas" value="Fakultas Bisnis" disabled>
                                        <?php } ?>
                                        <label for="fb" class="black-text">Bisnis</label>
                                    </div>
                                    <div>
                                        <?php if ($this->session->userdata('fakultas') == "Industri Kreatif") { ?>
                                            <input class="with-gap" id="ik" type="radio" name="fakultas" value="Industri Kreatif" checked disabled>
                                        <?php } else { ?>
                                            <input class="with-gap" id="ik" type="radio" name="fakultas" value="Industri Kreatif" disabled>
                                        <?php } ?>
                                        <label for="ik" class="black-text">Industri Kreatif</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field font-light col s6">
                                    <input id="nama" type="text" name="nama" value="<?= $this->session->userdata('nama'); ?>" class="font-light validate black-text" disabled>
                                    <label for="nama">Nama Lengkap</label>
                                </div>
                                <div class="input-field font-light col s6">
                                    <input id="nim" type="text" name="nim" value="<?= $this->session->userdata('nim'); ?>" class="font-light validate  black-text" disabled>
                                    <input type="hidden" name="nim" value="<?= $this->session->userdata('nim'); ?>">
                                    <label for="nim">NIM</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field font-light col s6">
                                    <input id="telp" type="text" name="telp" value="<?= $this->session->userdata('telepon'); ?>" class="font-light validate black-text" disabled>
                                    <label for="telp">Telepon</label>
                                </div>
                                <div class="col s5">
                                    <div class="input-field font-light">
                                        <select name="semestr" id="semestr">
                                            <?php foreach ($jurusan as $jur) :
                                                if ($this->session->userdata('kodeJurusan') == $jur['Kode_Jurusan']) {
                                            ?>
                                                    <option value="<?= $jur['Kode_Jurusan'] ?>" selected disabled><?= $jur['Nama_Jurusan']; ?></option>
                                                <?php } else {
                                                ?>
                                                    <option value="<?= $jur['Kode_Jurusan'] ?>" disabled><?= $jur['Nama_Jurusan']; ?></option>
                                            <?php }
                                            endforeach; ?>
                                        </select>
                                        <label class="font-light" for="semestr">Program Studi</label>
                                        <input type="hidden" name="jurusan" value="<?= $this->session->userdata('kodeJurusan'); ?>">
                                    </div>
                                </div>
                            </div>


                            <div class="row" style="margin-bottom: 2cm">
                                <div class="input-field font-light col s6">
                                    <label for="semester">Semester</label>
                                    <input id="semester" type="text" name="semester" value="Semester <?= $this->session->userdata('semester'); ?>" class="font-light validate black-text" disabled>
                                </div>
                                <div class="input-field col s6 font-light">
                                    <select name="matakuliah" id="matakuliah">
                                        <?php foreach ($matakuliah as $matkul) :?>
                                            <option value="<?= $matkul['Nama_Matkul'] ?>"><?= $matkul['Nama_Matkul']; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                    <label for="matakuliah">Nama Mata Kuliah</label>
                                </div>
                            </div>
                            
                            <div class="row">
                                <form class="col s12">
                                    <div class="row">
                                        <div class="input-field col s12 font-light">
                                            <input id="alasan" type="text" name="alasan">
                                            <?= form_error('alasan', '<small class="form-text black-text text-danger pl-3">', '</small>');?>
                                            <label for="textarea1">Alasan Tidak Mengikuti Ujian</label>
                                                <?php if ($this->session->flashdata('message')) : ?>
                                                  <div class="row">
                                                    <div class="col s6 center">
                                                      <?php endif; ?>
                                                    </div>
                                                  </div>
                                                 <?php echo $this->session->flashdata('message'); ?>
                                                 <button class="btn waves-effect waves-light" type="submit" style="margin: -20px -50px; top:45%; left:50%;">Submit</button>
                                        </div>
                                    </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>