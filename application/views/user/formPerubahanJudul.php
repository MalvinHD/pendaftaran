<div style="background-color: #203A43;">
    <div class="container">
        <div class="row">


        <?php if($cekskripsi['status'] == TRUE) {?>
            <div style="height: 60vh;">
                <h1 class="white-text font-bold">
                    Perubahan Judul Skripsi Sedang Diproses.
                </h1>
            </div>

        <?php } else {?>

            <?php if($skripsi['status'] == FALSE){ ?>
                <div style="height: 60vh;">
                    <h1 class="white-text font-bold">
                        Mohon Mengajukan Proposal Skripsi Terlebih Dahulu.
                    </h1>
                </div>                
            <?php } else { $skripsi = $skripsi['data'];?>
                <div class="card col s10 offset-s1 z-depth-12" style="margin-top: 10%; margin-bottom: 10%;   padding-top: 2%; border-radius: 20px;" data-aos="fade-up">
                    <div class="card-content">
                        
                        <h4 class="font-bold">Formulir Perubahan Judul Tugas Akhir</h4>
                    
                        <div class="row">
                            <form class="col s12" method="post" action="<?= base_url(); ?>user/gantiJudul">
                                <div class="row">

                                    <div class="col s12" style="padding-top: 3%;">
                                        <div class="col s6">
                                            <h6 class="col s12 font-bold">Tahun Akademik</h6>
                                            <div class="input-field col s5" style="margin-top: 0;">
                                                <input id="tahun1" type="number" name="tahun1" class="validate" placeholder="Dari">
                                                <?= form_error('tahun1', '<small class="form-text black-text text-danger pl-3">', '</small>');?>
                                            </div>
                                            <div class="col s1" style="padding-top: 5%;">
                                                /    
                                            </div>
                                            <div class="input-field col s5" style="margin-top: 0;">
                                                <input id="tahun2" type="number" name="tahun2" class="validate" placeholder="Ke">
                                                <?= form_error('tahun2', '<small class="form-text black-text text-danger pl-3">', '</small>');?>
                                            </div>
                                        </div>
                                        <div class="col s6">
                                            <h6 class="font-bold">Semester</h6>
                                            <?php if(($this->session->userdata('semester')%2) == 0){
                                                $jenis = "Genap";
                                            } else {
                                                $jenis = "Ganjil";  
                                            } ?>
                                            <div class="col s6">
                                                <?php if($jenis == "Ganjil"){?>
                                                    <input class="with-gap" id="ganjil" type="radio" name="semester" value="ganjil" checked disabled>
                                                <?php } else {?>
                                                    <input class="with-gap" id="ganjil" type="radio" name="semester" value="ganjil" disabled>
                                                <?php } ?>
                                                <label for="ganjil" class="font-light black-text">Ganjil</label>
                                            </div>
                                            <div class="col s6">
                                                <?php if($jenis == "Genap"){?>
                                                    <input class="with-gap" id="genap" type="radio" name="semester" value="genap" checked disabled>
                                                <?php } else {?>
                                                    <input class="with-gap" id="genap" type="radio" name="semester" value="genap" disabled>
                                                <?php } ?>  
                                                <label for="genap" class="font-light black-text">Genap</label>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="col s12" style="padding-top: 3%;">
                                        <div class="input-field col s6">
                                            <input id="nim" type="number" class="validate black-text font-light" value="<?= $this->session->userdata('nim'); ?>" disabled>
                                            <input type="hidden" name="nim" value="<?= $this->session->userdata('nim'); ?>">
                                            <label for="nim" class="font-light">NIM</label>
                                        </div>
                                        <div class="input-field col s6">
                                            <input id="nama" type="text" name="nama" class="validate black-text font-light" value="<?= $this->session->userdata('nama'); ?>" disabled>
                                            <label for="nama" class="font-light">Nama Lengkap</label>
                                        </div>
                                    </div>

                                    <div class="col s12" style="padding-top: 3%;">
                                        <div class="col s6">
                                            <h6 class="font-bold">Fakultas</h6>
                                            <div class="col s6" style="margin-top: 0;">
                                                <?php if($this->session->userdata('fakultas') == "Bisnis"){ ?>
                                                    <input class="with-gap" id="fb" type="radio" name="fakultas" value="Fakultas Bisnis" checked disabled>
                                                <?php } else { ?>
                                                    <input class="with-gap" id="fb" type="radio" name="fakultas" value="Fakultas Bisnis" disabled>
                                                <?php } ?>
                                                <label for="fb" class="font-light black-text">Bisnis</label>
                                            </div>
                                            <div class="col s6" style="margin-top: 0;">
                                                <?php if($this->session->userdata('fakultas') == "Industri Kreatif"){ ?>
                                                    <input class="with-gap" id="ik" type="radio" name="fakultas" value="Industri Kreatif" checked disabled>
                                                <?php } else { ?>
                                                    <input class="with-gap" id="ik" type="radio" name="fakultas" value="Industri Kreatif" disabled>
                                                <?php } ?>
                                                <label for="ik" class="font-light black-text">Industri Kreatif</label>
                                            </div>
                                        </div>

                                        <div class="col s6">
                                            <div class="input-field">
                                                <select name="semester" id="semester">
                                                    <?php foreach($jurusan as $jur) : 
                                                        if($this->session->userdata('kodeJurusan') == $jur['Kode_Jurusan']){
                                                    ?>
                                                        <option class="black-text font-light" value="<?= $jur['Kode_Jurusan']?>" selected disabled><?= $jur['Nama_Jurusan']; ?></option>
                                                    <?php }
                                                        else {
                                                    ?>
                                                        <option class="black-text font-light" value="<?= $jur['Kode_Jurusan']?>" disabled><?= $jur['Nama_Jurusan']; ?></option>
                                                    <?php } endforeach; ?>
                                                </select>
                                                <label for="semester" class="font-bold">Program Studi</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col s12" style="padding-top: 3%;">
                                        <div class="col s12">
                                            <h6 class="font-bold">Judul Awal</h6>
                                        </div>
                                        <div class="input-field col s12">
                                            <input id="ja1" type="text" class="validate black-text font-light" value="<?= $skripsi['Judul_BI'];?>" disabled>
                                            <input type="hidden" name="jl1" value="<?= $skripsi['Judul_BI']; ?>">
                                            <label for="ja1" class="font-light">Bahasa Indonesia</label>
                                        </div>
                                        <div class="input-field col s12">
                                            <input id="ja2" type="text" class="validate black-text font-light" value="<?= $skripsi['Judul_EN'];?>" disabled>
                                            <input type="hidden" name="jl2" value="<?= $skripsi['Judul_EN']; ?>">
                                            <label for="ja2" class="font-light">Bahasa Inggris</label>
                                        </div>
                                    </div>


                                    <div class="col s12" style="padding-top: 3%;">
                                        <div class="col s12">
                                            <h6 class="font-bold">Judul Baru</h6>
                                        </div>
                                        <div class="input-field col s12">
                                            <input id="jb1" type="text" name="jb1" class="validate font-light">
                                            <?= form_error('jb1', '<small class="form-text black-text text-danger pl-3">', '</small>');?>
                                            <label for="jb1" class="font-light">Bahasa Indonesia</label>
                                        </div>
                                        <div class="input-field col s12">
                                            <input id="jb2" type="text" name="jb2" class="validate font-light">
                                            <?= form_error('jb2', '<small class="form-text black-text text-danger pl-3">', '</small>');?>
                                            <label for="jb2" class="font-light">Bahasa Inggris</label>
                                        </div>
                                    </div>                                    
                                        

                                    <div class="col s12" style="padding-top: 3%; padding-bottom: 3%;">
                                        <h6 class="font-bold">Alasan</h6>
                                        <div class="col s6" style="margin-top: 0; margin-bottom: 5px;">
                                            <input class="with-gap" id="bimbingan" type="radio" name="alasan" value="Bimbingan">
                                            <label for="bimbingan" class="font-light black-text">Bimbingan</label>
                                        </div>
                                        <div class="col s6" style="margin-top: 0; margin-bottom: 5px;">
                                            <input class="with-gap" id="ks" type="radio" name="alasan" value="Keputusan Sidang">
                                            <label for="ks" class="font-light black-text">Keputusan Sidang</label>
                                        </div>
                                        <?= form_error('alasan', '<small class="form-text black-text text-danger pl-3">', '</small>');?>    
                                    </div>


                                </div>
                                <div class="card-action center" style="padding-top: 7%;">
                                    <input type="hidden" name="kode_skripsi" value="<?= $skripsi['Kode_Skripsi']; ?>">
                                     <button class="btn waves-effect waves-light" name="action" type="submit" >Submit</button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>

        <?php }} ?>
        </div>
    </div>
</div>