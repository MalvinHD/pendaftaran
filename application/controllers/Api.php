<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/RestController.php';
require APPPATH . 'libraries/Format.php';

use chriskacerguis\RestServer\RestController;

class Api extends RestController {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model('Api_Model');
        $this->load->helper('url');
        $this->load->helper('string');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->helper(['jwt', 'authorization']);

        $this->methods['mahasiswa_delete']['limit'] = 2;
        $this->methods['jadwalujian_delete']['limit'] = 2;
        $this->methods['skripsi_delete']['limit'] = 2;
        $this->methods['listskripsi_delete']['limit'] = 2;
    }



    public function loginadmin_post()
    {
        $kode = $this->post('kode');
        $password = $this->post('password');


        $user = $this->Api_Model->getAllData(15,$kode);

            if ($user) 
            {
                //Cek NIM nya
                if ($user['Kode_Admin'] == $kode) 
                {
                    //cek password
                    if (password_verify($password, $user['Password']))
                    {
                        $token = AUTHORIZATION::generateToken(['kode' => $user['Kode_Admin']]);
                        $data = [
                            'id' => $user['Id_Admin'],
                            'kode' => $user['Kode_Admin'],
                            'nama' => $user['Nama'],
                            'jabatan' => $user['Jabatan'],
                            'token' => $token,
                            'Status' => 2
                        ];

                        $this->response( [
                            'status' => true,
                            'data' => $data,
                            'message' => 'Login successfully'
                        ], 200 );
                    } 
                    else 
                    {
                        $this->response( [
                            'status' => false,
                            'message' => 'Wrong password'
                        ], 200 );
                    }
                }
            } 
            else 
            {
                $this->response( [
                    'status' => false,
                    'message' => 'User is not found'
                ], 200 );
            }

    }

    public function loginmahasiswa_post()
    {
        $nim = $this->post('nim');
        $password = $this->post('password');


        $user = $this->Api_Model->getAllData(16,$nim);

            if ($user) 
            {
                //Cek NIM nya
                if ($user['NIM'] == $nim) 
                {
                    //cek password
                    if (password_verify($password, $user['Password']))
                    {
                        $fakultas =  $this->Api_Model->getAllData(17,$user['Kode_Jurusan']); 
                        $program =  $this->Api_Model->getAllData(18,$user['Kode_Program']);
                        $token = AUTHORIZATION::generateToken(['nim' => $user['NIM']]);
                        $data = [
                            'id' => $user['Id_Mahasiswa'],
                            'nim' => $user['NIM'],
                            'password' => $user['Password'],
                            'nama' => $user['Nama'],
                            'email' => $user['Email'],
                            'telepon' => $user['Telepon'],
                            'kodeJurusan' => $user['Kode_Jurusan'],
                            'namaJurusan' => $fakultas['Nama_Jurusan'],
                            'fakultas' => $fakultas['Fakultas'],
                            'semester' => $user['Semester'],
                            'program' => $program['Program'],
                            'kelas' => $program['Kelas'],
                            'token' => $token,
                            'Status' => 1
                        ];

                        $this->response( [
                            'status' => true,
                            'data' => $data,
                            'message' => 'Login successfully'
                        ], 200 );
                    } 
                    else 
                    {
                        $this->response( [
                            'status' => false,
                            'message' => 'Wrong password'
                        ], 200 );
                    }
                }
            } 
            else 
            {
                $this->response( [
                    'status' => false,
                    'message' => 'User is not found'
                ], 200 );
            }
        
    }

    private function verify_request()
    {

        // Extract the token
        $token = $this->input->get_request_header("Authorization");

        // Use try-catch
        // JWT library throws exception if the token is not valid
        try {
            // Validate the token
            // Successfull validation will return the decoded user data else returns false
            $data = AUTHORIZATION::validateToken($token);
            if ($data === false) {
                $status = parent::HTTP_UNAUTHORIZED;
                $response = ['status' => $status, 'msg' => 'Unauthorized Access!'];
                $this->response($response, $status);
                exit();
            } else {
                return $data;
            }
        } catch (Exception $e) {
            // Token is invalid
            // Send the unathorized access message
            $status = parent::HTTP_UNAUTHORIZED;
            $response = ['status' => $status, 'data' => $token, 'msg' => 'Kampank '];
            $this->response($response, $status);
        }

    }

public function get_me_data_post()
{
    // Call the verification method and store the return value in the variable
    $data = $this->verify_request();
    // Send the return data as reponse
    $status = parent::HTTP_OK;
    $response = ['status' => $status, 'data' => $data];
    $this->response($response, $status);
}


//---------------------//
//     MAHASISWA       //
//---------------------//

    public function mahasiswa_get()
    {
        $verif = $this->verify_request();

        if($verif){
            $id = $this->get( 'id' );

            if ( $id === null )
            {         
                $users = $this->Api_Model->getAllData(1);
                if ( $users )
                {
                     $this->response( [
                        'status' => true,
                        'data' => $users,
                        'message' => 'Mahasiswa is found'
                    ], 200 );
                }
                else
                {
                    // Set the response and exit
                    $this->response( [
                        'status' => false,
                        'message' => 'No such user found'
                    ], 404 );
                }
            }

            else
            {
                $users = $this->Api_Model->getAllData(1,$id);
                if ( $users )
                {
                    $this->response( [
                        'status' => true,
                        'data' => $users,
                        'message' => 'Id Mahasiswa is found'
                    ], 200 );
                }
                else
                {
                    $this->response( [
                        'status' => false,
                        'message' => 'Id mahasiswa is not found'
                    ], 404 );
                }
            }
        }
    }


    public function mahasiswanim_get()
    {
        $verif = $this->verify_request();

        if($verif){
            $nim = $this->get( 'nim' );


                $users = $this->Api_Model->getAllData(16,$nim);
                if ( $users )
                {
                     $this->response( [
                        'status' => true,
                        'data' => $users,
                        'message' => 'Mahasiswa is found'
                    ], 200 );
                }
                else
                {
                    // Set the response and exit
                    $this->response( [
                        'status' => false,
                        'message' => 'No such user found'
                    ], 404 );
                }

        }
    }


    public function mahasiswapass_get()
    {
        $verif = $this->verify_request();

        if($verif){
            $id = $this->get( 'id' );


                $users = $this->Api_Model->getAllData(22,$id);
                if ( $users )
                {
                     $this->response( [
                        'status' => true,
                        'data' => $users,
                        'message' => 'Mahasiswa is found'
                    ], 200 );
                }
                else
                {
                    // Set the response and exit
                    $this->response( [
                        'status' => false,
                        'message' => 'No such user found'
                    ], 404 );
                }

        }
    }


    public function mahasiswa_delete()
    {
        $verif = $this->verify_request();

        if($verif){
            $id = $this->delete( 'id' );

            if ( $id === null )
            {         
                $this->response( [
                    'status' => false,
                    'message' => 'Please input the Id'
                ], 404 );
            }

            else
            {            
                if ( $this->Api_Model->delete(1,$id) > 0)
                {
                    $this->response( [
                        'status' => true,
                        'id' => $id,
                        'message' => 'Delete successfully'
                    ], 200 );
                }
                else
                {
                    $this->response( [
                        'status' => false,
                        'message' => 'ID mahasiswa is not found'
                    ], 404 );
                }
            }
        }
    }

    public function mahasiswa_post()
    {
        $verif = $this->verify_request();

        if($verif){
            $data = [
                "Password" => $this->post('password'),
                "NIM" => $this->post('nim'),
                "Nama" => $this->post('nama'),                 
                "Kode_Jurusan" => $this->post('jurusan'),
                "Semester" => $this->post('semester'),
                "Email" => $this->post('email'),
                "Telepon" => $this->post('telepon'),
                "Kode_Program" => $this->post('kode'),
            ];

            if ( $this->Api_Model->create(1,$data) > 0)
            {
                $this->response( [
                    'status' => true,
                    'data' => $data,
                    'message' => 'Mahasiswa is successfully created'
                ], 200 );
            }
            else
            {
                $this->response( [
                    'status' => false,
                    'message' => 'Create mahasiswa is failed'
                ], 404 );
            }
        }
    }

    public function mahasiswa_put()
    {
        $verif = $this->verify_request();

        if($verif){
            $id = $this->put('id');

            $data = [
                "NIM" => $this->put('nim'),
                "Nama" => $this->put('nama'),                 
                "Kode_Jurusan" => $this->put('jurusan'),
                "Semester" => $this->put('semester'),
                "Email" => $this->put('email'),
                "Telepon" => $this->put('telepon'),
                "Kode_Program" => $this->put('kode'),
            ];

            if ( $this->Api_Model->update(1,$data,$id) > 0)
            {
                $this->response( [
                    'status' => true,
                    'data' => $data,
                    'message' => 'Mahasiswa is successfully updated'
                ], 200 );
            }
            else
            {
                $this->response( [
                    'status' => false,
                    'message' => 'Update mahasiswa is failed'
                ], 404 );
            }
        }
    }






//-------------------------//
//      JADWAL UJIAN       //  
//-------------------------//    

    public function jadwalujian_get()
    {
        $verif = $this->verify_request();

        if($verif){
            $id = $this->get( 'id' );

            if ( $id === null )
            {         
                $users = $this->Api_Model->getAllData(2);
                if ( $users )
                {
                    $this->response( [
                        'status' => true,
                        'data' => $users,
                        'message' => 'Jadwal ujian is found'
                    ], 200 );
                }
                else
                {
                    // Set the response and exit
                    $this->response( [
                        'status' => false,
                        'message' => 'No such jadwal ujian found'
                    ], 200 );
                }
            }

            else
            {
                $users = $this->Api_Model->getAllData(2,$id);
                if ( $users )
                {
                    $this->response( [
                        'status' => true,
                        'data' => $users,
                        'message' => 'ID Jadwal ujian is found'
                    ], 200 );
                }
                else
                {
                    $this->response( [
                        'status' => false,
                        'message' => 'ID jadwal ujian is not found'
                    ], 404 );
                }
            }
        }
    }

    public function jadwalujiankode_get()
    {
        $verif = $this->verify_request();

        if($verif){
            $kode = $this->get( 'kode' );

            if ( $kode === null )
            {         
                $users = $this->Api_Model->getAllData(2);
                if ( $users )
                {
                    $this->response( [
                        'status' => true,
                        'data' => $users,
                        'message' => 'Jadwal ujian is found'
                    ], 200 );
                }
                else
                {
                    // Set the response and exit
                    $this->response( [
                        'status' => false,
                        'message' => 'No such jadwal ujian found'
                    ], 200 );
                }
            }

            else
            {
                $users = $this->Api_Model->getAllData(20,$kode);
                if ( $users )
                {
                    $this->response( [
                        'status' => true,
                        'data' => $users,
                        'message' => 'ID Jadwal ujian is found'
                    ], 200 );
                }
                else
                {
                    $this->response( [
                        'status' => false,
                        'message' => 'ID jadwal ujian is not found'
                    ], 404 );
                }
            }
        }
    }


    public function jadwalujian_delete()
    {
        $verif = $this->verify_request();

        if($verif){

            $id = $this->delete( 'id' );

            if ( $id === null )
            {         
                $this->response( [
                    'status' => false,
                    'message' => 'Please input the Id'
                ], 404 );
            }

            else
            {            
                if ( $this->Api_Model->delete(2,$id) > 0)
                {
                    $this->response( [
                        'status' => true,
                        'id' => $id,
                        'message' => 'Delete successfully'
                    ], 200 );
                }
                else
                {
                    $this->response( [
                        'status' => false,
                        'message' => 'ID jadwal ujian is not found'
                    ], 404 );
                }
            }
        }
    }
    
    public function jadwalujian_post()
    {
        $verif = $this->verify_request();

        if($verif){

            $data = [

                "Kode_Ujian" => $this->post('kode'),
                "Nama_Matkul" => $this->post('matkul'),
                "Kode_Jurusan" => $this->post('jurusan'),
                "Semester" => $this->post('semester'),
                "Tanggal" => $this->post('tanggal'),
                "Jam" => $this->post('jam'),
                "Ruang" => $this->post('ruang'),
            ];

            if ( $this->Api_Model->create(2,$data) > 0)
            {
                $this->response( [
                    'status' => true,
                    'data' => $data,
                    'message' => 'Jadwal Ujian is successfully created'
                ], 200 );
            }
            else
            {
                $this->response( [
                    'status' => false,
                    'message' => 'Create jadwal ujian is failed'
                ], 404 );
            }
        }
    }

    public function jadwalujian_put()
    {
        $verif = $this->verify_request();

        if($verif){

            $id = $this->put('id');
            $str = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $x = $this->put('jurusan').substr(str_shuffle($str),0,6);        
            $data = [
                "Kode_Ujian" => $x,
                "Nama_Matkul" => $this->put('matkul'),
                "Kode_Jurusan" => $this->put('jurusan'),
                "Semester" => $this->put('semester'),
                "Tanggal" => $this->put('tanggal'),
                "Jam" => $this->put('jam'),
                "Ruang" => $this->put('ruang')
            ];

            if ( $this->Api_Model->update(2,$data,$id) > 0)
            {
                $this->response( [
                    'status' => true,
                    'data' => $data,
                    'message' => 'Jadwal Ujian is successfully updated'
                ], 200 );
            }
            else
            {
                $this->response( [
                    'status' => false,
                    'message' => 'Update jadwal ujian is failed'
                ], 404 );
            }
        }
    }




//------------------//
//      SKRIPSI     //  
//------------------//

    public function skripsi_get()
    {
        $verif = $this->verify_request();

        if($verif){

            $id = $this->get( 'id' );
            $nim = $this->get( 'nim' );

            if ( $id === null )
            {   
                if($nim === null)      
                {
                    $users = $this->Api_Model->getAllData(3);
                    if ( $users )
                    {
                        $this->response( [
                            'status' => true,
                            'data' => $users,
                            'message' => 'Skripsi is found'
                        ], 200 );
                    }
                    else
                    {
                        // Set the response and exit
                        $this->response( [
                            'status' => false,
                            'message' => 'No such skripsi found'
                        ], 200 );
                    }
                }
                else
                {

                    $users = $this->Api_Model->getAllData(3,'',$nim);
                    if ( $users )
                    {
                        $this->response( [
                            'status' => true,
                            'data' => $users,
                            'message' => 'Skripsi is found'
                        ], 200 );
                    }
                    else
                    {
                        // Set the response and exit
                        $this->response( [
                            'status' => false,
                            'message' => 'No such skripsi found'
                        ], 200 );
                    }
                }
            }

            else
            {
                $users = $this->Api_Model->getAllData(3,$id);
                if ( $users )
                {
                    $this->response( [
                        'status' => true,
                        'data' => $users,
                        'message' => 'Id Skripsi is found'
                    ], 200 );
                }
                else
                {
                    $this->response( [
                        'status' => false,
                        'message' => 'Id daftar skripsi is not found'
                    ], 404 );
                }
            }
        }
    }

    public function skripsi_delete()
    {
        $verif = $this->verify_request();

        if($verif){

            $id = $this->delete( 'id' );

            if ( $id === null )
            {         
                $this->response( [
                    'status' => false,
                    'message' => 'Please input the Id'
                ], 404 );
            }

            else
            {            
                if ( $this->Api_Model->delete(3,$id) > 0)
                {
                    $this->response( [
                        'status' => true,
                        'id' => $id,
                        'message' => 'Delete successfully'
                    ], 200 );
                }
                else
                {
                    $this->response( [
                        'status' => false,
                        'message' => 'ID daftar skripsi is not found'
                    ], 404 );
                }
            }
        }
    }


    public function skripsi_post()
    {
        $verif = $this->verify_request();

        if($verif){

            $data = [
               "Kode_Skripsi" => $this->post('kode'), 
               "NIM" => $this->post('nim'), 
               "Judul_BI" => $this->post('judulbi'), 
               "Judul_EN" => $this->post('judulen')
            ];

            if ( $this->Api_Model->create(3,$data) > 0)
            {
                $this->response( [
                    'status' => true,
                    'data' => $data,
                    'message' => 'Skripsi is successfully created'
                ], 200 );
            }
            else
            {
                $this->response( [
                    'status' => false,
                    'message' => 'Create skripsi is failed'
                ], 404 );
            }
        }
    }

    public function skripsi_put()
    {
        $verif = $this->verify_request();

        if($verif){
            $kode = $this->put('kode');
            $data = [           
                "Judul_BI" => $this->put('judulbi'), 
                "Judul_EN" => $this->put('judulen')
            ];

            if ( $this->Api_Model->update(3,$data,$kode) > 0)
            {
                $this->response( [
                    'status' => true,
                    'data' => $data,
                    'message' => 'Skripsi is successfully updated'
                ], 200 );
            }
            else
            {
                $this->response( [
                    'status' => false,
                    'message' => 'Update skripsi is failed'
                ], 404 );
            }
        }
        
    }




//----------------------------------//
//      DAFTAR PROPOSAL SKRIPSI     //  
//----------------------------------//

    public function listskripsi_get()
    {
        $verif = $this->verify_request();

        if($verif){

            $id = $this->get( 'id' );
            $nim = $this->get( 'nim' );
            $status = $this->get( 'status' );

            if ( $id === null )
            {         
                if($nim === null){
                    $users = $this->Api_Model->getAllData(4);
                    if ( $users )
                    {
                        $this->response( [
                            'status' => true,
                            'data' => $users,
                            'message' => 'Daftar proposal is found'
                        ], 200 );
                    }
                    else
                    {
                        // Set the response and exit
                        $this->response( [
                            'status' => false, 
                            'message' => 'No such user found'
                        ], 200 );
                    }
                }
                else{
                    
                    $users = $this->Api_Model->getAllData(14,$nim,$status);
                    
                    
                    if ( $users )
                    {
                        $this->response( [
                            'status' => true,
                            'data' => $users,
                            'message' => 'Daftar proposal is found'
                        ], 200 );
                    }
                    else
                    {
                        // Set the response and exit
                        $this->response( [
                            'status' => false,
                            'message' => 'No such user found'
                        ], 200 );
                    }
                }
            }

            else
            {
                $users = $this->Api_Model->getAllData(4,$id);
                if ( $users )
                {
                    $this->response( [
                        'status' => true,
                        'data' => $users,
                        'message' => 'Daftar proposal is found'
                    ], 200 );
                }
                else
                {
                    $this->response( [
                        'status' => false,
                        'message' => 'ID daftar pengajuan skripsi is not found'
                    ], 200 );
                }
            }
        }
    }



    public function listskripsi_delete()
    {
        $verif = $this->verify_request();

        if($verif){

            $id = $this->delete( 'id' );

            if ( $id === null )
            {         
                $this->response( [
                    'status' => false,
                    'message' => 'Please input the Id'
                ], 404 );
            }

            else
            {            
                if ( $this->Api_Model->delete(4,$id) > 0)
                {
                    $this->response( [
                        'status' => true,
                        'id' => $id,
                        'message' => 'Delete successfully'
                    ], 200 );
                }
                else
                {
                    $this->response( [
                        'status' => false,
                        'message' => 'ID daftar pengajuan skripsi is not found'
                    ], 404 );
                }
            }
        }
    }

    public function listskripsi_post()
    {
        $verif = $this->verify_request();

        if($verif){

            date_default_timezone_set('Asia/Jakarta');
            $tanggal = date("Y-m-d");

            $data = [
                "Kode_Skripsi" => $this->post('jurusan').rand(1000, 9999), 
                "NIM" => $this->post('nim'), 
                "Judul_BI" => $this->post('ja'), 
                "Judul_EN" => $this->post('jb'),
                "Tanggal_Diajukan" => $tanggal,
                "Tanggal_Disetujui" => NULL,
                "Tahun_Di" => $this->post('tahunA'),
                "Tahun_Ke" => $this->post('tahunB'),
                "Status" => $this->post('status'),
            ];

            if ( $this->Api_Model->create(4,$data) > 0)
            {
                $this->response( [
                    'status' => true,
                    'data' => $data,
                    'message' => 'Daftar pengajuan proposal skripsi is successfully created'
                ], 200 );
            }
            else
            {
                $this->response( [
                    'status' => false,
                    'message' => 'Create daftar pengajuan proposal skripsi is failed'
                ], 404 );
            }
        }
    }

    public function listskripsi_put()
    {
        $verif = $this->verify_request();

        if($verif){

            $kode = $this->put('kode');

            $tanggal = $this->put('tanggal');
            $nim = $this->put('nim');

            if($tanggal){
                if($nim){

                    $data = [
                        "NIM" => $this->put('nim'),
                        "Judul_BI" => $this->put('ja'),
                        "Judul_EN" => $this->put('jb'),
                        "Tanggal_Diajukan" => $tanggal,
                        "Tanggal_Disetujui" => NULL,
                        "Tahun_Di" => $this->put('tahunA'),
                        "Tahun_Ke" => $this->put('tahunB'),
                        "Status" => $this->put('status'),
                    ];

                }
                else{
                    $data = [
                        "Status" => $this->put('status'), 
                        "Tanggal_Disetujui" => $tanggal
                    ];   
                } 
            }
            else{
                $data = [
                    "Status" => $this->put('status')
                ];

            }
            
            if ( $this->Api_Model->update(4,$data,$kode) > 0)
            {
                $this->response( [
                    'status' => true,
                    'data' => $data,
                    'message' => 'Daftar pengajuan proposal skripsi is successfully updated'
                ], 200 );
            }
            else
            {
                $this->response( [
                    'status' => false,
                    'message' => 'Update daftar pengajuan proposal skripsi is failed'
                ], 404 );
            }
        }
    }


//------------------//
//      ADMIN       //
//------------------//

    public function admin_get()
    {
        $verif = $this->verify_request();

        if($verif){

            $users = $this->Api_Model->getAllData(5);
            
            if ( $users )
            {
                // Set the response and exit
                $this->response( [
                    'status' => true,
                    'data' => $users,
                    'message' => 'admin is found'
                ], 200 );
            }
            
            else
            {
                // Set the response and exit
                $this->response( [
                    'status' => false,
                    'message' => 'No such user found'
                ], 404 );
            }
        }
    }

    public function adminforget_get()
    {
        $email = $this->get(' email ');
        $users = $this->Api_Model->getAllData(5,$email);
            
        if ( $users )
        {
            // Set the response and exit
            $this->response( [
                'status' => true,
                'data' => $users,
                'message' => 'admin is found'
            ], 200 );
        }
            
        else
        {
            // Set the response and exit
            $this->response( [
                'status' => false,
                'message' => 'No such user found'
            ], 200 );
        }
    }

    public function adminid_get()
    {
        $id = $this->get('id');
        
        $users = $this->Api_Model->getAllData(21,$id);
            
        if ( $users )
        {
            // Set the response and exit
            $this->response( [
                'status' => true,
                'data' => $users,
                'message' => 'admin is found'
            ], 200 );
        }
            
        else
        {
            // Set the response and exit
            $this->response( [
                'status' => false,
                'message' => 'No such user found'
            ], 200 );
        }
    }

    public function admin_delete()
    {
        $verif = $this->verify_request();

        if($verif){

            $id = $this->delete( 'id' );

            if ( $id === null )
            {         
                $this->response( [
                    'status' => false,
                    'message' => 'Please input the Id'
                ], 404 );
            }

            else
            {            
                if ( $this->Api_Model->delete(5,$id) > 0)
                {
                    $this->response( [
                        'status' => true,
                        'id' => $id,
                        'message' => 'Delete successfully'
                    ], 200 );
                }
                else
                {
                    $this->response( [
                        'status' => false,
                        'message' => 'ID admin is not found'
                    ], 404 );
                }
            }
        }
    }


    public function admin_post()
    {
        $verif = $this->verify_request();

        if($verif){

            $data = [            
                "Kode_Admin" => $this->post('kode'),
                "Nama" => $this->post('nama'),
                "Email" => $this->post('email'),
                "Jabatan" => $this->post('jabatan'),                 
                "Password" => password_hash($this->post('password'), PASSWORD_DEFAULT),
            ];

            if ( $this->Api_Model->create(5,$data) > 0)
            {
                $this->response( [
                    'status' => true,
                    'data' => $data,
                    'message' => 'Admin is successfully created'
                ], 200 );
            }
            else
            {
                $this->response( [
                    'status' => false,
                    'message' => 'Create admin is failed'
                ], 404 );
            }
        }
    }


    public function admin_put()
    {
        $verif = $this->verify_request();

        if($verif){

            $id = $this->put('id');

            $data = [
                "Kode_Admin" => $this->put('kode'),
                "Nama" => $this->put('nama'),                 
                "Email" => $this->put('email'),                 
                "Jabatan" => $this->put('jabatan'),                 
            ];

            if ( $this->Api_Model->update(5,$data,$id) > 0)
            {
                $this->response( [
                    'status' => true,
                    'data' => $data,
                    'message' => 'Admin is successfully updated'
                ], 200 );
            }
            else
            {
                $this->response( [
                    'status' => false,
                    'message' => 'Update admin is failed'
                ], 404 );
            }
        }
    }


//------------------//
//      MATKUL      //
//------------------//

    public function matkul_get()
    {
        $verif = $this->verify_request();

        if($verif){

            $kode = $this->get('jurusan');
            $semester = $this->get('semester');
            
            if($kode && $semester){
                $users = $this->Api_Model->getAllData(12,$kode,$semester);
                
                if ( $users )
                {
                    // Set the response and exit
                    $this->response( [
                        'status' => true,
                        'data' => $users,
                        'message' => 'matkul '.$kode.' in semester '.$semester.' is found'
                    ], 200 );
                }
                
                else
                {
                    // Set the response and exit
                    $this->response( [
                        'status' => false,
                        'message' => 'No such matkul found'
                    ], 404 );
                }
            }

            else{
                if($semester){
                    $users = $this->Api_Model->getAllData(12,'',$semester);            
                    if ( $users )
                    {
                        // Set the response and exit
                        $this->response( [
                            'status' => true,
                            'data' => $users,
                            'message' => 'matkul in semester '.$semester.' is found'
                        ], 200 );
                    }
                    
                    else
                    {
                        // Set the response and exit
                        $this->response( [
                            'status' => false,
                            'message' => 'No such matkul found'
                        ], 404 );
                    }

                }
                
                elseif($kode){
                    $users = $this->Api_Model->getAllData(12,$kode,'');            
                    if ( $users )
                    {
                        // Set the response and exit
                        $this->response( [
                            'status' => true,
                            'data' => $users,
                            'message' => 'matkul '.$kode.' is found'
                        ], 200 );
                    }
                    
                    else
                    {
                        // Set the response and exit
                        $this->response( [
                            'status' => false,
                            'message' => 'No such matkul found'
                        ], 404 );
                    }

                }
                

                else{
                    $users = $this->Api_Model->getAllData(6);
                    
                    if ( $users )
                    {
                        // Set the response and exit
                        $this->response( [
                            'status' => true,
                            'data' => $users,
                            'message' => 'matkul is found'
                        ], 200 );
                    }
                    
                    else
                    {
                        // Set the response and exit
                        $this->response( [
                            'status' => false,
                            'message' => 'No such matkul found'
                        ], 404 );
                    }
                }
            }
        }
    }


    public function matkul_delete()
    {
        $verif = $this->verify_request();

        if($verif){

            $id = $this->delete( 'id' );

            if ( $id === null )
            {         
                $this->response( [
                    'status' => false,
                    'message' => 'Please input the Id'
                ], 404 );
            }

            else
            {            
                if ( $this->Api_Model->delete(6,$id) > 0)
                {
                    $this->response( [
                        'status' => true,
                        'id' => $id,
                        'message' => 'Delete successfully'
                    ], 200 );
                }
                else
                {
                    $this->response( [
                        'status' => false,
                        'message' => 'ID matkul is not found'
                    ], 404 );
                }
            }
        }
    }



    public function matkul_post()
    {
        $verif = $this->verify_request();

        if($verif){

            $data = [            
                "Nama_Matkul" => $this->post('nama'),
                "Semester" => $this->post('semester'),
                "Kode_Jurusan" => $this->post('jurusan')
            ];

            if ( $this->Api_Model->create(6,$data) > 0)
            {
                $this->response( [
                    'status' => true,
                    'data' => $data,
                    'message' => 'Matkul is successfully created'
                ], 200 );
            }
            else
            {
                $this->response( [
                    'status' => false,
                    'message' => 'Create matkul is failed'
                ], 404 );
            }
        }
    }


    public function matkul_put()
    {
        $verif = $this->verify_request();

        if($verif){

            $id = $this->put('id');

            $data = [
                "Nama_Matkul" => $this->put('nama'),
                "Semester" => $this->put('semester'),            
            ];

            if ( $this->Api_Model->update(6,$data,$id) > 0)
            {
                $this->response( [
                    'status' => true,
                    'data' => $data,
                    'message' => 'Matkul is successfully updated'
                ], 200 );
            }
            else
            {
                $this->response( [
                    'status' => false,
                    'message' => 'Update matkul is failed'
                ], 404 );
            }
        }
    }



//------------------//
//      JURUSAN     //
//------------------//

    public function jurusan_get()
    {
        $verif = $this->verify_request();

        if($verif){

            $fakultas = $this->get('fakultas');
            if($fakultas === NULL){

                $users = $this->Api_Model->getAllData(7);            
                if ( $users )
                {
                    // Set the response and exit
                    $this->response( [
                        'status' => true,
                        'data' => $users,
                        'message' => 'Jurusan is found'
                    ], 200 );
                }
                
                else
                {
                    // Set the response and exit
                    $this->response( [
                        'status' => false,
                        'message' => 'No such jurusan found'
                    ], 404 );
                }

            } else {

                $users = $this->Api_Model->getAllData(7,$fakultas);            
                if ( $users )
                {
                    // Set the response and exit
                    $this->response( [
                        'status' => true,
                        'data' => $users,
                        'message' => 'Jurusan is found'
                    ], 200 );
                }
                
                else
                {
                    // Set the response and exit
                    $this->response( [
                        'status' => false,
                        'message' => 'No such jurusan found'
                    ], 404 );
                }
            }
        }
    }

//------------------//
//      PROGRAM     //
//------------------//

    public function program_get()
    {
        $verif = $this->verify_request();

        if($verif){

            $program = $this->get('program');
            $kelas = $this->get('kelas');
            if($program === NULL && $kelas === NULL){

                $users = $this->Api_Model->getAllData(8);
                if ( $users )
                {
                    // Set the response and exit
                    $this->response( [
                        'status' => true,
                        'data' => $users,
                        'message' => 'program is found'
                    ], 200 );
                }
                
                else
                {
                    // Set the response and exit
                    $this->response( [
                        'status' => false,
                        'message' => 'No such program found'
                    ], 404 );
                }
            }
            else{
                $users = $this->Api_Model->getAllData(8,$program,$kelas);
                if ( $users )
                {
                    // Set the response and exit
                    $this->response( [
                        'status' => true,
                        'data' => $users,
                        'message' => 'program is found'
                    ], 200 );
                }
                
                else
                {
                    // Set the response and exit
                    $this->response( [
                        'status' => false,
                        'message' => 'No such program found'
                    ], 404 );
                }
            }
        }
    }


//------------------------------//
//      DAFTAR UBAH JUDUL       //
//------------------------------//

    public function daftarubahjudul_get()
    {
        $verif = $this->verify_request();

        if($verif){

            $kode = $this->get('kode');
            $nim = $this->get('nim');
            $status = $this->get('status');
            
            if($kode === NULL){
                if($nim === NULL)
                {
                    $users = $this->Api_Model->getAllData(9);
                    
                    if ( $users )
                    {
                        // Set the response and exit
                        $this->response( [
                            'status' => true,
                            'data' => $users,
                            'message' => 'Daftar ubah judul is found'
                        ], 200 );
                    }
                    
                    else
                    {
                        // Set the response and exit
                        $this->response( [
                            'status' => false,
                            'message' => 'No such daftar ubah judul found'
                        ], 200 );
                    }   
                }
                else
                {
                    $users = $this->Api_Model->getAllData(13,$nim,$status);
                    
                    if ( $users )
                    {
                        // Set the response and exit
                        $this->response( [
                            'status' => true,
                            'data' => $users,
                            'message' => 'Daftar ubah judul is found'
                        ], 200 );
                    }
                    
                    else
                    {
                        // Set the response and exit
                        $this->response( [
                            'status' => false,
                            'message' => 'No such daftar ubah judul found'
                        ], 200 );
                    }   
                }
            }
            else{

                $users = $this->Api_Model->getAllData(9,$kode);
                
                if ( $users )
                {
                    // Set the response and exit
                    $this->response( [
                        'status' => true,
                        'data' => $users,
                        'message' => 'Daftar ubah judul is found'
                    ], 200 );
                }
                
                else
                {
                    // Set the response and exit
                    $this->response( [
                        'status' => false,
                        'message' => 'No such daftar ubah judul found'
                    ], 200 );
                }
            }
        }
    }

    public function daftarubahjudul_post()
    {
        $verif = $this->verify_request();

        if($verif){
            
            $data = [
                "Kode_Skripsi" => $this->post('kode'),
                "NIM" => $this->post('nim'),
                "Judul_BIlama" => $this->post('jl1'),
                "Judul_ENlama" => $this->post('jl2'),
                "Judul_BIbaru" => $this->post('jb1'),
                "Judul_ENbaru" => $this->post('jb2'),
                "Alasan" => $this->post('alasan'),
                "Tanggal_Diajukan" => $this->post('tanggal'),
                "Tanggal_Disetujui" => NULL,
                "Tahun_Di" => $this->post('tahun1'),
                "Tahun_Ke" => $this->post('tahun2'),
                "Status" => $this->post('status'),
            ];



            if ( $this->Api_Model->create(8,$data) )
            {
                $this->response( [
                    'status' => true,
                    'data' => $data,
                    'message' => 'Daftar ubah judul is successfully updated'
                ], 200 );
            }
            else
            {
                $this->response( [
                    'status' => false,
                    'message' => 'Daftar ubah judul is failed'
                ], 404 );
            }
        }
    }


    public function daftarubahjudul_put()
    {
        $verif = $this->verify_request();

        if($verif){

            $kode = $this->put('kode');
            $tanggal = $this->put('tanggal');
            $nim = $this->put('nim');

            if($tanggal){
                if($nim){
                    $data = [
                        "NIM" => $this->put('nim'),
                        "Judul_BIlama" => $this->put('jl1'),
                        "Judul_ENlama" => $this->put('jl2'),
                        "Judul_BIbaru" => $this->put('jb1'),
                        "Judul_ENbaru" => $this->put('jb2'),
                        "Alasan" => $this->put('alasan'),
                        "Tanggal_Diajukan" => $tanggal,
                        "Tanggal_Disetujui" => NULL,
                        "Tahun_Di" => $this->put('tahun1'),
                        "Tahun_Ke" => $this->put('tahun2'),
                        "Status" => $this->put('status')
                    ];

                }
                else{
                    $data = [
                        "Status" => $this->put('status'), 
                        "Tanggal_Disetujui" => $this->put('tanggal')
                    ];
                }
            }
            
            else{
                $data = [
                    "Status" => $this->put('status')
                ];
            }

            if ( $this->Api_Model->update(7,$data,$kode) > 0)
            {
                $this->response( [
                    'status' => true,
                    'data' => $data,
                    'message' => 'Daftar ubah judul is successfully updated'
                ], 200 );
            }
            else
            {
                $this->response( [
                    'status' => false,
                    'message' => 'Daftar ubah judul is failed'
                ], 404 );
            }
        }
    }



//------------------------------------------//
//     DAFTAR PENGAJUAN UJIAN SUSULAN       //
//------------------------------------------//

    public function ujiansusulan_get()
    {
        $verif = $this->verify_request();

        if($verif){

            $nim = $this->get('nim');
            if($nim===NULL)
            {
                $users = $this->Api_Model->getAllData(10);
            
                if ( $users )
                {
                    // Set the response and exit
                    $this->response( [
                        'status' => true,
                        'data' => $users,
                        'message' => 'Daftar pengajuan ujian susulan is found'
                    ], 200 );
                }
                
                else
                {
                    // Set the response and exit
                    $this->response( [
                        'status' => false,
                        'message' => 'No such daftar pengajuan ujian susulan found'
                    ], 200 );
                }
            }
            else
            {
                $users = $this->Api_Model->getAllData(10,$nim);
            
                if ( $users )
                {
                    // Set the response and exit
                    $this->response( [
                        'status' => true,
                        'data' => $users,
                        'message' => 'Daftar pengajuan ujian susulan is found'
                    ], 200 );
                }
                
                else
                {
                    // Set the response and exit
                    $this->response( [
                        'status' => false,
                        'message' => 'No such daftar pengajuan ujian susulan found'
                    ], 200 );
                }

            }
        }
    }

    public function ujiansusulan_post()
    {
        $verif = $this->verify_request();

        if($verif){

            $data = [
                "Kode_Form" => $this->post('kode'),
                "NIM" => $this->post('nim'),
                "Nama_Matkul" => $this->post('matakuliah'),
                "Tanggal_Diajukan" => $this->post('tanggal'),
                "Tanggal_Disetujui" => NULL,
                "Tahun_Di" => $this->post('tahunC'),
                "Tahun_Ke" => $this->post('tahunD'),
                "Alasan" => $this->post('alasan'),
                "Status" => $this->post('status'),
                "Bayar" => $this->post('bayar'),
            ];

            if ( $this->Api_Model->create(9,$data) > 0)
            {
                $this->response( [
                    'status' => true,
                    'data' => $data,
                    'message' => 'Ujian susulan is successfully created'
                ], 200 );
            }
            else
            {
                $this->response( [
                    'status' => false,
                    'message' => 'Create ujian susulan is failed'
                ], 404 );
            }
        }
    }


    public function ujiansusulan_put()
    {
        $verif = $this->verify_request();

        if($verif){

            $kode = $this->put('kode');
            $tanggal = $this->put('tanggal');

            if($tanggal){
                $data = [
                    "Status" => $this->put('status'), 
                    "Tanggal_Disetujui" => $this->put('tanggal')
                ];
            }
            else{
                $data = [
                    "Status" => $this->put('status')
                ];
            }

            if ( $this->Api_Model->update(8,$data,$kode) > 0)
            {
                $this->response( [
                    'status' => true,
                    'data' => $data,
                    'message' => 'Daftar ujian susulan is successfully updated'
                ], 200 );
            }
            else
            {
                $this->response( [
                    'status' => false,
                    'message' => 'Daftar ujian susulan is failed'
                ], 404 );
            }
        }
    }

//========================================================//
//            LIST MURID YANG AKAN UJIAN SUSULAN          //
//========================================================//

    public function jadwalsusulan_get()
    {
        $verif = $this->verify_request();

        if($verif){

            $users = $this->Api_Model->getAllData(11);
            
            if ( $users )
            {
                // Set the response and exit
                $this->response( [
                    'status' => true,
                    'data' => $users,
                    'message' => 'Daftar murid yang ujian susulan is found'
                ], 200 );
            }
            
            else
            {
                // Set the response and exit
                $this->response( [
                    'status' => false,
                    'message' => 'No such daftar murid yang ujian susulan found'
                ], 200 );
            }
        }
    }    

    public function jadwalsusulan_post()
    {
        $verif = $this->verify_request();

        if($verif){

            $data = [            
                "Kode_Ujian" => $this->post('kodeujian'),
                "NIM" => $this->post('nim'),
                "Nama_Matkul" => $this->post('namamatkul')
            ];

            if ( $this->Api_Model->create(7,$data) > 0)
            {
                $this->response( [
                    'status' => true,
                    'data' => $data,
                    'message' => 'Jadwal susulan is successfully created'
                ], 200 );
            }
            else
            {
                $this->response( [
                    'status' => false,
                    'message' => 'Create jadwal susulan is failed'
                ], 404 );
            }
        }
    }


//--------------------------//
//      CHANGE PASS         //
//--------------------------//

    public function chgpass_put()
    {
        $verif = $this->verify_request();

        if($verif){

            $id = $this->put('id');

            $data = ["Password" => $this->put('password')];

            if ( $this->Api_Model->update(1,$data,$id) > 0)
            {
                $this->response( [
                    'status' => true,
                    'data' => $data,
                    'message' => 'Mahasiswa is successfully updated'
                ], 200 );
            }
            else
            {
                $this->response( [
                    'status' => false,
                    'message' => 'Update mahasiswa is failed'
                ], 404 );
            }
        }
    }

//------------------------------//
//      FORGOT PASSWORD         //
//------------------------------//

    
    public function status_get()
    {  


        $status = $this->get('status');

        $users = $this->Api_Model->getAllData(19,$status);
            
            if ( $users )
            {
                // Set the response and exit
                $this->response( [
                    'status' => true,
                    'data' => $users,
                    'message' => 'admin is found'
                ], 200 );
            }
            
            else
            {
                // Set the response and exit
                $this->response( [
                    'status' => false,
                    'message' => 'No such user found'
                ], 404 );
            }


    }

    public function status_put()
    {  

        $id = $this->put('id');

        $data = [
            "Status" => $this->put('status')
        ];

        if ( $this->Api_Model->update(5,$data,$id) > 0)
        {
            $this->response( [
                'status' => true,
                'data' => $data,
                'message' => 'Admin status is successfully updated'
            ], 200 );
        }
        else
        {
            $this->response( [
                'status' => false,
                'message' => 'Update admin status is failed'
            ], 404 );
        }
    }


    public function chgpw_put()
    {  

        $id = $this->put('id');

        $data = [
            'Password' => $this->put('password'),
            "Status" => $this->put('status')
        ];

        if ( $this->Api_Model->update(5,$data,$id) > 0)
        {
            $this->response( [
                'status' => true,
                'data' => $data,
                'message' => 'Admin status is successfully updated'
            ], 200 );
        }
        else
        {
            $this->response( [
                'status' => false,
                'message' => 'Update admin status is failed'
            ], 404 );
        }
    }

//--------------------------//
//      BAYAR UJIAN         //
//--------------------------//

    public function bayarujian_put()
    {
        $kode = $this->put('kode');

        $data = [
            "Bayar" => $this->put('bayar')
        ];

        if ( $this->Api_Model->update(8,$data,$kode) > 0)
        {
            $this->response( [
                'status' => true,
                'data' => $data,
                'message' => 'Status pembayaran is successfully updated'
            ], 200 );
        }
        else
        {
            $this->response( [
                'status' => false,
                'message' => 'Update status pembayaran is failed'
            ], 200 );
        }
    }
}