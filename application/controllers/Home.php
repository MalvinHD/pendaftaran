<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->model('User_Model');
        $this->load->helper('url');
        $this->load->helper('string');
        $this->load->library('form_validation');
        $this->load->library('session');
        
    }

	public function index()
	{  
		$this->form_validation->set_rules('nim', 'NIM', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');

        if ($this->form_validation->run() == FALSE) 
        {
            if ($this->session->userdata('Status')==1) 
            {
                redirect('user/');
            }
            else if ($this->session->userdata('Status')==2) 
            {
                redirect('admin');
            }
			$data["judul"] = "Login";
			$this->load->view('templates/header', $data);
			$this->load->view('index');
			$this->load->view('templates/footer');
		}

		else 
		{
            $this->_login();
        }
	}

	private function _login()
    {

        $cek = $this->User_Model->loginmahasiswa($this->input->post('nim'),$this->input->post('password'));

        if($cek['status']){
            $this->session->set_userdata($cek['data']);
            redirect('user/index');
        }
        else{

            $this->session->set_flashdata('message', '<div class="center" style="color:red; font-weight:bold;"> '.$cek ['message'].'</div>');
            redirect('home');
        }
    }

    public function logout()
    {
        if($this->session->userdata('Status') == 1)
        {
            $this->session->sess_destroy();
            redirect(base_url());    
        }
        else
        {
            $this->session->set_flashdata('message', '<div class="center" style="color:red; font-weight:bold;">Login Terlebih dahulu</div>');
            redirect('Home');       
        }
        
    }
}
    