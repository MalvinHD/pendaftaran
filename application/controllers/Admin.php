<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->model('Admin_Model');
        $this->load->helper('url');
        $this->load->helper('string');
        $this->load->library('phpmailer_lib');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('phpmailer_lib');
        
    }

	public function index()
	{
		$this->form_validation->set_rules('kodeadmin', 'KodeAdmin', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');

        if ($this->form_validation->run() == FALSE) 
        {
            if ($this->session->userdata('Status')==2) {
                redirect('admin/home');
            }
            else if($this->session->userdata('Status')==1){
                redirect('home');
            }
			$data["judul"] = "Login Admin";
			$this->load->view('templates/headeradmin', $data);
			$this->load->view('admin/index');
			$this->load->view('templates/footeradmin');
		}

		else 
		{
            $this->_login();
        }
	}

	private function _login()
    {
        
        $cek = $this->Admin_Model->loginadmin($this->input->post('kodeadmin'),$this->input->post('password'));

        if($cek['status']){
            $this->session->set_userdata($cek['data']);
            redirect('admin/home');
        }
        else{

            $this->session->set_flashdata('message', '<div class="alert" style="color: red;"> '.$cek['message']);
            redirect('admin');
        }

    }

    public function home()
    {
        if ($this->session->userdata('Status')==2)
        {
            $data["judul"] = "Selamat datang";
            $data["daftarProposal"] = $this->Admin_Model->getAllData(6);
            $data["perubahanJudul"] = $this->Admin_Model->getAllData(7);
            $data["ujianSusulan"] = $this->Admin_Model->getAllData(11);
            $data["jadwalujian"] = $this->Admin_Model->getAllData(13);
            $this->load->view('templates/headeradmin1', $data);
            $this->load->view('templates/navbaradmin');
            $this->load->view('admin/home');
            $this->load->view('templates/footeradmin1');
        }
        else{
            redirect('admin');
        }   
    }

    public function dataUjianSusul()
    {
        if ($this->session->userdata('Status')==2)
        {
            $data["judul"] = "Jadwal Ujian Susulan";
            $data["ujiansusulan"] = $this->Admin_Model->getAllData(12);
            $this->load->view('templates/headeradmin1', $data);
            $this->load->view('templates/navbaradmin');
            $this->load->view('admin/ujianSusulan');
            $this->load->view('templates/footeradmin1');
        }
        else{
            redirect('admin');
        }   
    }

    public function dataJadwalUjian()
    {
        if ($this->session->userdata('Status')==2)
        {
            $data["judul"] = "Jadwal Ujian Susulan";
            $data["jadwal"] = $this->Admin_Model->getAllData(13);
            $data["jurusan"] = $this->Admin_Model->getAllData(5);
             $data["matkul"] = $this->Admin_Model->getAllData(1);
            $this->load->view('templates/headeradmin1', $data);
            $this->load->view('templates/navbaradmin');
            $this->load->view('admin/data/jadwalUjian', $data);
            $this->load->view('templates/footeradmin1');
        }
        else{
            redirect('admin');
        }   
    }

    public function dataMatkul()
    {
        if ($this->session->userdata('Status')==2)
        {
            $data["judul"] = "Mata Kuliah";
            $data["matkul"] = $this->Admin_Model->getAllData(1);
            $data["jurusan"] = $this->Admin_Model->getAllData(5);
            $data["bisnis"] = $this->Admin_Model->getAllData(5,'Bisnis');
            $data["industri"] = $this->Admin_Model->getAllData(5,'Industri Kreatif');
            $this->load->view('templates/headeradmin1', $data);
            $this->load->view('templates/navbaradmin');
            $this->load->view('admin/data/mataKuliah', $data);
            $this->load->view('templates/footeradmin1');
        }
        else{
            redirect('admin');
        }   
    }

    public function dataMahasiswa()
    {
        if ($this->session->userdata('Status')==2)
        {
            $data["judul"] = "Data Mahasiswa";
            $data["mahasiswa"] = $this->Admin_Model->getAllData(3);
            $data["jurusan"] = $this->Admin_Model->getAllData(5);
            $this->load->view('templates/headeradmin1', $data);
            $this->load->view('templates/navbaradmin');
            $this->load->view('admin/data/dataMahasiswa', $data);
            $this->load->view('templates/footeradmin1');
        }
        else{
            redirect('admin');
        }   
    }

    public function dataAdmin()
    {
        if ($this->session->userdata('Status')==2)
        {
            $data["judul"] = "Data Admin";
            $data["admin"] = $this->Admin_Model->getAllData(2);
            $this->load->view('templates/headeradmin1', $data);
            $this->load->view('templates/navbaradmin');
            $this->load->view('admin/data/daftarAdmin', $data);
            $this->load->view('templates/footeradmin1');
        }
        else{
            redirect('admin');
        }   
    }

    public function dataSkripsi()
    {
        if ($this->session->userdata('Status')==2)
        {
            $data["judul"] = "Data Skripsi";
            $data["skripsi"] = $this->Admin_Model->getAllData(8);
            $this->load->view('templates/headeradmin1', $data);
            $this->load->view('templates/navbaradmin');
            $this->load->view('admin/skripsi', $data);
            $this->load->view('templates/footeradmin1');
        }
        else{
            redirect('admin');
        }   
    }

    public function tambahMahasiswa()
    {
        $cek = $this->db->get_where('mahasiswa', ['NIM' => $this->input->post('nim')])->row_array();
        if ($cek) 
        {
            redirect('admin/dataMahasiswa');
        }
        else
        {
            $kode = $this->Admin_Model->getAllData(4,$this->input->post('program'),$this->input->post('kelas'));
            $this->Admin_Model->tambahMahasiswa($kode['Kode']);
            redirect('admin/dataMahasiswa');
        }
    }

    public function editMahasiswa()
    {
        if(0 < $this->input->post('semester') && $this->input->post('semester') < 9)
        {
            $kode = $this->Admin_Model->getAllData(4,$this->input->post('program'),$this->input->post('kelas'));
            $this->Admin_Model->editMahasiswa($kode['Kode']);
            redirect('admin/dataMahasiswa');
        }
        else{
            redirect('admin/dataMahasiswa');
        }
    }

    public function tambahAdmin()
    {
        $cek = $this->db->get_where('admin', ['Kode_Admin' => $this->input->post('kode')])->row_array();
        if ($cek) 
        {
            redirect('admin/dataAdmin');
        }
        else
        {
            if($this->input->post('password') == $this->input->post('password1')) 
            {                
                $this->Admin_Model->tambahAdmin();
                redirect('admin/dataAdmin');
            }
            else
            {
                redirect('admin/dataAdmin');
            }
        }
    }

    public function editAdmin()
    {
        $this->Admin_Model->editAdmin();
        redirect('admin/dataAdmin');    
    }

    public function tambahMatkul()
    {
        $cek = $this->db->get_where('matkul', ['Nama_Matkul' => $this->input->post('nama')])->row_array();
        if ($cek) 
        {
            redirect('admin/dataMatkul');
        }
        else
        {
            $this->Admin_Model->tambahMatkul();
            redirect('admin/dataMatkul');
        }
    }

    public function editMatkul($id)
    {
        $cek = $this->db->get_where('matkul', ['Nama_Matkul' => $this->input->post('nama')])->row_array();
        if ($cek) 
        {
            redirect('admin/dataMatkul');
        }
        else
        {
            $this->Admin_Model->editMatkul($id);
            redirect('admin/dataMatkul');    
        }           
    }

    public function tambahJadwal()
    {
        $cek = $this->db->get_where('jadwalujian', ['Tanggal' => $this->input->post('tanggal'), 'Jam' => $this->input->post('jam'), 'Ruang' => $this->input->post('ruang')])->row_array();
        if ($cek) 
        {
            redirect('admin/dataJadwalUjian');
        }
        else
        {
            for ($i=0; $i < 10; $i++) { 
                $str = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $shuffle = substr(str_shuffle($str),0,6);
                $kode = $this->input->post('jurusan').$shuffle;
                $ncek = $this->db->get_where('jadwalujian', ['Kode_Ujian' => $kode])->row_array();
                if ($ncek = FALSE) 
                {
                    break;
                }
            }
            $this->Admin_Model->tambahJadwal($kode);
            redirect('admin/dataJadwalUjian');
        }
    }    

    public function updateUjian($x,$y)
    {
        
            if ($x == 1) 
            {       

                $this->form_validation->set_rules('kodeujiansusulan', 'Kode Ujian Susulan', 'required');

                if ($this->form_validation->run() == false) 
                {
                   redirect('admin/home');
                }
                else{

                    $dataujian = $this->Admin_Model->getAllData(17,$this->input->post('kodeujiansusulan'));
                    $datanya = $dataujian['data'];
                    $matkul = $datanya['Nama_Matkul'];
                    $tanggal = $datanya['Tanggal'];
                    $jam = $datanya['Jam'];
                    $ruang = $datanya['Ruang'];

                    $mahasiswa = $this->Admin_Model->getAllData(18,$this->input->post('nim'));
                    $dataMahasiswa = $mahasiswa['data'];
                    $email = $dataMahasiswa['Email'];
                 
                    
                    $mail = $this->phpmailer_lib->load();
                    //SMTP configuration
                    $mail->isSMTP();
                    $mail->Host = 'smtp.gmail.com';
                    $mail->SMTPAuth = true;
                    $mail->Username = 'layanankalbis@gmail.com';
                    $mail->Password = 'kalbis1234';
                    $mail->SMTPSecure = 'tls';
                    $mail->Port = 587;

                    $mail->setFrom('layanankalbis@gmail.com', 'Layanan Mahasiswa Kalbis Institute');
                    $mail->addReplyTo('layanankalbis@gmail.com', 'Layanan Mahasiswa Kalbis Institute');

                    //add a recipient
                    $mail->addAddress($email);

                    //Email subject
                    $mail->Subject = 'Jadwal Ujian Susulan';

                    //set email format to HTML
                    $mail->isHTML(true);

                    //Email body content
                    $mailContent = "
                            <p>Hai, Form ujian susulan dengan kode ".$y." telah kami terima, berikut detail jadwal untuk mengikuti ujian susulan nya : </p>
                            <p>Kode Ujian       : ".$this->input->post('kodeujiansusulan')."</p>
                            <p>Mata Kuliah      : ".$matkul."</p>
                            <p>Tanggal          : ".date("d F Y", strtotime($tanggal))."</p>
                            <p>Jam              : ".date("H:i", strtotime($jam))." WIB</P>
                            <p>Ruang            : ".$ruang."</p>
                            <p>Mohon datang tepat waktu untuk mengikuti ujian, Terima kasih.</p>

                            ";
                        $mail->Body = $mailContent;
                        //Send email
                        if($mail->send())
                        {
                            $this->Admin_Model->editujian($x,$y);
                            redirect('admin/home');
                        }else{
                            echo 'message could not be sent';
                            echo 'Mailer Error: ' . $mail->ErrorInfo;
                        }    
                }
            }

            else{
                $this->Admin_Model->editujian($x,$y);
                redirect('admin/home');
            }           
    }


    public function updateJudul($x,$y)
    {
        
        $this->Admin_Model->editjudul($x,$y);
        redirect('admin/home');   
    }

    public function updateProposal($x,$y)
    {
        $this->Admin_Model->editproposal($x,$y);
        redirect('admin/home');   
    }

    public function hapus($jenis, $id)
    {
        switch ($jenis)
        {
            case 1:
                $this->Admin_Model->hapus(1,$id);
                redirect('admin/dataMahasiswa');                     
                break;

            case 2:
                $this->Admin_Model->hapus(2,$id);
                redirect('admin/dataAdmin');                     
                break;

            case 3:
                $this->Admin_Model->hapus(3,$id);
                redirect('admin/dataMatkul');                       
                break;

            case 4:
                $this->Admin_Model->hapus(4,$id);
                redirect('admin/dataJadwalUjian');                        
                break;
        }

    }

    public function cariDataMatkul()
    {
        $kode = $this->input->post('Kode_Jurusan');
        $kodeSem = $this->input->post('Semester');
        $matkul = $this->Admin_Model->getAllData(14,$kode,$kodeSem);    
        $lists = '<option class="font-light" disabled selected>Pilih Mata Kuliah</option>';
    
        foreach($matkul as $data)
        {
            $lists .= "<option value='".$data['Nama_Matkul']."'>".$data['Nama_Matkul']."</option>";
        }
        
        $callback = array('list_matkul'=>$lists); // Masukan variabel lists tadi ke dalam array $callback dengan index array : list_kota
        echo json_encode($callback); // konversi varibael $callback menjadi JSON

    }

    public function verifForgotPass()
    {
        $email = $this->input->post('email');
        
        $cek = $this->Admin_Model->getAllData(15,$email);
        if($cek['status'])
        {
            $mail = $this->phpmailer_lib->load();

            $nama = $cek['data'][0]['Nama'];
            $id = $cek['data'][0]['Id_Admin'];

            $code = random_string('md5');
            $key = base64_encode($email.$code);

            //SMTP configuration
            $mail->isSMTP();
            $mail->Host = 'smtp.gmail.com';
            $mail->SMTPAuth = true;
            $mail->Username = 'layanankalbis@gmail.com';
            $mail->Password = 'kalbis1234';
            $mail->SMTPSecure = 'tls';
            $mail->Port = 587;

            $mail->setFrom('layanankalbis@gmail.com', 'Layanan Mahasiswa Kalbis Institute');
            $mail->addReplyTo('layanankalbis@gmail.com', 'Layanan Mahasiswa Kalbis Institute');

            //add a recipient
            $mail->addAddress($email);

            //Email subject
            $mail->Subject = 'Verifikasi Forgot Password';

            //set email format to HTML
            $mail->isHTML(true);            

            //Email body content
            $mailContent = "
                <h4>Hai, ".$nama." </h4>
                <p>Hai, apakah kamu lupa dengan password kamu? Jika ya, mohon klik tombol dibawah ini untuk konfirmasi.</p>
                
                <button style='background-color: #4285F4;'><a href='".base_url()."admin/forgotPass/".$key."' style='color: #FFFFFF;'>Verifikasi</a></button>
                        
                    ";
            
            $mail->Body = $mailContent;

            //Send email
            if($mail->send()){
                $this->Admin_Model->setStatus($id,$key);
                redirect('admin');
                
            }else{
                echo 'message could not be sent';
                echo 'Mailer Error: ' . $mail->ErrorInfo;
            }
        }

    }

    public function forgotPass($key)
    {
        $data['judul'] = "Change Password";
        $data['admin'] = $this->Admin_Model->getAllData(16,$key);
        $this->load->view('templates/headeradmin2', $data);
        $this->load->view('admin/chgpass');
        $this->load->view('templates/footeradmin2');
    }

    public function chgpw($key)
    {
        $password = $this->input->post('password');
        $confirmpassword = $this->input->post('password1');
        if($password == $confirmpassword){
            $this->Admin_Model->chgpw();
            redirect('Admin');

        }
        else{
            redirect('Admin/forgotPass/'.$key);
        }
        
    }

    public function gantipassword()
    {

        if ($this->session->userdata('Status') == 2) {
            $this->form_validation->set_rules('passlama', 'Current Password', 'required|trim');
            $this->form_validation->set_rules('password', 'New Password', 'required|trim|min_length[4]|matches[password1]');
            $this->form_validation->set_rules('password1', 'confirm New Password', 'required|trim|min_length[4]|matches[password]');

            if ($this->form_validation->run() == false) 
            {
                echo "Ubah password gagal. Pastikan password baru sama dengan konfirmasi password";
            } 
            else 
            {
                $admin = $this->Admin_Model->getAllData(19, $this->input->post('id'));
                $data = $admin['data'];
          
                $current_password = $this->input->post('passlama');
                $new_password = $this->input->post('password');

                if (!password_verify($current_password, $data['Password'])) 
                {
                    // $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert" style="color: red; font-weight:bold;">Wrong Current Password!</div>');
                    // redirect('Admin');
                    echo "Password tidak cocok";
                } 
                else 
                {
                    if ($current_password == $new_password) 
                    {
                        // $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">New password caanot be the same as current password</div>');
                        // redirect('Admin');
                        echo "Password baru tidak boleh sama dengan password baru";
                    } 
                    else 
                    {
                        $this->Admin_Model->chgpw();
                        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert" style="color:blue; font-weight=bold;">Change password success!</div>');
                        redirect('Admin');
                    }
                }
            }
        } 
        else {
           redirect('admin');
        }
    }

    public function logout()
    {
        $this->session->sess_destroy();
        redirect('admin');
    }
}
    
