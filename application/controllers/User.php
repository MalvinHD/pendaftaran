<?php

defined('BASEPATH') or exit('No direct script access allowed');

class User extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('User_Model');
        $this->load->helper('url');
        $this->load->helper('string');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('phpmailer_lib');
        
    }

    public function index()
    {
        if ($this->session->userdata('Status') == 1) {
            $data['judul'] = 'Layanan Mahasiswa Kalbis Institute';
            $this->load->view('templates/header1', $data);
            $this->load->view('user/index');
            $this->load->view('templates/footer1');
        }
        else {
            if($this->session->userdata('Status') == 2)
            {
                redirect('Admin');    
            }
            else{
                $this->session->set_flashdata('message', '<div class="center" style="color:red; font-weight:bold;">Login Terlebih dahulu</div>');
                redirect('Home');
            }  
        }
    }

    public function ujianSusulan()
    {
        if ($this->session->userdata('Status') == 1) {
            $data['judul'] = 'Ujian Susulan';
            $data['jurusan'] = $this->User_Model->getAllData(2);
            $data['matakuliah'] = $this->User_Model->getAllData(6, $this->session->userdata('semester'), $this->session->userdata('kodeJurusan'));

            $this->load->view('templates/header1', $data);
            $this->load->view('user/ujianSusulan', $data);
            $this->load->view('templates/footer1');
        } 
        else {
            if($this->session->userdata('Status') == 2)
            {
                redirect('Admin');    
            } else{

                $this->session->set_flashdata('message', '<div class="red font-light">Login Terlebih dahulu</div>');
                redirect('Home');
            }
        }
    }

    public function proposal()
    {
        if ($this->session->userdata('Status') == 1) {
            $data['judul'] = 'Form Pengajuan Proposal Tugas Akhir';
            $data['jurusan'] = $this->User_Model->getAllData(2);
            //CEK
            $data['skripsi'] = $this->User_Model->getAllData(3, $this->session->userdata('nim'));
            $data['cekproposal'] = $this->User_Model->getAllData(5, $this->session->userdata('nim'),'WAIT');

            $this->load->view('templates/header1', $data);
            $this->load->view('user/proposal', $data);
            $this->load->view('templates/footer1');
        }

        else {
            if($this->session->userdata('Status') == 2)
            {
                redirect('Admin');    
            }
            else
            {
                $this->session->set_flashdata('message', '<div class="red font-light">Login Terlebih dahulu</div>');
                redirect('Home');    
            }
            
        }
    }

    public function formPerubahanJudul()
    {
        if ($this->session->userdata('Status') == 1) {
            $data['judul'] = 'Form Perubahan Judul Tugas Akhir';
            $data['jurusan'] = $this->User_Model->getAllData(2);
            //CEK
            $data['skripsi'] = $this->User_Model->getAllData(3, $this->session->userdata('nim'));
            $data['cekskripsi'] = $this->User_Model->getAllData(4, $this->session->userdata('nim'),'WAIT');

            $this->load->view('templates/header1', $data);
            $this->load->view('user/formPerubahanJudul', $data);
            $this->load->view('templates/footer1');
        } 
        else {
            if($this->session->userdata('Status') == 2)
            {
                redirect('Admin');    
            } else{

                $this->session->set_flashdata('message', '<div class="center" style="color:red; font-weight:bold;">Login Terlebih dahulu</div>');
                redirect('Home');
            }
        }
    }

    public function change_pass()
    {
        if ($this->session->userdata('Status') == 1) {
            $data = $this->User_Model->getAllData(8, $this->input->post('id'));
            $this->form_validation->set_rules('current_password', 'Current Password', 'required|trim');
            $this->form_validation->set_rules('new_password1', 'New Password', 'required|trim|min_length[6]|matches[new_password2]');
            $this->form_validation->set_rules('new_password2', 'confirm New Password', 'required|trim|min_length[6]|matches[new_password1]');

            if ($this->form_validation->run() == false) 
            {
                redirect('user/index');
            }
            else 
            {
                $current_password = $this->input->post('current_password');
                $new_password = $this->input->post('new_password1');

                if (!password_verify($current_password, $data['Password'])) 
                {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert" style="color: red; font-weight:bold;">Wrong Current Password!</div>');
                    redirect('user/index');
                } 
                else 
                {
                    if ($current_password == $new_password) 
                    {
                        $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">New password cannot be the same as current password</div>');
                        redirect('user/index');
                    } 
                    else 
                    {
                        $this->User_Model->change_pass($this->input->post('id'));
                        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert" style="color:blue; font-weight=bold;">Change password success!</div>');
                        redirect('user/index');
                    }
                }
            }
        } 
        else {
            if($this->session->userdata('Status') == 2)
            {
                redirect('Admin');    
            } else{

                $this->session->set_flashdata('message', '<div class="red font-light">Login Terlebih dahulu</div>');
                redirect('Home');
                
            }
        }
    }


    public function ujian()
    {
        if ($this->session->userdata('Status') == 1)
        {
            $this->form_validation->set_rules('tahunC', 'Tahun pertama', 'required|trim');
            $this->form_validation->set_rules('tahunD', 'Tahun kedua', 'required|trim');
            $this->form_validation->set_rules('matakuliah', 'Mata kuliah', 'required');
            $this->form_validation->set_rules('alasan', 'Alasan', 'required');

            if ($this->form_validation->run() == FALSE){
                $data['judul'] = 'Ujian Susulan';
                $data['jurusan'] = $this->User_Model->getAllData(2);
                $data['matakuliah'] = $this->User_Model->getAllData(6, $this->session->userdata('semester'), $this->session->userdata('kodeJurusan'));

                $this->load->view('templates/header1', $data);
                $this->load->view('user/ujianSusulan', $data);
                $this->load->view('templates/footer1');
            } else {
                $str = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $shuffled = substr(str_shuffle($str),0,6);
                $kode = $this->session->userdata('kodeJurusan').$shuffled;
                $this->User_Model->tambahUjian($kode);

                //PHPmailer Object
                $mail = $this->phpmailer_lib->load();
                $email = $this->session->userdata('email');

                //SMTP configuration
                $mail->isSMTP();
                $mail->Host = 'smtp.gmail.com';
                $mail->SMTPAuth = true;
                $mail->Username = 'layanankalbis@gmail.com';
                $mail->Password = 'kalbis1234';
                $mail->SMTPSecure = 'tls';
                $mail->Port = 587;

                $mail->setFrom('layanankalbis@gmail.com', 'Layanan Mahasiswa Kalbis Institute');
                $mail->addReplyTo('layanankalbis@gmail.com', 'Layanan Mahasiswa Kalbis Institute');

                //add a recipient
                $mail->addAddress($email);

                //Email subject
                $mail->Subject = 'Pembayaran Ujian Susulan Kalbis Institute';

                //set email format to HTML
                $mail->isHTML(true);

                // kirim gambar $mail->AddEmbeddedImage('images/kalbis.png', 'logo_2u');

                //Email body content
                $mailContent = "
                        <p>Hai mohon konfirmasi ujian susulan yang telah kalbiser masukan dibawah ini : .</p>
                        <p>Nama         : ".$this->session->userdata('nama')."</p>
                        <p>NIM          : ".$this->session->userdata('nim')."</p>
                        <p>Jurusan      : ".$this->session->userdata('namaJurusan')."</P>
                        <p>Mata Kuliah  : ".$this->input->post('matakuliah')."</p>
                        <p>Mohon dibayarkan secepatnya agar dapat diproses sebesar:'Rp.500.000' berikut cara pembayarannya melalui virtual account BCA '00470 + NIM' Terima kasih.</p>

                        <button type='submit'><a href='".base_url()."user/bayar/".$kode."'>Konfirmasi</button>
                        ";
                    $mail->Body = $mailContent;

                    //Send email
                    if($mail->send()){
                        redirect('User/index');
                    }else{
                        echo 'message could not be sent';
                        echo 'Mailer Error: ' . $mail->ErrorInfo;
                    }
            } 
        } else {
            if($this->session->userdata('Status') == 2)
            {
                redirect('Admin');    
            } else{

                $this->session->set_flashdata('message', '<div class="red font-light">Login Terlebih dahulu</div>');
                redirect('Home');
                
            } 
        }
    }



    public function ajuinproposal()
    {
        if ($this->session->userdata('Status') == 1)
        {
            $this->form_validation->set_rules('tahunA', 'Tahun pertama', 'required|trim');
            $this->form_validation->set_rules('tahunB', 'Tahun kedua', 'required|trim');
            $this->form_validation->set_rules('ja', 'Judul Bahasa Indonesia', 'required');
            $this->form_validation->set_rules('jb', 'Judul Bahasa Inggris', 'required');

            if ($this->form_validation->run() == FALSE){
                $data['judul'] = 'Form Pengajuan Proposal Tugas Akhir';
                $data['jurusan'] = $this->User_Model->getAllData(2);
                //CEK
                $data['skripsi'] = $this->User_Model->getAllData(3, $this->session->userdata('nim'));
                $data['cekproposal'] = $this->User_Model->getAllData(5, $this->session->userdata('nim'),'WAIT');

                $this->load->view('templates/header1', $data);
                $this->load->view('user/proposal', $data);
                $this->load->view('templates/footer1');

            } else {
                $this->User_Model->listAjuinProposal();
                redirect('user/index');
            }
        } else {
            if($this->session->userdata('Status') == 2)
            {
                redirect('Admin');    
            } else{

                $this->session->set_flashdata('message', '<div class="red font-light">Login Terlebih dahulu</div>');
                redirect('Home');
                
            } 
        }
    }


    public function gantiJudul()
    {
        if ($this->session->userdata('Status') == 1)
        {
            $this->form_validation->set_rules('tahun1', 'Tahun pertama', 'required|trim');
            $this->form_validation->set_rules('tahun2', 'Tahun kedua', 'required|trim');
            $this->form_validation->set_rules('jb1', 'Judul Bahasa Indonesia', 'required');
            $this->form_validation->set_rules('jb2', 'Judul Bahasa Inggris', 'required');
            $this->form_validation->set_rules('alasan', 'Alasan', 'required');

            if ($this->form_validation->run() == FALSE){
                $data['judul'] = 'Form Perubahan Judul Tugas Akhir';
                $data['jurusan'] = $this->User_Model->getAllData(2);
                //CEK
                $data['skripsi'] = $this->User_Model->getAllData(3, $this->session->userdata('nim'));
                $data['cekskripsi'] = $this->User_Model->getAllData(4, $this->session->userdata('nim'),'WAIT');

                $this->load->view('templates/header1', $data);
                $this->load->view('user/formPerubahanJudul', $data);
                $this->load->view('templates/footer1');
            }
            else {
                $cek = $this->db->get_where('skripsi', ['Kode_Skripsi' => $this->input->post('kode_skripsi')])->row_array();
                if ($cek) {
                    $this->User_Model->listGantiJudul();
                    redirect('user/index');
                } else {
                    redirect('user/formPerubahanJudul');
                }
            }
        } else {
            if($this->session->userdata('Status') == 2)
            {
                redirect('Admin');    
            } else{

                $this->session->set_flashdata('message', '<div class="red font-light">Login Terlebih dahulu</div>');
                redirect('Home');
                
            } 
        }
    }


    public function cekStatus()
    {
        if ($this->session->userdata('Status') == 1) {
            $data['judul'] = "Status Form";
            $data['skripsi'] = $this->User_Model->getAllData(5, $this->session->userdata('nim'));
            $data['ujian'] = $this->User_Model->getAllData(7, $this->session->userdata('nim'));
            $data['daftarubahjudul'] = $this->User_Model->getAllData(4, $this->session->userdata('nim'));
            
            $this->load->view('templates/header1', $data);
            $this->load->view('user/status', $data);
            $this->load->view('templates/footer1');
        }
        else {
            if($this->session->userdata('Status') == 2)
            {
                redirect('Admin');    
            } 
            else{

                $this->session->set_flashdata('message', '<div class="red font-light">Login Terlebih dahulu</div>');
                redirect('Home');
            }
        }
    }

    public function bayar($kode)
    {
        if (($this->session->userdata('Status') == 2) == FALSE) {
            $this->User_Model->bayar($kode);
            redirect('User/index');
        }
        else {
            if($this->session->userdata('Status') == 2)
            {
                redirect('Admin');    
            } 
            else{
                $this->session->set_flashdata('message', '<div class="red font-light">Login Terlebih dahulu</div>');
                redirect('Home');
            }
        }
    }
}
