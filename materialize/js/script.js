$(document).ready(function(){

	$(".dropdown-button").dropdown();
	$(".modal").modal();
	$("select").material_select();
	$('.parallax').parallax();
  $(".btnside").sideNav();

  $('.tombol').on('click', function(){
    Swal({
      title: 'Error!',
      text: 'Do you want to continue',
      icon: 'success',
      confirmButtonText: 'Cool'
    })
  });
    
	AOS.init();
  
});

// const flashData = $('.flash-data').data('flashdata');
//   if (flashData) {
//     Swal({
//       title: 'FOrm berhasil' + flashData,
//       text: 'goblol', 
//       type: 'success'
//     });
//   }
